import 'package:meta/meta.dart';

abstract class RecoverAccountCode {
  Future<String?> recover(InputRecoverAccountCode input);
}

@immutable
class InputRecoverAccountCode {
  const InputRecoverAccountCode(this.code);

  final String code;
}
