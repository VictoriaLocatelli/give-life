import 'package:give_life/shared/core/entity/client_account_entitie.dart';
import 'package:give_life/shared/data/http/http.dart';

import '../map_ext.dart';

class RemoteClientAccountKeys {
  static const String token = 'token';
  static const String id = 'user.id';
  static const String name = 'user.people.name';
  static const String photo = 'user.people.logo';
  static const String email = 'user.email';
  static const String personId = 'user.person_id';
}

class RemoteClientAccountSerializer {
  const RemoteClientAccountSerializer(
      {required this.name,
      required this.email,
      required this.token,
      this.photo,
      this.personId});

  final String name;
  final String? photo;
  final String email;
  final String token;
  final int? personId;

  factory RemoteClientAccountSerializer.fromMap(Map<String, dynamic> map) {
    try {
      final name = map
          .getValueByKeyList(RemoteClientAccountKeys.name.split('.')) as String;
      final photo =
          map.getValueByKeyListPhoto(RemoteClientAccountKeys.photo.split('.'))
              as String;

      final email =
          map.getValueByKeyList(RemoteClientAccountKeys.email.split('.'))
              as String;
      final token =
          map.getValueByKeyList(RemoteClientAccountKeys.token.split('.'))
              as String;
      final personId =
          map.getValueByKeyList(RemoteClientAccountKeys.personId.split('.'))
              as int?;

      return RemoteClientAccountSerializer(
          name: name,
          photo: photo,
          email: email,
          token: token,
          personId: personId);
    } on TypeError {
      throw HttpError.invalidData;
    }
  }

  ClientAccountEntity toEntity() => ClientAccountEntity(
      name: name, photo: photo, email: email, token: token, personId: personId);
}
