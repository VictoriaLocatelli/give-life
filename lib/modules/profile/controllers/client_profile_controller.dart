import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:give_life/modules/profile/contracts/load_banks.dart';
import 'package:give_life/modules/profile/contracts/load_client_profile.dart';
import 'package:give_life/modules/profile/entity/bank_entity.dart';
import 'package:give_life/modules/profile/entity/update_client_profile.dart';
import 'package:give_life/modules/profile/contracts/update_current_account.dart';
import 'package:give_life/modules/profile/models/account_profile_model.dart';
import 'package:give_life/modules/profile/models/dropdown_type_model.dart';
import 'package:give_life/modules/profile/models/property_model.dart';
import 'package:give_life/modules/profile/widgets/dist_compl_widget.dart';
import 'package:give_life/modules/profile/widgets/input_date_widget.dart';
import 'package:give_life/modules/profile/widgets/rg_input_widget.dart';
import 'package:give_life/modules/profile/widgets/widgets.dart';
import 'package:give_life/shared/componets/utils/show_snack_bar.dart';
import 'package:give_life/shared/componets/widgets/fields/mask_all.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';
import 'package:give_life/shared/core/entity/city_entity.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/contracts/remotes/load_cities.dart';
import 'package:give_life/shared/data/validation/validation.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/shared/componets/extensions/extensions.dart';

import 'client_profile_vm.dart';
import '../entity/client_profile_entity_extensions.dart';

part 'client_profile_controller.g.dart';

class ClientProfileController = _ClientProfileControllerBase
    with _$ClientProfileController;

abstract class _ClientProfileControllerBase with Store {
  _ClientProfileControllerBase({
    required LoadClientProfile loadClientProfile,
    required UpdateClientProfile updateClientProfile,
    required LoadCities loadCities,
    required LoadBanks loadBanks,
    void Function()? callback,
  })  : _loadClientProfile = loadClientProfile,
        _updateClientProfile = updateClientProfile,
        _loadCities = loadCities,
        _callback = callback,
        _loadBanks = loadBanks;

  final LoadClientProfile _loadClientProfile;
  final UpdateClientProfile _updateClientProfile;
  final LoadCities _loadCities;
  final LoadBanks _loadBanks;
  List<CityEntity> _list = [];
  final void Function()? _callback;
  PropertyModel propertyModel = PropertyModel();
  AccountModel accountModel = AccountModel();

  List<DropdownTypeModel> accountTypes = <DropdownTypeModel>[
    DropdownTypeModel(name: 'Corrente', value: 1),
    DropdownTypeModel(name: 'Poupança', value: 2),
  ];

  @observable
  BankEntity? _currentbBank;
  @observable
  DropdownTypeModel? _currentBankType;

  List<BankEntity?> banks = [];

  ClientProfileVM _clientProfileVM = ClientProfileVM(address: '');

  @observable
  int next = 0;

  @action
  setNext(int value) {
    if (-1 > value && value < 5) {
      next = value;
    }
    print(next);
  }

  @observable
  bool _isLoading = false;
  bool get isLoading => _isLoading;
  @action
  void setIsLoading(bool value) => _isLoading = value;

  @observable
  ImageProvider<Object>? _photoBase64;

  ImageProvider<Object>? get photoBase64 => _photoBase64;

  @action
  void setPhotoBase64(ImageProvider<Object>? value) => _photoBase64 = value;

  String? get photo => _clientProfileVM.photo;
  set photo(String? value) {
    _clientProfileVM.photo = value;
  }

  Future<void> loadData() async {
    setIsLoading(true);

    try {
      banks = await _loadBanks.load();
      await Future.wait([
        _fetchProfile(),
        _fetchCities(),
      ]);
    } on DomainError catch (error) {
      ShowSnackBar.error(error.description).show();
    } finally {
      setIsLoading(false);
    }
  }

  Future<void> cropImage(BuildContext context) async {
    final ImagePicker picker = ImagePicker();
    // Pick an image
    String? photoB;
    final XFile? image = await picker.pickImage(
        preferredCameraDevice: CameraDevice.front, source: ImageSource.gallery);
    if (image != null) {
      CroppedFile? resultFile =
          await ImageCropped(context: context, pickedFile: image).cropImage();
      if (resultFile != null) {
        resultFile.readAsBytes().then((value) {
          photoB = base64Encode(value);
          photo = "data:image/png;base64,$photoB";
          setPhotoBase64(MemoryImage(value));
        });
      }
    }
  }

  List<CityEntity> get list => _list;

  String removerAcentos(String str) {
    var comAcento =
        'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var semAcento =
        'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz';

    for (int i = 0; i < comAcento.length; i++) {
      str = str.replaceAll(comAcento[i], semAcento[i]);
    }

    return str;
  }

  List<CityEntity> getCities() {
    List<CityEntity> citiess = [];
    for (var element in list) {
      element.setName(removerAcentos(element.name));
      citiess.add(element);
    }
    citiess.addAll(_cities);
    return citiess;
  }

  @action
  Future<void> _fetchProfile() async {
    _clientProfileVM = (await _loadClientProfile.load()).toViewModel();
    if (!banks.isEmpty && _clientProfileVM.bankId != null) {
      _currentbBank =
          banks.firstWhere((bank) => bank?.id == _clientProfileVM.bankId);
    }
    if (_clientProfileVM.bankId != null) {
      _currentBankType = accountTypes.firstWhere(
          (type) => type.value == _clientProfileVM.bankId,
          orElse: () => DropdownTypeModel(
                name: '',
                value: 0,
              ));
    }
    _clientProfileVM;
  }

  @action
  Future<void> _fetchCities() async {
    _list = await Modular.get<LoadCities>().load();
    _cities = (await _loadCities.load()).asObservable();
  }

  @observable
  ObservableList<CityEntity> _cities = <CityEntity>[].asObservable();
  ObservableList<CityEntity> get cities => _cities;

  String? get name => _clientProfileVM.name;
  String? get propertyName => _clientProfileVM.propertyName;
  String? get propertyNameError => _clientProfileVM.propertyNameError;
  String? get propertyStateRegistration =>
      _clientProfileVM.propertyStateRegistration;
  String? get propertyStateRegistrationError =>
      _clientProfileVM.propertyStateRegistrationError;
  String? get zipCode => _clientProfileVM.zipCode;
  String? get zipCodeError => _clientProfileVM.zipCodeError;
  String? get nameError => _clientProfileVM.nameError;

  String? get email => _clientProfileVM.email;
  String? get emailError => _clientProfileVM.emailError;

  String? get phone1 => _clientProfileVM.phone1;
  String? get phone1Error => _clientProfileVM.phone1Error;

  String? get nif => _clientProfileVM.nif;
  String? get nifError => _clientProfileVM.nifError;

  String? get rg => _clientProfileVM.rg;
  String? get rgError => _clientProfileVM.rgError;

  String? get address => _clientProfileVM.address;
  String? get addressError => _clientProfileVM.addressError;

  String? get number =>
      _clientProfileVM.number != null ? _clientProfileVM.number.toString() : '';
  String? get numberError => _clientProfileVM.numberError;

  String? get district => _clientProfileVM.district;
  String? get districtError => _clientProfileVM.districtError;
  String? get complement => _clientProfileVM.complement;
  String? get complementError => _clientProfileVM.complementError;

  @action
  String? districtValidator(String? value) {
    _clientProfileVM.district = value;
    _clientProfileVM.districtError = ValidationBuilder(value).max(30).build();
    return _clientProfileVM.districtError;
  }

  @action
  String? complementValidator(String? value) {
    _clientProfileVM.complement = value;
    _clientProfileVM.complementError = ValidationBuilder(value).max(30).build();
    return _clientProfileVM.complementError;
  }

  @action
  String? numberValidator(String? value) {
    if (value != null && value != '') {
      _clientProfileVM.number = value;
    }
    _clientProfileVM.numberError = ValidationBuilder(value).max(5).build();
    return _clientProfileVM.numberError;
  }

  String? get propertyAddress => _clientProfileVM.propertyAddress;
  String? get propertyAddressError => _clientProfileVM.propertyAddressError;

  CityEntity? get city =>
      _cities.firstWhere((e) => e.id == _clientProfileVM.city,
          orElse: () => CityEntity(id: 0, name: '', ufSigla: ''));
  String? get cityError => _clientProfileVM.cityError;

  CityEntity? get propertyCity =>
      _cities.firstWhere((e) => e.id == _clientProfileVM.propertyCity,
          orElse: () => CityEntity(id: 0, name: '', ufSigla: ''));
  String? get propertyCityError => _clientProfileVM.propertyCityError;

  //INPUT DATE

  String get date => _clientProfileVM.date != null
      ? formatDate(DateTime.parse(_clientProfileVM.date!))
      : "";
  String? get dateError => _clientProfileVM.dateError;

  String formatDate(DateTime date) {
    List<String> partDateText = date.toString().split(' ')[0].split('-');
    return "${partDateText[2]}/${partDateText[1]}/${partDateText[0]}";
  }

  //BANK / ACCOUNT
  int? get bankId => _clientProfileVM.bankId;
  String? get bankAgency => _clientProfileVM.bankAgency;
  int? get bankType => _clientProfileVM.bankType;
  String? get bankAccount => _clientProfileVM.bankAccount;
  BankEntity? get currentbBank => _currentbBank;
  DropdownTypeModel? get currentBankType => _currentBankType;

  @action
  String? bankAccountValidator(String? value) {
    _clientProfileVM.bankAccount = value;
    accountModel.account = value;
    _clientProfileVM.bankAccountError =
        ValidationBuilder(value).max(10).build();
    return _clientProfileVM.bankAccountError;
  }

  void dateValidator(String? value) {
    print(value);
    _clientProfileVM.date = value;
  }

  @action
  String? bankAgencyValidator(String? value) {
    _clientProfileVM.bankAgency = value;
    accountModel.agency = value;
    _clientProfileVM.bankAgencyError = ValidationBuilder(value).max(6).build();
    return _clientProfileVM.bankAgencyError;
  }

  @action
  String? bankTypeValidator(DropdownTypeModel? value) {
    String? fixedValue = value != null && value.value == 1 ? 'um' : 'dois';
    _clientProfileVM.bankType = value?.value;
    accountModel.type = value?.value;
    _currentBankType = value;
    fixedValue = ValidationBuilder(fixedValue).max(6).build();
    return fixedValue;
  }

  @action
  String? bankIdValidator(BankEntity? value) {
    _clientProfileVM.bankId = value?.id;
    accountModel.bankid = value?.id;
    _currentbBank = value;
    _clientProfileVM.bankIdError =
        ValidationBuilder(value?.name).required().min(3).build();
    return _clientProfileVM.bankIdError;
  }

  @action
  String? nameValidator(String? value) {
    _clientProfileVM.name = value;

    _clientProfileVM.nameError =
        ValidationBuilder(value).required().min(3).build();

    return _clientProfileVM.nameError;
  }

  @action
  String? propertyNameValidator(String? value) {
    _clientProfileVM.propertyName = value;
    propertyModel.name = value;
    _clientProfileVM.propertyNameError =
        ValidationBuilder(value).required().min(3).build();

    return _clientProfileVM.propertyNameError;
  }

  @action
  String? emailValidator(String? value) {
    _clientProfileVM.email = value;
    _clientProfileVM.emailError =
        ValidationBuilder(value).required().email().build();

    return _clientProfileVM.emailError;
  }

  @action
  String? phone1Validator(String? value) {
    value = value?.digitsOnly();
    _clientProfileVM.phone1 = value;
    _clientProfileVM.phone1Error =
        ValidationBuilder(value).required().min(10).max(11).build();

    return _clientProfileVM.phone1Error;
  }

  @action
  void rgValidator(String? value) {
    _clientProfileVM.rg = value;
  }

  @action
  String? nifValidator(String? value) {
    const cpfLength = 11;
    _clientProfileVM.nif = value;

    value = value?.digitsOnly();
    var validator = ValidationBuilder(value).required();

    if ((value ?? '').length <= cpfLength) {
      validator = validator.cpf();
    } else {
      validator = validator.cnpj();
    }

    _clientProfileVM.nifError = validator.build();

    return _clientProfileVM.nifError;
  }

  @action
  String? addressValidator(String? value) {
    _clientProfileVM.address = value;
    _clientProfileVM.addressError =
        ValidationBuilder(value).required().max(60).build();

    return _clientProfileVM.addressError;
  }

  @action
  String? propertyStateRegistrationValidator(String? value) {
    _clientProfileVM.propertyStateRegistration = value;
    propertyModel.stateregistration = value;
    _clientProfileVM.propertyStateRegistrationError =
        ValidationBuilder(value).required().max(30).build();

    return _clientProfileVM.propertyStateRegistrationError;
  }

  @action
  String? propertyAddressValidator(String? value) {
    _clientProfileVM.propertyAddress = value;
    propertyModel.address = value;
    _clientProfileVM.propertyAddressError =
        ValidationBuilder(value).required().max(60).build();

    return _clientProfileVM.propertyAddressError;
  }

  @action
  String? cityValidator(CityEntity? value) {
    _clientProfileVM.city = value?.id;
    _clientProfileVM.cityError =
        ValidationBuilder(value?.name).required().build();

    return _clientProfileVM.cityError;
  }

  @action
  String? propertyCityValidator(CityEntity? value) {
    _clientProfileVM.propertyCity = value?.id;
    propertyModel.cityid = value?.id;
    _clientProfileVM.propertyCityError =
        ValidationBuilder(value?.name).required().build();

    return _clientProfileVM.propertyCityError;
  }

  @action
  String? zipValidator(String? value) {
    _clientProfileVM.zipCode = value;
    _clientProfileVM.zipCodeError =
        ValidationBuilder(value).required().min(10).build();

    return _clientProfileVM.zipCodeError;
  }

  @computed
  bool get isFormValid =>
      _clientProfileVM.name != null &&
      _clientProfileVM.nameError == null &&
      _clientProfileVM.email != null &&
      _clientProfileVM.emailError == null &&
      _clientProfileVM.phone1 != null &&
      _clientProfileVM.phone1Error == null &&
      _clientProfileVM.nif != null &&
      _clientProfileVM.nifError == null &&
      _clientProfileVM.address != null &&
      _clientProfileVM.addressError == null &&
      _clientProfileVM.city != null &&
      _clientProfileVM.cityError == null &&
      _clientProfileVM.zipCode != null &&
      _clientProfileVM.zipCodeError == null;

  @action
  void clean() {
    _isLoading = false;
  }

  Future<void> update() async {
    setIsLoading(true);

    try {
      final input = InputUpdateClientProfile(
          name: _clientProfileVM.name!,
          email: _clientProfileVM.email!,
          phone1: _clientProfileVM.phone1!.digitsOnly(),
          nif: _clientProfileVM.nif!,
          address: _clientProfileVM.address!,
          cityId: _clientProfileVM.city!,
          photo: _clientProfileVM.photo,
          zipCode:
              _clientProfileVM.zipCode!.replaceAll('.', '').replaceAll('-', ''),
          property: propertyModel,
          accountModel: accountModel,
          date: _clientProfileVM.date,
          rg: _clientProfileVM.rg,
          number: _clientProfileVM.number,
          district: _clientProfileVM.district,
          complement: _clientProfileVM.complement);

      await _updateClientProfile.update(input);
      await _fetchProfile();
      await Modular.get<UpdateCurrentAccount>().update();
      ShowSnackBar.success('Perfil atualizado com sucesso.').show();
      _callback?.call();
      Modular.to.navigate('/profile/');
    } on DomainError catch (error) {
      ShowSnackBar.error(error.description).show();
    } finally {
      setIsLoading(false);
    }
  }
}
