import 'package:flutter/material.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:give_life/shared/componets/utils/launch_url.dart';

import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';

class AppVersionWithLogo extends StatefulWidget {
  const AppVersionWithLogo({
    Key? key,
    this.color,
    this.width = 24 * 3,
    this.textColor,
  }) : super(key: key);

  final Color? color;
  final double width;
  final Color? textColor;

  @override
  State<AppVersionWithLogo> createState() => _AppVersionWithLogoState();
}

class _AppVersionWithLogoState extends State<AppVersionWithLogo> {
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return FutureBuilder(
      future: PackageInfo.fromPlatform(),
      builder: (BuildContext context, AsyncSnapshot<PackageInfo> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting)
          return Container();

        return GestureDetector(
          onTap: () => LaunchUrl.link('https://dix.digital/'),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Transform.translate(
                offset: Offset(0, -10),
                child: Text(
                  '\n V.${snapshot.data?.version ?? ''}',
                  textAlign: TextAlign.end,
                  style: textTheme.caption?.copyWith(color: widget.textColor),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
