import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:give_life/shared/componets/theme/app_colors.dart';

class BottomWidget extends StatelessWidget {
  const BottomWidget({super.key, required this.selected});
  final int selected;
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, c) {
      return Container(
        color: Theme.of(context).primaryColor,
        height: 70,
        width: c.maxWidth,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            GestureDetector(
                onTap: () => Modular.to.navigate('/home/'),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.contain,
                              image: AssetImage(AppImages.home.path))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        border: selected == 2
                            ? Border(
                                bottom: BorderSide(
                                color: AppColors.buttonGreenColor,
                                width: 3.0,
                              ))
                            : Border(
                                bottom: BorderSide(color: Colors.transparent)),
                      ),
                      child: Text(
                        "Agenda de leilões",
                        style: TextStyle(color: Colors.white, fontSize: 12),
                      ),
                    ),
                  ],
                )),
            GestureDetector(
                onTap: () => Modular.to.navigate('/trading/'),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.contain,
                              image: AssetImage(AppImages.hammer.path))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        border: selected == 3
                            ? Border(
                                bottom: BorderSide(
                                color: AppColors.buttonGreenColor,
                                width: 3.0,
                              ))
                            : Border(
                                bottom: BorderSide(color: Colors.transparent)),
                      ),
                      child: Text(
                        "Pregão",
                        style: TextStyle(color: Colors.white, fontSize: 12),
                      ),
                    ),
                  ],
                )),
            GestureDetector(
                onTap: () => Modular.to.navigate('/direct/'),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.contain,
                              image: AssetImage(AppImages.quotation.path))),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        border: selected == 1
                            ? Border(
                                bottom: BorderSide(
                                color: AppColors.buttonGreenColor,
                                width: 3.0,
                              ))
                            : Border(
                                bottom: BorderSide(color: Colors.transparent)),
                      ),
                      child: Text(
                        "Venda Direta",
                        style: TextStyle(color: Colors.white, fontSize: 12),
                      ),
                    ),
                  ],
                )),
          ],
        ),
      );
    });
  }
}
