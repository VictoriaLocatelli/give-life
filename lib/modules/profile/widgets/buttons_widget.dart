import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/shared/componets/constants/spacing.dart';

class ButtonsWidget extends StatelessWidget {
  const ButtonsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();
    return Column(
      children: [
        const SizedBox(height: Spacing.large),
        Observer(builder: (_) {
          return ElevatedButton(
            onPressed: controller.isFormValid ? controller.update : null,
            child: const Text('Confirmar'),
          );
        }),
        const SizedBox(height: Spacing.small),
        OutlinedButton(
          onPressed: () {
            int v = controller.next -= 1;
            controller.setNext(v);
          },
          child: const Text('Voltar'),
        ),
        const SizedBox(height: Spacing.xSmall),
      ],
    );
  }
}
