import '../app_config.dart';

mixin Environment {
  static final dev = AppConfig(
    appEnvironment: AppEnvironment.development,
    apiBaseUrl: 'https://bis365.com.br/bid365/api/v1',
    apiStorageUrl: 'https://bis365.com.br/bid365/storage/',
    apiTermsUrl:
        'https://bis365.com.br/bid365/storage/pdfs/termos-condicoes.pdf',
    appKey: '23a20977-9368-4f13-a008-d556163fa8df',
  );

  static final prod = AppConfig(
    appEnvironment: AppEnvironment.production,
    apiBaseUrl: 'https://bis365.com.br/bid365/api/v1',
    apiStorageUrl: 'https://app.chefinhodelivery.com.br/storage/',
    apiTermsUrl:
        'https://app.chefinhodelivery.com.br/storage/pdfs/termos-condicoes.pdf',
    appKey: '23a20977-9368-4f13-a008-d556163fa8df',
  );
}
