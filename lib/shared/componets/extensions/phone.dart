extension PhoneExtensions on String {
  String phoneFormat() {
    var pattern = r'(\d{2})(\d{1})(\d{4})(\d+)';
    var format = (Match m) => '(${m[1]}) ${m[2]} ${m[3]}-${m[4]}';

    if (length == 10) {
      pattern = r'(\d{2})(\d{4})(\d+)';
      format = (Match m) => '(${m[1]}) ${m[2]}-${m[3]}';
    }

    return replaceAllMapped(RegExp(pattern), format);
  }
}
