import 'package:equatable/equatable.dart';

class LocalClientCityEntitie extends Equatable {

  const LocalClientCityEntitie({
    required this.id,
    required this.title,
    required this.letter,
  });
  final int id;
  final String title;
  final String letter;


  @override
  List<Object?> get props => [id,title,letter];

  Map<String,dynamic> toJson()=><String,dynamic>{
    'id':id,
    'title':title,
    'letter':letter
    };



  LocalClientCityEntitie copyWith({
    int? id,
    String? title,
    String? letter
  }) {
    return LocalClientCityEntitie(id: id ?? this.id, title: title??this.title, letter: letter??this.letter);
  }

}