class PropertyModel {
    String? name;
    String? stateregistration;
    String? address;
    int? cityid;
    String? contactphone;
    String? contactname;
    String? car;
    double? area;

    PropertyModel({this.name, this.stateregistration, this.address, this.cityid, this.contactphone, this.contactname, this.car, this.area}); 

    PropertyModel.fromJson(Map<String, dynamic> json) {
        name = json['name'];
        stateregistration = json['state_registration'];
        address = json['address'];
        cityid = json['city_id'];
        contactphone = json['contact_phone'];
        contactname = json['contact_name'];
        car = json['car'];
        area = json['area'];
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = Map<String, dynamic>();
        data['name'] = name;
        data['state_registration'] = stateregistration;
        data['address'] = address;
        data['city_id'] = cityid;
        data['contact_phone'] = contactphone;
        data['contact_name'] = contactname;
        data['car'] = car;
        data['area'] = area;
        return data;
    }
}