/* import 'dart:async';
import 'dart:developer' as developer;
import 'package:connectivity/connectivity.dart';

enum ConnectionStatus { online, offline }

abstract class ConnectionService {
  Stream<ConnectionStatus> connectionStatus();
  Future<ConnectionStatus> checkConnection();
}

class ConnectionServiceImpl extends ConnectionService {
  StreamController<ConnectionStatus> _connectionController =
      StreamController<ConnectionStatus>.broadcast();

  void dispose() {
    _connectionController.close();
  }

  Future<void> setStatusConnection() async {
    try {
      final result = await checkConnection();
      _connectionController.add(result);
      onConnectionChanged();
    } catch (error, stackTrace) {
      developer.log(
        'Error ao alterar status da conexão',
        name: 'setStatusConnection',
        error: error.toString(),
        stackTrace: stackTrace,
      );
    }
  }

  @override
  Stream<ConnectionStatus> connectionStatus() {
    setStatusConnection();
    return _connectionController.stream;
  }

  @override
  Future<ConnectionStatus> checkConnection() async {
    final ConnectivityResult result = await Connectivity().checkConnectivity();
    return _parseConnectionstatus(result);
  }

  void onConnectionChanged() {
    Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) async {
      final status = _parseConnectionstatus(result);
      _connectionController.add(status);
    });
  }

  ConnectionStatus _parseConnectionstatus(ConnectivityResult result) {
    switch (result) {
      case ConnectivityResult.mobile:
        return ConnectionStatus.online;
      case ConnectivityResult.wifi:
        return ConnectionStatus.online;
      default:
        return ConnectionStatus.offline;
    }
  }
}
 */