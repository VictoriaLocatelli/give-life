import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';

class EmailInputWidget extends StatelessWidget {
  const EmailInputWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();

    return Observer(builder: (_) {
      return CustomTextFormField(
        label: 'E-mail',
        initialValue: controller.email,
        onChanged: controller.emailValidator,
        decoration: InputDecoration(
          errorText: controller.emailError,
        ),
      );
    });
  }
}
