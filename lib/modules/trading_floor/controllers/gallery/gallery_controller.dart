import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/trading_floor/factories/make_load_lots.dart';
import 'package:give_life/modules/trading_floor/factories/save_shot.dart';
import 'package:give_life/modules/trading_floor/models/input_save_shot.dart';
import 'package:give_life/modules/trading_floor/models/lot_entity.dart';
import 'package:give_life/modules/trading_floor/models/lot_images_entity.dart';
import 'package:give_life/modules/trading_floor/views/widget/dialogs_widget.dart';
import 'package:intl/intl.dart';

import 'package:mobx/mobx.dart';

part 'gallery_controller.g.dart';

class GalleryController = GalleryControllerBase with _$GalleryController;

abstract class GalleryControllerBase with Store {
  GalleryControllerBase({required LoadLots loadLot}) : _loadLot = loadLot;
  final LoadLots _loadLot;
  LotEntity? lot;
  LotImagesEntity? lotImages;

  int? mainId;

  @observable
  bool _isLoading = false;

  @observable
  bool isButton = false; //Para bloqueado

  @action
  void setButton(bool value) => isButton = value;

  @computed
  bool get isLoading => _isLoading;

  set isLoading(bool value) => _isLoading = value;

  Future<void> loadData(int id) async {
    mainId = id;
    try {
      isLoading = true;
      setButton(false);
      lot = (await _loadLot.load(id));
      isLoading = false;
    } catch (e) {
      print(e);
    }
  }

  late BuildContext pageContext;

  toBack() {
    loadData(mainId!);
  }

  toDatails(int id) {
    Modular.to.pushNamed('/trading/details/$id');
  }
  // modais

  // String getFormatMoney(dynamic value) =>
  //   NumberFormat.currency(symbol: 'R\$', locale: 'pt_BR').format(value);
}
