import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/home/factories/load_auctions.dart';
import 'package:give_life/modules/home/model/entity_%20detail_auctions.dart';
import 'package:give_life/modules/home/model/entity_%20live_auctions.dart';
import 'package:give_life/modules/home/model/live_sponsor.dart';
import 'package:give_life/modules/home/model/lot_entity.dart';
import 'package:mobx/mobx.dart';

import '../factories/load_live.dart';

part 'live_controller.g.dart';

class LiveController = LiveControllerBase with _$LiveController;

class LiveControllerBase with Store {
  LiveControllerBase({required this.loadAuctions, required this.loadLive});
  final LoadAuctions loadAuctions;
  final LoadLive loadLive;
  late BuildContext pageContext;
  //LiveSponsor liveSponsor;
  @observable
  bool _isLoading = true;

  bool get isLoading => _isLoading;

  set isLoading(bool value) => _isLoading = value;
  List<LotEntity> lots = <LotEntity>[];
  DetailsAuctionsEntity? auction;

  List<LiveAuctionsEntity> lot = <LiveAuctionsEntity>[];
  LiveSponsor? auctions;

  liveSponsor(value) => _isLoading = value;
  List<LiveSponsor?> live = [];
  LiveSponsor? auctionSponsor;

  String getPhone() =>
      auction?.phone != null ? phoneFormat(auction!.phone!) : "";

  String phoneFormat(String value) {
    final pattern = RegExp(r'(\d{2})(\d{5})(\d+)');
    final patternTwo = RegExp(r'(\d{2})(\d{4})(\d+)');

    return value.replaceAllMapped(value.length > 10 ? pattern : patternTwo,
        (Match m) => '(${m[1]}) ${m[2]}-${m[3]}');
  }

  void toLotDetails(int id) {
    Modular.to.pushNamed('/home/lotdetails/$id');
  }

  Future<void> loadData(int id) async {
    isLoading = true;
    try {
      auction = await loadAuctions.details(id);
      auction;
      live = await loadLive.loadLiveSponson();
      live;
      if (auction?.lots != null) {
        lots = auction!.lots!;
      }

      isLoading = false;
    } catch (e) {
      isLoading = false;
      print(e);
    }
  }
}
