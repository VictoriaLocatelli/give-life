abstract class LoadEmail {
  Future<Map<String, dynamic>> load();
}
