import 'package:give_life/modules/home/factories/load_live.dart';
import 'package:give_life/modules/home/model/live_sponsor.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';

class RemoteLoadBanners implements LoadLive {
  const RemoteLoadBanners({
    required this.url,
    required this.httpClient,
  });

  final String url;
  final HttpClient httpClient;

  @override
  Future<List<LiveSponsor?>> loadLiveSponson() async {
    try {
      List<LiveSponsor?> sponsor = [];
      var resul = (await httpClient.request(
          url: '$url?position=9', method: 'get'))['data'];
      resul;
      for (var e in resul) {
        sponsor.add(LiveSponsor.fromJson(e));
      }

      return sponsor;
    } on HttpError {
      throw DomainError.unexpected;
    }
  }
}
