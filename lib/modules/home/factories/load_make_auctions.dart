import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/home/factories/load_auctions.dart';
import 'package:give_life/modules/home/usescases/remote_load_actions.dart';
import 'package:give_life/shared/data/factories/api_url_factory.dart';
import 'package:give_life/shared/data/http/http_client.dart';

LoadAuctions makeRemoteLoadAuctions() => RemoteLoadAuctions(
      url: makeApiUrl('/auctions'),
      httpClient: Modular.get<HttpClient>(),
    );
