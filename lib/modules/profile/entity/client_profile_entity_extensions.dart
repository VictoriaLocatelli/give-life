import 'package:give_life/shared/componets/extensions/extensions.dart';
import 'package:give_life/modules/profile/entity/client_profile_entity.dart';

import '../controllers/client_profile_vm.dart';

extension ClientProfileExtensions on ClientProfileEntity {
  ClientProfileVM toViewModel() => ClientProfileVM(
      name: name,
      email: email,
      phone1: phone1 != null ? phone1!.phoneFormat() : phone1,
      nif: nif,
      date: date,
      address: address,
      city: cityId,
      propertyCity: property?.cityid,
      zipCode: zipCode,
      photo: photo,
      rg: rg,
      propertyAddress: property?.address,
      propertyName: property?.name,
      propertyStateRegistration: property?.stateregistration,
      bankAccount: account?.account,
      bankId: account?.bankid,
      bankAgency: account?.agency,
      bankType: account?.type,
      number: number,
      district: district,
      complement: complement);
}
