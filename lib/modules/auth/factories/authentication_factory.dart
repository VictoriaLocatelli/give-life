import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/auth/authentication.dart';
import 'package:give_life/modules/auth/usecases/remote_authentication.dart';
import 'package:give_life/shared/data/factories/api_url_factory.dart';
import 'package:give_life/shared/data/http/http_client.dart';

Authentication makeRemoteAuthentication() => RemoteAuthentication(
      httpClient: Modular.get<HttpClient>(),
      url: makeApiUrl('/auth/login'),
    );
