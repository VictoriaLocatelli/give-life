import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/gestures.dart';

import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:brasil_fields/brasil_fields.dart' as brasil_fields;
import 'package:give_life/modules/auth/controllers/signup/signup_controller.dart';
import 'package:give_life/shared/app_config.dart';
import 'package:give_life/shared/componets/constants/spacing.dart';
import 'package:give_life/shared/componets/widgets/app_version_with_logo.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';
import 'package:url_launcher/url_launcher.dart';

class SignUpPage extends StatefulWidget {
  SignUpPage({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final SignUpController controller;

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  bool _toggle = false;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;

    return RefreshIndicator(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).focusedChild?.unfocus(),
          child: Theme(
            data: Theme.of(context).copyWith(
              textTheme: Theme.of(context).textTheme.copyWith(
                    headline4: textTheme.headline4
                        ?.copyWith(color: colorScheme.onPrimary),
                    bodyText2: textTheme.bodyText2
                        ?.copyWith(color: colorScheme.onPrimary),
                    caption: textTheme.caption
                        ?.copyWith(color: colorScheme.onPrimary),
                  ),
            ),
            child: Scaffold(
              appBar: AppBar(
                title: Text("Criar Conta"),
                centerTitle: true,
              ),
              backgroundColor: Colors.white,
              body: Observer(builder: (context) {
                final textTheme = Theme.of(context).textTheme;

                if (widget.controller.isLoading) {
                  return Center(
                      child: CircularProgressIndicator(
                          color: colorScheme.onPrimary));
                }

                Type other = ShapeBorder;
                return Container(
                  height: double.infinity,
                  width: double.infinity,
                  child: ListView(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: Spacing.large),
                        child: Form(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              const _NameInput(),
                              const _EmailInput(),
                              const _PhoneInput(),
                              const _PasswordInput(),
                              const _RecoverPassword(),
                              const _TermsOfUse(),
                              const SizedBox(height: Spacing.xLarge),
                              const _CreateAccountButton(),
                              const SizedBox(height: Spacing.medium),
                              const AppVersionWithLogo(
                                color: Colors.black,
                                textColor: Colors.black,
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              }),
            ),
          ),
        ),
        onRefresh: () async {
          setState(() {
            _toggle = false;
          });
        });
  }
}

class _RecoverPassword extends StatelessWidget {
  const _RecoverPassword({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<SignUpController>();

    return Observer(builder: (_) {
      return CustomTextFormField(
        label: 'Repetir senha',
        initialValue: controller.passwordConfirmation,
        keyboardType: TextInputType.visiblePassword,
        onChanged: controller.validatePasswordConfirmation,
        obscureText: controller.obscurePasswordConfirmation,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey.shade200,
          errorText: controller.passwordConfirmationError,
          hintText: 'Informe uma senha',
          prefixIcon: const Icon(Icons.lock_outline),
          suffixIcon: IconButton(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onPressed: controller.toggleObscurePasswordConfirmation,
            icon: Icon(controller.obscurePasswordConfirmation
                ? Icons.visibility_off_outlined
                : Icons.visibility_outlined),
          ),
        ),
      );
    });
  }
}

class _PasswordInput extends StatelessWidget {
  const _PasswordInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<SignUpController>();

    return Observer(builder: (_) {
      return CustomTextFormField(
        label: 'Senha',
        initialValue: controller.password,
        keyboardType: TextInputType.visiblePassword,
        obscureText: controller.obscurePassword,
        onChanged: controller.validatePassword,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey.shade200,
          errorText: controller.passwordError,
          prefixIcon: const Icon(Icons.lock_outline),
          hintText: 'Informe uma senha',
          suffixIcon: IconButton(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onPressed: controller.toggleObscurePassword,
            icon: Icon(controller.obscurePassword
                ? Icons.visibility_off_outlined
                : Icons.visibility_outlined),
          ),
        ),
      );
    });
  }
}

class _PhoneInput extends StatelessWidget {
  const _PhoneInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<SignUpController>();

    return Observer(builder: (_) {
      return CustomTextFormField(
        label: 'Celular',
        initialValue: controller.phone,
        keyboardType: TextInputType.phone,
        onChanged: controller.validatePhone,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey.shade200,
          errorText: controller.phoneError,
          hintText: 'Informe um número de telefone',
          prefixIcon: const Icon(Icons.phone_android),
        ),
        inputFormatters: [
          FilteringTextInputFormatter.digitsOnly,
          brasil_fields.TelefoneInputFormatter(),
        ],
      );
    });
  }
}

class _NameInput extends StatelessWidget {
  const _NameInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<SignUpController>();

    return Observer(builder: (_) {
      return CustomTextFormField(
        label: 'Nome completo',
        initialValue: controller.name,
        keyboardType: TextInputType.name,
        onChanged: controller.validateName,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey.shade200,
          errorText: controller.nameError,
          hintText: 'Informe seu nome',
          prefixIcon: const Icon(Icons.person_outline),
        ),
      );
    });
  }
}

class _TermsOfUse extends StatelessWidget {
  const _TermsOfUse({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bodyText2 = Theme.of(context).textTheme.bodyText2;
    final controller = Modular.get<SignUpController>();

    return Row(
      children: [
        Flexible(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(
                flex: 1,
                child: Observer(builder: (context) {
                  return Checkbox(
                    checkColor: Colors.white,
                    fillColor: MaterialStateProperty.all(
                      Colors.blue.shade600,
                    ),
                    value: controller.checkbox,
                    onChanged: (bool? value) {
                      controller.setCheckbox(value);
                      print(controller.checkbox);
                    },
                  );
                }),
              ),
              Expanded(
                flex: 9,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    style: TextStyle(color: Colors.black, fontSize: 14),
                    text: 'Ao criar sua conta você concorda com os ',
                    children: [
                      TextSpan(
                        text: 'Termos de Uso ',
                        style: bodyText2?.copyWith(
                          color: Colors.blue.shade600,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline,
                        ),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () => launchUrl(
                              Uri.parse(AppConfig.getInstance().apiTermsUrl)),
                      ),
                      TextSpan(
                        text: ' e ',
                      ),
                      TextSpan(
                        text: ' Política de Privacidade.',
                        style: bodyText2?.copyWith(
                          color: Colors.blue.shade600,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline,
                        ),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () => launchUrl(
                              Uri.parse(AppConfig.getInstance().apiTermsUrl)),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class _CreateAccountButton extends StatelessWidget {
  const _CreateAccountButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<SignUpController>();

    return Observer(builder: (_) {
      return ElevatedButton(
        onPressed: controller.isFormValid ? controller.signup : null,
        child: const Text('Criar Conta'),
      );
    });
  }
}

class _EmailInput extends StatelessWidget {
  const _EmailInput({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<SignUpController>();

    return Observer(builder: (_) {
      return CustomTextFormField(
        label: 'E-mail',
        initialValue: controller.email,
        keyboardType: TextInputType.emailAddress,
        onChanged: controller.validateEmail,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey.shade200,
          errorText: controller.emailError,
          hintText: 'Insira seu e-mail',
          prefixIcon: const Icon(Icons.email_outlined),
        ),
      );
    });
  }
}
