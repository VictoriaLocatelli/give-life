import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/direct_selling/controllers/details/details_controller.dart';
import 'package:give_life/modules/direct_selling/controllers/direct_selling_controller.dart';
import 'package:give_life/modules/direct_selling/factories/load_trading_floor.dart';
import 'package:give_life/modules/direct_selling/factories/make_save_shot.dart';
import 'package:give_life/modules/direct_selling/views/details/details_trading_view.dart';
import 'package:give_life/modules/direct_selling/views/direct_selling_view.dart';

class DirectSale extends Module {
  @override
  List<Bind> get binds => [
        Bind.singleton((i) => makeRemoteLoadLots()),
        Bind.singleton((i) => makeRemoteSaveshot()),
        Bind.singleton((i) => DirectSaleController(loadLots: i())),
        Bind.singleton((i) => DetailsController(loadLot: i(), saveShot: i()))
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute('/',
            child: (_, __) => DirectSellingView(
                  controller: Modular.get<DirectSaleController>(),
                )),
        ChildRoute('/details/:id',
            child: (_, args) => DetailsTradingView(
                  id: int.parse(args.params['id']),
                  controller: Modular.get<DetailsController>(),
                ))
      ];
}
