import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/home/factories/load_auctions.dart';
import 'package:give_life/modules/home/model/entity_%20detail_auctions.dart';
import 'package:give_life/modules/home/model/lot_entity.dart';
import 'package:mobx/mobx.dart';

part 'details_controller.g.dart';

class DetailsController = DetailsControllerBase with _$DetailsController;

abstract class DetailsControllerBase with Store {
  DetailsControllerBase({required this.loadAuctions});
  final LoadAuctions loadAuctions;
  late BuildContext pageContext;
  @observable
  bool _isLoading = true;

  bool get isLoading => _isLoading;

  set isLoading(bool value) => _isLoading = value;
  List<LotEntity> lots = <LotEntity>[];
  DetailsAuctionsEntity? auction;

  String getPhone() =>
      auction?.phone != null ? phoneFormat(auction!.phone!) : "";

  String phoneFormat(String value) {
    final pattern = RegExp(r'(\d{2})(\d{5})(\d+)');
    final patternTwo = RegExp(r'(\d{2})(\d{4})(\d+)');

    return value.replaceAllMapped(value.length > 10 ? pattern : patternTwo,
        (Match m) => '(${m[1]}) ${m[2]}-${m[3]}');
  }

  void toLotDetails(int id) {
    Modular.to.pushNamed('/home/lotdetails/$id');
  }

  Future<void> loadData(int id) async {
    isLoading = true;
    try {
      auction = await loadAuctions.details(id);
      if (auction?.lots != null) {
        lots = auction!.lots!;
      }

      isLoading = false;
    } catch (e) {
      isLoading = false;
      print(e);
    }
  }
}
