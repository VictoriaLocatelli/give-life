import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';

class PropertyNameWidget extends StatelessWidget {
  const PropertyNameWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();
    controller;

    return Observer(builder: (_) {
      return CustomTextFormField(
        isRequired: false,
        label: 'Nome da Fazenda',
        initialValue:
            controller.propertyName != null ? controller.propertyName : "",
        onChanged: controller.propertyNameValidator,
        decoration: InputDecoration(
          errorText: controller.propertyNameError,
        ),
      );
    });
  }
}
