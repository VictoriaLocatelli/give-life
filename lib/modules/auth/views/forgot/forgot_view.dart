import 'package:flutter/material.dart';

import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/auth/controllers/forgot/forgot_controller.dart';
import 'package:give_life/shared/componets/constants/spacing.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';

class ForgotPage extends StatelessWidget {
  const ForgotPage({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final ForgotController controller;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return GestureDetector(
      onTap: () => FocusScope.of(context).focusedChild?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Recuperar senha'),
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          padding: const EdgeInsets.symmetric(
            vertical: Spacing.large,
            horizontal: Spacing.medium,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Esqueceu sua senha?',
                style:
                    textTheme.headline6?.copyWith(fontWeight: FontWeight.w900),
              ),
              const SizedBox(height: Spacing.small),
              const Text(
                  'Informe o e-mail da sua conta para recuperar sua senha.'),
              const SizedBox(height: Spacing.medium),
              const _EmailInput(),
              const SizedBox(height: Spacing.medium),
              const _SendButton(),
            ],
          ),
        ),
      ),
    );
  }
}

class _SendButton extends StatelessWidget {
  const _SendButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ForgotController>();

    return Observer(builder: (_) {
      return ElevatedButton(
        onPressed: controller.isFormValid ? controller.recover : null,
        child: controller.isLoading
            ? const CircularProgressIndicator()
            : const Text('Enviar'),
      );
    });
  }
}

class _EmailInput extends StatelessWidget {
  const _EmailInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ForgotController>();

    return Observer(builder: (_) {
      return CustomTextFormField(
        label: 'E-mail',
        initialValue: controller.email,
        onChanged: controller.validateEmail,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey.shade200,
          prefixIcon: const Icon(Icons.email_outlined),
          errorText: controller.emailError,
        ),
      );
    });
  }
}
