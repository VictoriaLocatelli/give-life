import 'package:give_life/modules/direct_selling/factories/save_shot.dart';
import 'package:give_life/modules/direct_selling/models/input_save_shot.dart';
import 'package:give_life/modules/direct_selling/models/lot_entity.dart';

import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';

class RemoteSaveShot implements SaveShot {
  const RemoteSaveShot({
    required this.url,
    required this.httpClient,
  });

  final String url;
  final HttpClient httpClient;

  @override
  Future<dynamic> save(InputSaveShot data) async {
    try {
      var resul = await httpClient.request(
          url: url, method: 'post', body: data.toJson());

      return resul;
    } catch (error) {
      return error;
    }
  }
}
