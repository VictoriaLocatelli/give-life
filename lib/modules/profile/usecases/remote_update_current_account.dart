import 'package:give_life/shared/componets/extensions/extensions.dart';
import 'package:give_life/modules/profile/contracts/update_current_account.dart';
import 'package:give_life/shared/core/entity/client_account_entitie.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/contracts/local/load_client_current_account.dart';
import 'package:give_life/shared/data/contracts/local/save_client_current_account.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';

class RemoteUpdateCurrentAccount implements UpdateCurrentAccount {
  const RemoteUpdateCurrentAccount({
    required this.url,
    required this.httpClient,
    required this.loadCurrentAccount,
    required this.saveCurrentAccount,
  });

  final String url;
  final HttpClient httpClient;
  final LoadClientCurrentAccount loadCurrentAccount;
  final SaveClientCurrentAccount saveCurrentAccount;

  @override
  Future<ClientAccountEntity> update() async {
    try {
      final response = await httpClient.request(url: url, method: 'get');

      final data = response['data'] as Map<String, dynamic>;

      final serializer = RemoteAcountProfileSerializer.fromMap(data);

      final account = (await loadCurrentAccount.load())!.copyWith(
        name: serializer.name,
        email: serializer.email,
        clientId: serializer.clientId,
        personId: serializer.personId,
      );

      await saveCurrentAccount.save(account);

      return account;
    } on HttpError {
      throw DomainError.unexpected;
    }
  }
}

class RemoteAccountProfileKeys {
  static const String name = 'name';
  static const String email = 'email';
  static const String clientId = 'people.client.id';
  static const String professionalId = 'people.professional.id';
  static const String personId = 'person_id';
}

class RemoteAcountProfileSerializer {
  const RemoteAcountProfileSerializer._({
    required this.name,
    required this.email,
    this.clientId,
    this.professionalId,
    this.personId,
  });

  final String name;
  final String email;
  final int? clientId;
  final int? professionalId;
  final int? personId;

  factory RemoteAcountProfileSerializer.fromMap(Map<String, dynamic> map) {
    final name = map.getValueByKeyList(RemoteAccountProfileKeys.name.split('.'))
        as String;
    final email = map
        .getValueByKeyList(RemoteAccountProfileKeys.email.split('.')) as String;
    final clientId =
        map.getValueByKeyList(RemoteAccountProfileKeys.clientId.split('.'))
            as int?;
    final professionalId = map.getValueByKeyList(
        RemoteAccountProfileKeys.professionalId.split('.')) as int?;
    final personId =
        map.getValueByKeyList(RemoteAccountProfileKeys.personId.split('.'))
            as int?;

    return RemoteAcountProfileSerializer._(
      name: name,
      email: email,
      clientId: clientId,
      professionalId: professionalId,
      personId: personId,
    );
  }
}
