enum AppEnvironment {
  production,
  development,
}

class AppConfig {
  final AppEnvironment appEnvironment;
  final String apiBaseUrl;
  final String apiStorageUrl;
  final String apiTermsUrl;
  final String appKey;

  AppConfig({
    required this.appEnvironment,
    required this.apiBaseUrl,
    required this.apiStorageUrl,
    required this.apiTermsUrl,
    required this.appKey,
  });

  static late AppConfig _instance;

  static AppConfig getInstance({AppConfig? config}) {
    if (config != null) {
      _instance = config;
      return _instance;
    }
    return _instance;
  }

  bool get isProd {
    if (_instance.appEnvironment == AppEnvironment.production)
      return true;
    else
      return false;
  }
}
