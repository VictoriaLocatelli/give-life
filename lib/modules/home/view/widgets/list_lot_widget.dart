import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/home/controller/details_controller.dart';
import 'package:give_life/shared/componets/theme/app_colors.dart';

class ListLotWidget extends StatelessWidget {
  const ListLotWidget({super.key, required this.lotName, required this.lotId});
  final String lotName;
  final int lotId;

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<DetailsController>();
    return Container(
      width: 100,
      height: 50,
      child: OutlinedButton(
          style: ButtonStyle(
              side: MaterialStateProperty.all(
                  BorderSide(color: AppColors.buttonGreenColor))),
          onPressed: () {
            controller.toLotDetails(lotId);
            Navigator.of(context).pop();
          },
          child: AutoSizeText(
            "$lotName",
            maxLines: 3,
            minFontSize: 10,
          )),
    );
  }
}
