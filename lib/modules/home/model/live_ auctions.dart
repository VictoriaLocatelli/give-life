import 'package:intl/intl.dart';

class AuctionsLive {
  int? id;
  String? name;
  String? description;
  String? catalog;
  String? regulation;
  String? phone;
  String? deskone;
  String? desktwo;
  String? deskthree;
  String? deskfour;
  String? deskfive;
  String? desksix;
  String? deskseven;
  String? deskeight;
  String? image;
  String? video;
  String? player;
  String? local;
  int? tenantid;
  String? website;

  AuctionsLive({
    this.id,
    this.name,
    this.description,
    this.catalog,
    this.regulation,
    this.phone,
    this.deskone,
    this.desktwo,
    this.deskthree,
    this.deskfour,
    this.deskfive,
    this.desksix,
    this.deskseven,
    this.deskeight,
    this.image,
    this.video,
    this.player,
    this.local,
    this.tenantid,
    this.website,
  });
  factory AuctionsLive.fromJson(Map<String, dynamic> json) {
    return AuctionsLive(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      image: json['image'],
      catalog: json['catalog'],
      regulation: json['regulation'],
      phone: json['phone'],
      deskone: json['deskone'],
      desktwo: json['desktwo'],
      deskthree: json['deskthree'],
      deskfour: json['deskfour'],
      deskfive: json['deskfive'],
      desksix: json['desksix'],
      deskseven: json['deskseven'],
      deskeight: json['deskeight'],
      video: json['video'],
      player: json['player'],
      local: json['local'],
      tenantid: json['tenantid'],
      website: json['website'],
    );
  }

  details(int id) {}
}
