import 'package:give_life/shared/componets/extensions/extensions.dart';
import 'package:give_life/shared/core/entity/city_entity.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/contracts/remotes/load_cities.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';

class RemoteLoadCities implements LoadCities {
  const RemoteLoadCities({
    required this.url,
    required this.httpClient,
  });

  final String url;
  final HttpClient httpClient;

  @override
  Future<List<CityEntity>> load() async {
    try {
      final response = await httpClient.request(url: url, method: 'get');

      final data = response['data'] as List;

      final results =
          data.map((e) => RemoteCitySerializer.fromMap(e).toEntity()).toList();

      return results;
    } on HttpError catch (_) {
      throw DomainError.unexpected;
    }
  }
}

// SERIALIZER

class RemoteCityKeys {
  static const String id = 'id';
  static const String name = 'title';
  static const String uf = 'letter';
}

class RemoteCitySerializer {
  const RemoteCitySerializer._({
    required this.id,
    required this.name,
    required this.uf,
  });

  final int id;
  final String name;
  final String uf;

  factory RemoteCitySerializer.fromMap(Map<String, dynamic> map) {
    try {
      final int id = map.getValueByKeyList(RemoteCityKeys.id.split('.')) as int;
      final String name =
          map.getValueByKeyList(RemoteCityKeys.name.split('.')) as String;
      final String uf =
          map.getValueByKeyList(RemoteCityKeys.uf.split('.')) as String;

      return RemoteCitySerializer._(
        id: id,
        name: name,
        uf: uf,
      );
    } catch (error) {
      throw HttpError.invalidData;
    }
  }

  CityEntity toEntity() => CityEntity(
        id: id,
        name: name,
        ufSigla: uf,
      );
}
