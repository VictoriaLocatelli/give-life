/*




catalog	:	null
regulation	:	null







modality	:	P
catalog_url	:	null
regulation_url	:	null

 */

class TradingFloorEntity {
  int? id;
  // String? image;
  // String? name;
  // String? description;
  // int? type;
  // String contact;
  // String? address;
  // String? zip_code;
  // String? catalog;
  // String? regulation;
  // String? tenant_id;
  // String? auction_house_id;
  // int? city_id;
  // int? status;
  String? desk_one;
  // String? desk_two;
  // int? is_enabled;
  // int? is_featured;
  // int? commission;
  String? created_at;
  String? updated_at;
  String? modality;
  String? catalog_url;
  String? regulation_url;

  TradingFloorEntity({
    this.id,
    // this.image,
    // this.name,
    // this.description,
    // this.type,
    // required this.contact,
    // this.address,
    // this.zip_code,
    // this.catalog,
    // this.regulation,
    // this.tenant_id,
    // this.auction_house_id,
    // this.city_id,
    // this.status,
    this.desk_one,
    // this.desk_two,
    // this.is_enabled,
    // this.is_featured,
    // this.commission,
    // this.created_at,
    // this.updated_at,
    this.modality,
    this.catalog_url,
    this.regulation_url,
  });

  TradingFloorEntity.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        // image = json['image'],
        // name = json['name'],
        // description = json['description'],
        // type = json['type'],
        // // contact = json['contact'],
        // address = json['address'],
        // zip_code = json['zip_code'],
        // catalog = json['catalog'],
        // regulation = json['regulation'],
        // tenant_id = json['tenant_id'],
        // auction_house_id = json['auction_house_id'],
        // city_id = json['city_id'],
        // status = json['status'],
        desk_one = json['desk_one'],
        // desk_two = json['desk_two'],
        // is_enabled = json['is_enabled'],
        // is_featured = json['is_featured'],
        // commission = json['commission'] is int
        //     ? json['commission'].toDouble()
        //     : json['commission'],
        created_at = json['created_at'],
        updated_at = json['updated_at'],
        modality = json['modality'],
        catalog_url = json['catalog_url'],
        regulation_url = json['regulation_url'];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    // data['name'] = name;
    // data['description'] = description;
    // data['type'] = type;
    // // data['contact'] = contact;
    // data['address'] = address;
    // data['zip_code'] = zip_code;
    // data['catalog'] = catalog;
    // data['regulation'] = regulation;
    // data['tenant_id'] = tenant_id;
    // data['auction_house_id'] = auction_house_id;
    // data['city_id'] = city_id;
    // data['status'] = status;
    data['desk_one'] = desk_one;
    // data['desk_two'] = desk_two;
    // data['is_enabled'] = is_enabled;
    // data['is_featured'] = is_featured;
    // data['commission'] = commission;
    data['created_at'] = created_at;
    data['updated_at'] = updated_at;
    data['modality'] = modality;
    data['catalog_url'] = catalog_url;
    data['regulation_url'] = regulation_url;
    return data;
  }
}
