import 'package:give_life/modules/profile/models/account_profile_model.dart';
import 'package:give_life/modules/profile/models/document_model.dart';
import 'package:give_life/modules/profile/models/property_model.dart';
import 'package:give_life/shared/app_config.dart';

import 'map_ext.dart';
import 'package:give_life/modules/profile/entity/client_profile_entity.dart';
import 'package:give_life/modules/profile/contracts/load_client_profile.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';

class RemoteLoadClientProfile implements LoadClientProfile {
  const RemoteLoadClientProfile({
    required this.url,
    required this.httpClient,
  });

  final String url;
  final HttpClient httpClient;

  @override
  Future<ClientProfileEntity> load() async {
    try {
      final response = await httpClient.request(url: url, method: 'get');
      response;

      final data = response['data'] as Map<String, dynamic>;
      final result = RemoteClientProfileSerializer.fromMap(data).toEntity();
      result;
      return result;
    } on HttpError {
      throw DomainError.unexpected;
    }
  }
}

//SERIALIZER

class RemoteClientProfileKeys {
  static const String id = 'id';
  static const String zipCode = 'zip_code';
  static const String name = 'name';
  static const String email = 'email';
  static const String phone1 = 'people.phone';
  static const String phone2 = 'people.telephone';
  static const String nif = 'people.nif';
  static const String address = 'people.address';
  static const String cityId = 'people.city_id';
  static const String photo = 'people.logo';
  static const String personId = 'person_id';
  static const String rg = 'state_registration';
  static const String district = 'district';
  static const String complement = 'complement';
  static const String number = 'number';
  static const String profession = 'profession'; //client.profession
  static const String property = "property";
  static const String account = "account";
  static const String documents = "documents";
  static const String date = "birthday";
}

class RemoteClientProfileSerializer {
  const RemoteClientProfileSerializer._(
      {required this.id,
      required this.name,
      required this.email,
      this.photo,
      this.phone1,
      this.personId,
      this.phone2,
      this.nif,
      this.address,
      this.cityId,
      this.zipCode,
      this.rg,
      this.district,
      this.complement,
      this.profession,
      this.property,
      this.account,
      this.date,
      this.number,
      required this.documents});

  final int? personId;
  final int id;
  final String name;
  final String? zipCode;
  final String? date;
  final String? photo;
  final String email;
  final String? phone1;
  final String? phone2;
  final String? nif;
  final String? rg;
  final String? address;
  final String? district;
  final String? complement;
  final String? number;
  final String? profession;
  final int? cityId;
  final PropertyModel? property;
  final AccountModel? account;
  final List<DocumentModel> documents;

  factory RemoteClientProfileSerializer.fromMap(Map<String, dynamic> map) {
    //Tear-offs
    DocumentModel toDocumentModel(Map<String, dynamic> document) =>
        DocumentModel.fromJson(document);

    try {
      final id = map['id'] as int;
      final name = map['name'] as String;
      final zipCode = map['zip_code'] as String?;

      final email = map['email'] as String;
      final phone1 = map['people']['phone'] as String?;
      final phone2 = map['people']['telephone'] as String?;
      final nif = map['people']['nif'] as String?;
      final address = map['people']['address'] as String?;
      final purePhoto = map['people']['logo'] as String?;
      final photo = purePhoto != null
          ? AppConfig.getInstance().apiStorageUrl + purePhoto
          : purePhoto;
      final cityId = map['people']['city_id'] as int?;
      final personId = map['person_id'] as int?;
      final rg = map['people'][RemoteClientProfileKeys.rg] as String?;
      final district =
          map['people'][RemoteClientProfileKeys.district] as String?;
      final number = map['people'][RemoteClientProfileKeys.number] as String?;
      final complement =
          map['people'][RemoteClientProfileKeys.complement] as String?;
      final profession = map['people']['client']
          ?[RemoteClientProfileKeys.profession] as String?;
      final propertyNull = map['people']['client']?['property'];
      final accountNull = map['people']['client']?["account"];
      final documentsNull = map['people']['client']?['documents'];
      final date = map['people']['client']?['birthday'];
      final property =
          propertyNull != null ? PropertyModel.fromJson(propertyNull) : null;
      final account =
          accountNull != null ? AccountModel.fromJson(accountNull) : null;
      final documents = documentsNull != null && documentsNull.length > 0
          ? documentsNull.map(toDocumentModel).toList()
          : [DocumentModel()] as List<DocumentModel>;

      return RemoteClientProfileSerializer._(
          id: id,
          name: name,
          zipCode: zipCode,
          email: email,
          phone1: phone1,
          phone2: phone2,
          nif: nif,
          address: address,
          cityId: cityId,
          personId: personId,
          photo: photo,
          rg: rg,
          district: district,
          complement: complement,
          profession: profession,
          property: property,
          account: account,
          documents: documents,
          date: date,
          number: number);
    } catch (error) {
      throw HttpError.invalidData;
    }
  }

  ClientProfileEntity toEntity() => ClientProfileEntity(
      id: id,
      name: name,
      email: email,
      phone1: phone1,
      phone2: phone2,
      nif: nif,
      address: address,
      cityId: cityId,
      personId: personId,
      photo: photo,
      zipCode: zipCode,
      rg: rg,
      complement: complement,
      profession: profession,
      property: property,
      documents: documents,
      account: account,
      date: date,
      district: district,
      number: number);
}
