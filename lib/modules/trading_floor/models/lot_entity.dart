import 'package:give_life/modules/trading_floor/models/lot_images_entity.dart';
import 'package:give_life/modules/trading_floor/models/shot_entity.dart';
import 'package:give_life/modules/trading_floor/models/trading_floor_entity.dart';

class LotEntity {
  int? id;
  int? type;
  String? name;
  String? description;
  String? age;
  String? bloodGrade;
  String? tatRp;
  String? ce;
  String? coat;
  String? salesman;
  String? gender;
  int? quantity;
  double? averageWeight;
  int? isEnabled;
  int? auctionId;
  int? animalFamilyId;
  int? animalCategoryId;
  int? animalBreedId;
  double? initValue;
  double? incValueOne;
  double? incValueTwo;
  double? incValueThree;
  double? incValueFour;
  double? targetBid;
  double? valueA;
  double? valueB;
  String? disputeTime;
  String? dateInit;
  String? dateEnd;
  String? hourInit;
  String? hourEnd;
  String? observation;
  int? status;
  String? createdAt;
  String? updatedAt;
  int? tradingFloorId;
  String? location;
  int? shotId;
  String? paymentForm;
  String? condition;
  int? typeLot;
  String? video;
  String? genealogy;
  String? family;
  String? category;
  String? breed;
  String? phone;
  List<LotImagesEntity>? images;
  TradingFloorEntity? tradingFloor;
  Shot? lastShot;
  List<dynamic>? shots;

  LotEntity({
    this.id,
    this.type,
    this.name,
    this.description,
    this.age,
    this.bloodGrade,
    this.tatRp,
    this.ce,
    this.coat,
    this.salesman,
    this.gender,
    this.quantity,
    this.averageWeight,
    this.isEnabled,
    this.auctionId,
    this.animalFamilyId,
    this.animalCategoryId,
    this.animalBreedId,
    this.initValue,
    this.incValueOne,
    this.incValueTwo,
    this.incValueThree,
    this.incValueFour,
    this.targetBid,
    this.valueA,
    this.valueB,
    this.disputeTime,
    this.dateInit,
    this.dateEnd,
    this.hourInit,
    this.hourEnd,
    this.observation,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.tradingFloorId,
    this.location,
    this.shotId,
    this.paymentForm,
    this.condition,
    this.typeLot,
    this.video,
    this.genealogy,
    this.family,
    this.category,
    this.breed,
    this.images,
    this.tradingFloor,
    this.lastShot,
    this.shots,
  });

  factory LotEntity.fromJson(Map<String, dynamic> json) {
    json;
    List<LotImagesEntity> img = [];
    img = [LotImagesEntity.fromDomain(json['images'][0])];
    List<Shot> shots = [];

    if (json['shots'] != null) {
      for (var shot in json['shots']) {
        shots.add(Shot.fromJson(shot));
      }
    }

    return LotEntity(
      id: json['id'],
      type: json['type'],
      name: json['name'],
      description: json['description'],
      age: json['age'],
      bloodGrade: json['blood_grade'],
      tatRp: json['tat_rp'],
      ce: json['ce'],
      coat: json['coat'],
      salesman: json['salesman'],
      gender: json['gender'],
      quantity: json['quantity'],
      averageWeight: json['average_weight'] is int
          ? json['average_weight'].toDouble()
          : json['average_weight'],
      isEnabled: json['is_enabled'],
      auctionId: json['auction_id'],
      animalFamilyId: json['animal_family_id'],
      animalCategoryId: json['animal_category_id'],
      animalBreedId: json['animal_breed_id'],
      initValue: json['init_value'] is int
          ? json['init_value'].toDouble()
          : json['init_value'],
      incValueOne: json['inc_value_one'] is int
          ? json['inc_value_one'].toDouble()
          : json['inc_value_one'],
      incValueTwo: json['inc_value_two'] is int
          ? json['inc_value_two'].toDouble()
          : json['inc_value_two'],
      incValueThree: json['inc_value_three'] is int
          ? json['inc_value_three'].toDouble()
          : json['inc_value_three'],
      incValueFour: json['inc_value_four'] is int
          ? json['inc_value_four'].toDouble()
          : json['inc_value_four'],
      targetBid: json['target_bid'] is int
          ? json['target_bid'].toDouble()
          : json['target_bid'],
      valueA:
          json['value_a'] is int ? json['value_a'].toDouble() : json['value_a'],
      valueB:
          json['value_b'] is int ? json['value_b'].toDouble() : json['value_b'],
      disputeTime: json['dispute_time'],
      dateInit: json['date_init'],
      dateEnd: json['date_end'],
      hourInit: json['hour_init'],
      hourEnd: json['hour_end'],
      observation: json['observation'],
      status: json['status'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
      tradingFloorId: json['trading_floor_id'],
      location: json['location'],
      shotId: json['shot_id'],
      paymentForm: json['payment_form'],
      condition: json['condition'],
      typeLot: json['type_lot'],
      video: json['video'],
      genealogy: json['genealogy'],
      family: json['family'],
      category: json['category'],
      breed: json['breed'],
      images: img,
      tradingFloor: json['trading_floor'] =
          TradingFloorEntity.fromJson(json['trading_floor']),
      lastShot:
          json['last_shot'] != null ? Shot.fromJson(json['last_shot']) : null,
      shots: shots,
    );
  }

  LotEntity? toList() {}
}
