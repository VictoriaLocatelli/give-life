import 'package:give_life/shared/core/entity/client_account_entitie.dart';

abstract class UpdateCurrentAccount {
  Future<ClientAccountEntity> update();
}
