import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/contracts/update_current_account.dart';
import 'package:give_life/modules/profile/usecases/remote_update_current_account.dart';
import 'package:give_life/shared/data/contracts/local/load_client_current_account.dart';
import 'package:give_life/shared/data/contracts/local/save_client_current_account.dart';
import 'package:give_life/shared/data/factories/api_url_factory.dart';
import 'package:give_life/shared/data/http/http.dart';

UpdateCurrentAccount makeRemoteUpdateCurrentAccount() =>
    RemoteUpdateCurrentAccount(
      url: makeApiUrl('/auth/profile'),
      httpClient: Modular.get<HttpClient>(),
      loadCurrentAccount: Modular.get<LoadClientCurrentAccount>(),
      saveCurrentAccount: Modular.get<SaveClientCurrentAccount>(),
    );
