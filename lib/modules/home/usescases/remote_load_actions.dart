import 'package:give_life/modules/home/factories/load_auctions.dart';
import 'package:give_life/modules/home/model/entity_%20auctions.dart';
import 'package:give_life/modules/home/model/entity_%20detail_auctions.dart';
import 'package:give_life/modules/home/model/live_sponsor.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';

class RemoteLoadAuctions implements LoadAuctions {
  const RemoteLoadAuctions({
    required this.url,
    required this.httpClient,
  });

  final String url;
  final HttpClient httpClient;

  @override
  Future<List<AuctionsEntity>?> load() async {
    try {
      List resul = (await httpClient.request(url: url, method: 'get'))['data'];
      final body = resul.map((lot) => AuctionsEntity.fromJson(lot)).toList();
      return body;
    } on HttpError {
      throw DomainError.unexpected;
    }
  }

  @override
  Future<DetailsAuctionsEntity?> details(int id) async {
    try {
      Map<String, dynamic> resul =
          (await httpClient.request(url: '$url/$id', method: 'get'))['data'];
      resul;
      final body = DetailsAuctionsEntity.fromJson(resul);
      body;
      return body;
    } on HttpError {
      throw DomainError.unexpected;
    }
  }

  @override
  Future<List<LiveSponsor?>> loadLiveSponson() async {
    try {
      var resul = (await httpClient.request(
          url: '$url?position=9', method: 'get'))['data'];
      resul;
      final body = resul.map((lot) => LiveSponsor.fromJson(lot)).toList();
      body;
      return body;
    } on HttpError {
      throw DomainError.unexpected;
    }
  }
}
