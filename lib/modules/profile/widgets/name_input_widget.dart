import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';

class NameInputWidget extends StatelessWidget {
  const NameInputWidget();

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();
    controller;

    return Observer(builder: (_) {
      return CustomTextFormField(
        label: 'Nome completo',
        initialValue: controller.name != null ? controller.name : "",
        onChanged: controller.nameValidator,
        decoration: InputDecoration(
          errorText: controller.nameError,
        ),
      );
    });
  }
}
