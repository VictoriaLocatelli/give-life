import 'package:give_life/modules/auth/models/forgot/recover_account_code.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';
import 'package:meta/meta.dart';

class RemoteRecoverAccountCode implements RecoverAccountCode {
  const RemoteRecoverAccountCode({
    required this.httpClient,
    required this.url,
  });

  final String url;
  final HttpClient httpClient;

  @override
  Future<String?> recover(InputRecoverAccountCode input) async {
    final body = InputRemoteRecoverAccountCode.fromDomain(input).toMap();

    try {
      var resul =
          await httpClient.request(url: url, method: 'post', body: body);
      return resul["data"]['code'] as String;
    } on HttpError catch (error) {
      throw error == HttpError.unauthorized
          ? DomainError.invalidCredentials
          : DomainError.unexpected;
    }
  }
}

@immutable
class InputRemoteRecoverAccountCode {
  const InputRemoteRecoverAccountCode(this.code);

  final String code;

  factory InputRemoteRecoverAccountCode.fromDomain(
          InputRecoverAccountCode input) =>
      InputRemoteRecoverAccountCode(input.code);

  Map<String, dynamic> toMap() => {'code': code};
}
