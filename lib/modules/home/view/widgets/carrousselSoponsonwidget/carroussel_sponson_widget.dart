import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:give_life/modules/home/model/live_sponsor.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:give_life/shared/componets/theme/app_colors.dart';
import 'package:give_life/shared/componets/utils/scaffold_messenger.dart';
import 'package:url_launcher/url_launcher.dart';

class CarrousselWidget extends StatefulWidget {
  const CarrousselWidget({super.key, required this.contents});
  final List<LiveSponsor?> contents;

  @override
  State<CarrousselWidget> createState() => _CarrousselWidgetState();
}

class _CarrousselWidgetState extends State<CarrousselWidget> {
  LiveSponsor? currentContent;

  get i => null;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.contents.length > 0) {
      currentContent = widget.contents[0];
    }
  }

  ImageProvider<Object> _imageChoice(LiveSponsor content) {
    if (content.imageurl != null) {
      return NetworkImage(content!.imageurl!);
    } else {
      return AssetImage(AppImages.noProfilePicture.path);
    }
  }

  String _urlChoice(LiveSponsor content) {
    if (content.url != null) {
      return content.url!;
    } else {
      return "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, c) {
      return Container(
        margin: const EdgeInsets.all(3),
        padding: const EdgeInsets.all(3),
        width: c.maxWidth,
        height: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
          border: Border.all(
            color: Color(0xffD9D9D9),
            style: BorderStyle.solid,
          ),
          color: AppColors.secondaryWhite,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            if (widget.contents.length > 0)
              Expanded(
                flex: 3,
                child: CarouselSlider(
                  items: widget.contents
                      .map((contents) => GestureDetector(
                          onTap: () {
                            launchUrl(
                              Uri.parse(_urlChoice(contents)),
                            );
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: _imageChoice(contents!))),
                          )))
                      .toList(),
                  options: CarouselOptions(autoPlay: true, height: 180),
                ),
              ),
          ],
        ),
      );
    });
  }
}
