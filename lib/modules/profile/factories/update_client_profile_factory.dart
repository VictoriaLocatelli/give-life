import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/entity/update_client_profile.dart';
import 'package:give_life/modules/profile/usecases/remote_update_client_profile.dart';
import 'package:give_life/shared/data/factories/api_url_factory.dart';
import 'package:give_life/shared/data/http/http_client.dart';

UpdateClientProfile makeRemoteUpdateClientProfile() =>
    RemoteUpdateClientProfile(
      url: makeApiUrl('/auth/profile'),
      httpClient: Modular.get<HttpClient>(),
    );
