import 'dart:async';

abstract class LoginPresenter {
  String? get emailError;
  String? get passwordError;
  String? get mainError;
  bool get formIsValid;
  bool get isObscurePassword;
  bool get isLoading;

  String? validateEmail(String? email);
  String? validatePassword(String? password);
  FutureOr<void> auth();
  void toggleObscurePassword();
  void dispose();
}
