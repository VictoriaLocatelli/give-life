import 'package:meta/meta.dart';

abstract class AddAccount {
  Future<void> add(InputAddAccount input);
}

@immutable
class InputAddAccount {
  const InputAddAccount(
      {required this.name,
      required this.email,
      required this.phone,
      required this.password,
      this.photo});

  final String name;
  final String email;
  final String phone;
  final String password;
  final String? photo;
}
