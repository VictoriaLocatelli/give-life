//
// Application-wide
//
import 'package:flutter/material.dart';

import 'constants.dart';

const genericPaddingPage = EdgeInsets.symmetric(
  horizontal: Spacing.medium,
  vertical: Spacing.large,
);

const double iconSize = 24;
const double smallIconSize = 16;
const double largeIconSize = iconSize * 2;
