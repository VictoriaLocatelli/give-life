class PurchaseModel {
    int? animalfamilyid;
    int? animalbreedid;

    PurchaseModel({this.animalfamilyid, this.animalbreedid}); 

    PurchaseModel.fromJson(Map<String, dynamic> json) {
        animalfamilyid = json['animal_family_id'];
        animalbreedid = json['animal_breed_id'];
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = Map<String, dynamic>();
        data['animal_family_id'] = animalfamilyid;
        data['animal_breed_id'] = animalbreedid;
        return data;
    }
}