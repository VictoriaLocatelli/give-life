import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/home/controller/details_controller.dart';
import 'package:give_life/modules/home/controller/home_controller.dart';
import 'package:give_life/modules/home/controller/live_controller.dart';
import 'package:give_life/modules/home/factories/load_make_auctions.dart';
import 'package:give_life/modules/home/factories/load_make_banners.dart';
import 'package:give_life/modules/home/view/details_lot_veiw.dart';
import 'package:give_life/modules/home/view/details_view.dart';
import 'package:give_life/modules/home/view/gallery_view.dart';
import 'package:give_life/modules/home/view/live_view.dart';
import 'package:give_life/modules/home/view/widgets/video_page.dart';

import '../view/home_view.dart';

class HomeRouter extends Module {
  @override
  List<Bind> get binds => [
        Bind.singleton((i) => makeRemoteLoadAuctions()),
        Bind.singleton((i) => makeRemoteLoadBanners()),
        Bind.singleton((i) => HomeController(loadAuctions: i())),
        Bind.singleton((i) => DetailsController(loadAuctions: i())),
        Bind.singleton((i) => LiveController(loadLive: i(), loadAuctions: i())),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute('/',
            child: (_, __) => HomeView(
                  controller: Modular.get<HomeController>(),
                )),
        ChildRoute('/details/:id',
            child: (_, args) => DetailsView(
                  controller: Modular.get<DetailsController>(),
                  id: int.parse(args.params['id']),
                )),
        ChildRoute('/live/:id',
            child: (_, args) => LiveView(
                  controller: Modular.get<LiveController>(),
                  id: int.parse(args.params['id']),
                )),
        ChildRoute('/lotdetails/:id',
            child: (_, args) => DetailsLotView(
                  lotId: int.parse(args.params['id']),
                )),
        ChildRoute('/showvideo/:url',
            child: (_, args) => VideoPage(
                  url: args.params['url'],
                )),
        ChildRoute('/gallery/:id',
            child: (_, args) => GalleryView(
                  id: args.params['id'],
                ))
      ];
}
