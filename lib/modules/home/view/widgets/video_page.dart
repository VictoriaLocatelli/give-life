import 'package:flutter/material.dart';
import 'package:give_life/modules/home/view/widgets/youtube_players_widget.dart';
import 'package:give_life/shared/componets/theme/app_colors.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class VideoPage extends StatelessWidget {
  final String url;

  VideoPage({required this.url});

  @override
  Widget build(BuildContext context) {
    return YoutubePlayerBuilder(
      player: YoutubePlayer(
        controller: YoutubePlayerController(
          initialVideoId: YoutubePlayer.convertUrlToId(url)!,
          flags: const YoutubePlayerFlags(
            autoPlay: false,
            mute: false,
          ),
        ),
        showVideoProgressIndicator: true,
        onReady: () => debugPrint('Redy'),
        bottomActions: [
          CurrentPosition(),
          ProgressBar(
            isExpanded: true,
            colors: const ProgressBarColors(
                playedColor: AppColors.primary,
                handleColor: AppColors.secondary),
          ),
          FullScreenButton(),
          const PlaybackSpeedButton(),
        ],
      ),
      builder: (context, player) => Scaffold(
        appBar: AppBar(
          title: Text("Mais Informações"),
        ),
        body: LayoutBuilder(builder: (context, c) {
          return Container(
            width: c.maxWidth,
            height: c.maxHeight,
            color: AppColors.black,
            child: Center(
              child: YoutubePlayerWidget(
                url: url,
              ),
            ),
          );
        }),
      ),
    );
  }
}
