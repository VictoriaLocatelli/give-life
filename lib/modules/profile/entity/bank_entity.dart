class BankEntity {
    int? id;
    String? code;
    String? name;
    String? createdat;
    String? updatedat;
    int? isenabled;

    BankEntity({this.id, this.code, this.name, this.createdat, this.updatedat, this.isenabled}); 

    BankEntity.fromJson(Map<String, dynamic> json) {
        id = json['id'];
        code = json['code'];
        name = json['name'];
        createdat = json['created_at'];
        updatedat = json['updated_at'];
        isenabled = json['is_enabled'];
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = Map<String, dynamic>();
        data['id'] = id;
        data['code'] = code;
        data['name'] = name;
        data['created_at'] = createdat;
        data['updated_at'] = updatedat;
        data['is_enabled'] = isenabled;
        return data;
    }
}