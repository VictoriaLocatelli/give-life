import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:give_life/modules/home/controller/live_controller.dart';
import 'package:give_life/modules/home/view/widgets/carrousselSoponsonwidget/carroussel_sponson_widget.dart';
import 'package:give_life/modules/home/view/widgets/dialogs_widget.dart';
import 'package:give_life/modules/home/view/widgets/youtube_players_widget.dart';
import 'package:give_life/shared/app_config.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:give_life/shared/componets/theme/app_colors.dart';
import 'package:give_life/shared/componets/utils/launch_link.dart';
import 'package:give_life/shared/componets/utils/scaffold_messenger.dart';
import 'package:give_life/shared/componets/widgets/fields/network_fade_image_widget.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class LiveView extends StatefulWidget {
  const LiveView({super.key, required this.controller, required this.id});
  final int id;
  final LiveController controller;

  @override
  State<LiveView> createState() => _LiveViewState();
}

class _LiveViewState extends State<LiveView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.controller.loadData(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;
    final textButtonStyle = TextButton.styleFrom(
      backgroundColor: Colors.transparent,
      alignment: Alignment.centerLeft,
    );
    return Observer(builder: (context) {
      widget.controller.pageContext = context;
      if (widget.controller.isLoading)
        return const Center(child: CircularProgressIndicator());
      String baseUrl = AppConfig.getInstance().apiStorageUrl;
      String url = '$baseUrl${widget.controller.auction?.image}';
      bool hasImage = true;

      if (widget.controller.auction?.image == null) {
        hasImage = false;
      }

      String deskone = widget.controller.getPhone();

      int? day = widget.controller.auction?.date.day;
      int? month = widget.controller.auction?.date.month;
      int? year = widget.controller.auction?.date.year;
      String catalog = widget.controller.auction?.catalog ?? "";
      String baseStorageUrl = AppConfig.getInstance().apiStorageUrl;
      String regulation = widget.controller.auction?.regulation ?? "";

      TextStyle styleTitle = TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 25,
      );
      if (widget.controller.auction?.link != null) {
        return YoutubePlayerBuilder(
          player: YoutubePlayer(
            controller: YoutubePlayerController(
              initialVideoId: YoutubePlayer.convertUrlToId(
                  widget.controller.auction!.link!)!,
              flags: const YoutubePlayerFlags(
                autoPlay: true,
                mute: false,
              ),
            ),
            showVideoProgressIndicator: true,
            onReady: () => debugPrint('Redy'),
            bottomActions: [
              CurrentPosition(),
              ProgressBar(
                isExpanded: true,
                colors: const ProgressBarColors(
                    playedColor: AppColors.primary,
                    handleColor: AppColors.secondary),
              ),
              FullScreenButton(),
              const PlaybackSpeedButton(),
            ],
          ),
          builder: (context, player) => Scaffold(
            appBar: AppBar(
              title: Column(children: [
                Text("Leilão ao Vivo"),
                Text(
                  widget.controller.auction!.name!,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                )
              ]),
              centerTitle: true,
            ),
            body: Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: LayoutBuilder(builder: (context, c) {
                return Container(
                    width: c.maxWidth,
                    child: SingleChildScrollView(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Column(
                          children: [
                            Container(
                              width: c.maxWidth,
                              height: c.maxWidth + 60,
                              child: widget.controller.auction?.link != null
                                  ? YoutubePlayerWidget(
                                      url: widget.controller.auction!.link!)
                                  : widget.controller.auction?.image != null
                                      ? NetWorkFadeImageWidget(
                                          url: url,
                                          widget: Center(
                                            child: Text(
                                              '${widget.controller.auction?.name}'
                                              ' - '
                                              '${widget.controller.auction?.city}',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 22),
                                            ),
                                          ),
                                        )
                                      : Container(
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(AppImages
                                                      .noProfilePicture.path))),
                                        ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Faça seu Lance.',
                                      style: styleTitle,
                                    ),
                                    Text(
                                        'Clique em um dos telefones para fazer o seu lance, agora mesmo',
                                        style: TextStyle(
                                          fontSize: 17,
                                        )),
                                    SizedBox(
                                      height: 15,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              height: 250,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 15),
                                child: Row(
                                  children: [
                                    SizedBox(
                                      width: 25,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        TextButton.icon(
                                          label: Text(
                                            widget.controller.phoneFormat(widget
                                                    .controller
                                                    .auction!
                                                    .deskone! ??
                                                ""),
                                            style: textTheme.bodyText1
                                                ?.copyWith(fontSize: 14),
                                          ),
                                          icon: Icon(
                                            Icons.whatsapp,
                                            color: Colors.green,
                                          ),
                                          style: textButtonStyle,
                                          onPressed: () async => launchWhatsApp(
                                            widget.controller.auction!.deskone!,
                                            onFailLaunchingUrl: (value) =>
                                                showSnackBar(
                                              context,
                                              content: Text(value),
                                              type: SnackBarType.error,
                                            ),
                                          ),
                                        ),
                                        TextButton.icon(
                                          label: Text(
                                            widget.controller.phoneFormat(widget
                                                    .controller
                                                    .auction!
                                                    .deskthree! ??
                                                ""),
                                            style: textTheme.bodyText1
                                                ?.copyWith(fontSize: 14),
                                          ),
                                          icon: Icon(
                                            Icons.whatsapp,
                                            color: Colors.green,
                                          ),
                                          style: textButtonStyle,
                                          onPressed: () async => launchWhatsApp(
                                            widget
                                                .controller.auction!.deskthree!,
                                            onFailLaunchingUrl: (value) =>
                                                showSnackBar(
                                              context,
                                              content: Text(value),
                                              type: SnackBarType.error,
                                            ),
                                          ),
                                        ),
                                        TextButton.icon(
                                          label: Text(
                                            widget.controller.phoneFormat(widget
                                                    .controller
                                                    .auction!
                                                    .deskfive! ??
                                                ""),
                                            style: textTheme.bodyText1
                                                ?.copyWith(fontSize: 14),
                                          ),
                                          icon: Icon(
                                            Icons.whatsapp,
                                            color: Colors.green,
                                          ),
                                          style: textButtonStyle,
                                          onPressed: () async => launchWhatsApp(
                                            widget
                                                .controller.auction!.deskfive!,
                                            onFailLaunchingUrl: (value) =>
                                                showSnackBar(
                                              context,
                                              content: Text(value),
                                              type: SnackBarType.error,
                                            ),
                                          ),
                                        ),
                                        TextButton.icon(
                                          label: Text(
                                            widget.controller.phoneFormat(widget
                                                    .controller
                                                    .auction!
                                                    .deskseven! ??
                                                ""),
                                            style: textTheme.bodyText1
                                                ?.copyWith(fontSize: 14),
                                          ),
                                          icon: Icon(
                                            Icons.whatsapp,
                                            color: Colors.green,
                                          ),
                                          style: textButtonStyle,
                                          onPressed: () async => launchWhatsApp(
                                            widget
                                                .controller.auction!.deskseven!,
                                            onFailLaunchingUrl: (value) =>
                                                showSnackBar(
                                              context,
                                              content: Text(value),
                                              type: SnackBarType.error,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        TextButton.icon(
                                          label: Text(
                                            widget.controller.phoneFormat(widget
                                                    .controller
                                                    .auction!
                                                    .desktwo! ??
                                                ""),
                                            style: textTheme.bodyText1
                                                ?.copyWith(fontSize: 14),
                                          ),
                                          icon: Icon(
                                            Icons.whatsapp,
                                            color: Colors.green,
                                          ),
                                          style: textButtonStyle,
                                          onPressed: () async => launchWhatsApp(
                                            widget.controller.auction!.desktwo!,
                                            onFailLaunchingUrl: (value) =>
                                                showSnackBar(
                                              context,
                                              content: Text(value),
                                              type: SnackBarType.error,
                                            ),
                                          ),
                                        ),
                                        TextButton.icon(
                                          label: Text(
                                            widget.controller.phoneFormat(widget
                                                    .controller
                                                    .auction!
                                                    .deskfour! ??
                                                ""),
                                            style: textTheme.bodyText1
                                                ?.copyWith(fontSize: 14),
                                          ),
                                          icon: Icon(
                                            Icons.whatsapp,
                                            color: Colors.green,
                                          ),
                                          style: textButtonStyle,
                                          onPressed: () async => launchWhatsApp(
                                            widget
                                                .controller.auction!.deskfour!,
                                            onFailLaunchingUrl: (value) =>
                                                showSnackBar(
                                              context,
                                              content: Text(value),
                                              type: SnackBarType.error,
                                            ),
                                          ),
                                        ),
                                        TextButton.icon(
                                          label: Text(
                                            widget.controller.phoneFormat(widget
                                                    .controller
                                                    .auction!
                                                    .desksix! ??
                                                ""),
                                            style: textTheme.bodyText1
                                                ?.copyWith(fontSize: 14),
                                          ),
                                          icon: Icon(
                                            Icons.whatsapp,
                                            color: Colors.green,
                                          ),
                                          style: textButtonStyle,
                                          onPressed: () async => launchWhatsApp(
                                            widget.controller.auction!.desksix!,
                                            onFailLaunchingUrl: (value) =>
                                                showSnackBar(
                                              context,
                                              content: Text(value),
                                              type: SnackBarType.error,
                                            ),
                                          ),
                                        ),
                                        TextButton.icon(
                                          label: Text(
                                            widget.controller.phoneFormat(widget
                                                    .controller
                                                    .auction!
                                                    .deskeight! ??
                                                ""),
                                            style: textTheme.bodyText1
                                                ?.copyWith(fontSize: 14),
                                          ),
                                          icon: Icon(
                                            Icons.whatsapp,
                                            color: Colors.green,
                                          ),
                                          style: textButtonStyle,
                                          onPressed: () async => launchWhatsApp(
                                            widget
                                                .controller.auction!.deskeight!,
                                            onFailLaunchingUrl: (value) =>
                                                showSnackBar(
                                              context,
                                              content: Text(value),
                                              type: SnackBarType.error,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  width: 215,
                                  child: ElevatedButton(
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                AppColors.button),
                                      ),
                                      onPressed: () {
                                        print("$catalog print catálogo");
                                        if (catalog != "") {
                                          launch(baseStorageUrl + catalog);
                                        } else {
                                          alertModal(context,
                                              "Este leilão não possui catálogo ");
                                        }
                                      },
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text('          CATÁLOGO         '),
                                          Image.asset(AppImages.catalog.path),
                                        ],
                                      )),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  width: 215,
                                  child: ElevatedButton(
                                      style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                AppColors.button),
                                      ),
                                      onPressed: () {
                                        if (regulation != "") {
                                          launch(baseStorageUrl + regulation);
                                        } else {
                                          alertModal(context,
                                              "Este leilão não possui regulamento!");
                                        }
                                      },
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Text('     REGULAMENTO      '),
                                          Image.asset(
                                              AppImages.regulamention.path),
                                        ],
                                      )),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                    width: 330,
                                    child: AutoSizeText(
                                      'Patrocinadores',
                                      style: styleTitle,
                                    )),
                                SizedBox(
                                  height: 20,
                                ),
                                if (widget.controller.auction?.contents != null)
                                  CarrousselWidget(
                                    contents: widget.controller.live,
                                  )
                              ],
                            ),
                          ],
                        ),
                      ],
                    )));
              }),
            ),
          ),
        );
      } else {
        return Scaffold(
          appBar: AppBar(
            title: Column(children: [
              Text("Leilão ao Vivo"),
              Text(
                widget.controller.auction!.name!,
                style: TextStyle(
                  fontSize: 12,
                ),
              )
            ]),
            centerTitle: true,
          ),
          body: Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: LayoutBuilder(builder: (context, c) {
              return Container(
                  width: c.maxWidth,
                  child: SingleChildScrollView(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Column(
                        children: [
                          Container(
                            width: c.maxWidth,
                            height: c.maxWidth + 60,
                            child: widget.controller.auction?.link != null
                                ? YoutubePlayerWidget(
                                    url: widget.controller.auction!.link!)
                                : widget.controller.auction?.image != null
                                    ? NetWorkFadeImageWidget(
                                        url: url,
                                        widget: Center(
                                          child: Text(
                                            '${widget.controller.auction?.name}'
                                            ' - '
                                            '${widget.controller.auction?.city}',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 22),
                                          ),
                                        ),
                                      )
                                    : Container(
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: AssetImage(AppImages
                                                    .noProfilePicture.path))),
                                      ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    'Faça seu Lance.',
                                    style: styleTitle,
                                  ),
                                  Text(
                                      'Clique em um dos telefones para fazer o seu lance, agora mesmo',
                                      style: TextStyle(
                                        fontSize: 17,
                                      )),
                                  SizedBox(
                                    height: 15,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            height: 250,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 15),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 25,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      TextButton.icon(
                                        label: Text(
                                          widget.controller.phoneFormat(widget
                                                  .controller
                                                  .auction!
                                                  .deskone! ??
                                              ""),
                                          style: textTheme.bodyText1
                                              ?.copyWith(fontSize: 14),
                                        ),
                                        icon: Icon(
                                          Icons.whatsapp,
                                          color: Colors.green,
                                        ),
                                        style: textButtonStyle,
                                        onPressed: () async => launchWhatsApp(
                                          widget.controller.auction!.deskone!,
                                          onFailLaunchingUrl: (value) =>
                                              showSnackBar(
                                            context,
                                            content: Text(value),
                                            type: SnackBarType.error,
                                          ),
                                        ),
                                      ),
                                      TextButton.icon(
                                        label: Text(
                                          widget.controller.phoneFormat(widget
                                                  .controller
                                                  .auction!
                                                  .deskthree! ??
                                              ""),
                                          style: textTheme.bodyText1
                                              ?.copyWith(fontSize: 14),
                                        ),
                                        icon: Icon(
                                          Icons.whatsapp,
                                          color: Colors.green,
                                        ),
                                        style: textButtonStyle,
                                        onPressed: () async => launchWhatsApp(
                                          widget.controller.auction!.deskthree!,
                                          onFailLaunchingUrl: (value) =>
                                              showSnackBar(
                                            context,
                                            content: Text(value),
                                            type: SnackBarType.error,
                                          ),
                                        ),
                                      ),
                                      TextButton.icon(
                                        label: Text(
                                          widget.controller.phoneFormat(widget
                                                  .controller
                                                  .auction!
                                                  .deskfive! ??
                                              ""),
                                          style: textTheme.bodyText1
                                              ?.copyWith(fontSize: 14),
                                        ),
                                        icon: Icon(
                                          Icons.whatsapp,
                                          color: Colors.green,
                                        ),
                                        style: textButtonStyle,
                                        onPressed: () async => launchWhatsApp(
                                          widget.controller.auction!.deskfive!,
                                          onFailLaunchingUrl: (value) =>
                                              showSnackBar(
                                            context,
                                            content: Text(value),
                                            type: SnackBarType.error,
                                          ),
                                        ),
                                      ),
                                      TextButton.icon(
                                        label: Text(
                                          widget.controller.phoneFormat(widget
                                                  .controller
                                                  .auction!
                                                  .deskseven! ??
                                              ""),
                                          style: textTheme.bodyText1
                                              ?.copyWith(fontSize: 14),
                                        ),
                                        icon: Icon(
                                          Icons.whatsapp,
                                          color: Colors.green,
                                        ),
                                        style: textButtonStyle,
                                        onPressed: () async => launchWhatsApp(
                                          widget.controller.auction!.deskseven!,
                                          onFailLaunchingUrl: (value) =>
                                              showSnackBar(
                                            context,
                                            content: Text(value),
                                            type: SnackBarType.error,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      TextButton.icon(
                                        label: Text(
                                          widget.controller.phoneFormat(widget
                                                  .controller
                                                  .auction!
                                                  .desktwo! ??
                                              ""),
                                          style: textTheme.bodyText1
                                              ?.copyWith(fontSize: 14),
                                        ),
                                        icon: Icon(
                                          Icons.whatsapp,
                                          color: Colors.green,
                                        ),
                                        style: textButtonStyle,
                                        onPressed: () async => launchWhatsApp(
                                          widget.controller.auction!.desktwo!,
                                          onFailLaunchingUrl: (value) =>
                                              showSnackBar(
                                            context,
                                            content: Text(value),
                                            type: SnackBarType.error,
                                          ),
                                        ),
                                      ),
                                      TextButton.icon(
                                        label: Text(
                                          widget.controller.phoneFormat(widget
                                                  .controller
                                                  .auction!
                                                  .deskfour! ??
                                              ""),
                                          style: textTheme.bodyText1
                                              ?.copyWith(fontSize: 14),
                                        ),
                                        icon: Icon(
                                          Icons.whatsapp,
                                          color: Colors.green,
                                        ),
                                        style: textButtonStyle,
                                        onPressed: () async => launchWhatsApp(
                                          widget.controller.auction!.deskfour!,
                                          onFailLaunchingUrl: (value) =>
                                              showSnackBar(
                                            context,
                                            content: Text(value),
                                            type: SnackBarType.error,
                                          ),
                                        ),
                                      ),
                                      TextButton.icon(
                                        label: Text(
                                          widget.controller.phoneFormat(widget
                                                  .controller
                                                  .auction!
                                                  .desksix! ??
                                              ""),
                                          style: textTheme.bodyText1
                                              ?.copyWith(fontSize: 14),
                                        ),
                                        icon: Icon(
                                          Icons.whatsapp,
                                          color: Colors.green,
                                        ),
                                        style: textButtonStyle,
                                        onPressed: () async => launchWhatsApp(
                                          widget.controller.auction!.desksix!,
                                          onFailLaunchingUrl: (value) =>
                                              showSnackBar(
                                            context,
                                            content: Text(value),
                                            type: SnackBarType.error,
                                          ),
                                        ),
                                      ),
                                      TextButton.icon(
                                        label: Text(
                                          widget.controller.phoneFormat(widget
                                                  .controller
                                                  .auction!
                                                  .deskeight! ??
                                              ""),
                                          style: textTheme.bodyText1
                                              ?.copyWith(fontSize: 14),
                                        ),
                                        icon: Icon(
                                          Icons.whatsapp,
                                          color: Colors.green,
                                        ),
                                        style: textButtonStyle,
                                        onPressed: () async => launchWhatsApp(
                                          widget.controller.auction!.deskeight!,
                                          onFailLaunchingUrl: (value) =>
                                              showSnackBar(
                                            context,
                                            content: Text(value),
                                            type: SnackBarType.error,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                width: 215,
                                child: ElevatedButton(
                                    style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              AppColors.button),
                                    ),
                                    onPressed: () {
                                      print("$catalog print catálogo");
                                      if (catalog != "") {
                                        launch(baseStorageUrl + catalog);
                                      } else {
                                        alertModal(context,
                                            "Este leilão não possui catálogo ");
                                      }
                                    },
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Text('          CATÁLOGO         '),
                                        Image.asset(AppImages.catalog.path),
                                      ],
                                    )),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                width: 215,
                                child: ElevatedButton(
                                    style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              AppColors.button),
                                    ),
                                    onPressed: () {
                                      if (regulation != "") {
                                        launch(baseStorageUrl + regulation);
                                      } else {
                                        alertModal(context,
                                            "Este leilão não possui regulamento!");
                                      }
                                    },
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Text('     REGULAMENTO      '),
                                        Image.asset(
                                            AppImages.regulamention.path),
                                      ],
                                    )),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                  width: 330,
                                  child: AutoSizeText(
                                    'Patrocinadores',
                                    style: styleTitle,
                                  )),
                              SizedBox(
                                height: 20,
                              ),
                              if (widget.controller.auction?.contents != null)
                                CarrousselWidget(
                                  contents: widget.controller.live,
                                )
                            ],
                          ),
                        ],
                      ),
                    ],
                  )));
            }),
          ),
        );
      }
    });
  }
}
