import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/modules/profile/controllers/initial_client_profile.dart';
import 'package:give_life/modules/profile/factories/load_bank_factory.dart';
import 'package:give_life/modules/profile/factories/load_client_profile_factory.dart';
import 'package:give_life/modules/profile/factories/update_client_profile_factory.dart';
import 'package:give_life/modules/profile/factories/update_current_account_factory.dart';
import 'package:give_life/modules/profile/views/client_profile_edit_view.dart';
import 'package:give_life/modules/profile/views/client_profile_view.dart';

class ProfileModule extends Module {
  @override
  List<Module> get imports => const [];

  @override
  List<Bind> get binds => [
        Bind.singleton((i) => makeRemoteUpdateCurrentAccount()),
        Bind.singleton((i) => makeRemoteLoadClientProfile()),
        Bind.singleton((i) => makeRemoteUpdateClientProfile()),
        Bind.singleton((i) => makeLoadBank()),
        Bind.singleton((i) => InitialClientProfile(loadClient: i())),
        Bind.singleton((i) => ClientProfileController(
              loadClientProfile: i(),
              updateClientProfile: i(),
              loadCities: i(),
              loadBanks: i(),
            ))
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute('/',
            child: (_, __) => ClientProfileView(
                  controller: Modular.get<InitialClientProfile>(),
                )),
        ChildRoute('/edit',
            child: (_, __) => ClientProfileEditView(
                  controller: Modular.get<ClientProfileController>(),
                ))
      ];
}
