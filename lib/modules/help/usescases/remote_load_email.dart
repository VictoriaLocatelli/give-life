import 'package:give_life/modules/help/models/load_email.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';

class RemoteLoadEmail implements LoadEmail {
  const RemoteLoadEmail({
    required this.url,
    required this.httpClient,
  });

  final List<String> url;
  final HttpClient httpClient;

  @override
  Future<Map<String, dynamic>> load() async {
    Map<String, dynamic> data = {};
    try {
      Map<String, dynamic>? resul =
          (await httpClient.request(url: url[2], method: 'get'))['data'];
      if (resul != null) {
        data['EMAIL'] = resul['email'];
        data['CONTATO'] = resul['phone'];
      } else {
        final responseEmail =
            await httpClient.request(url: url[0], method: 'get');
        final responseContato =
            await httpClient.request(url: url[1], method: 'get');

        if (responseEmail != null && responseContato != null) {
          data['EMAIL'] =
              responseEmail['data']['value'] ?? "Nenhum e-mail cadstrado";
          data['CONTATO'] =
              responseContato['data']['value'] ?? "Nenhum contato cadstrado";
        } else {
          data['EMAIL'] = "Nenhum e-mail cadstrado";
          data['CONTATO'] = "Nenhum contato cadstrado";
        }
      }

      return data;
    } on HttpError {
      throw DomainError.unexpected;
    }
  }
}
