// ignore_for_file: unused_element

import 'package:mobx/mobx.dart';

part 'client_profile_vm.g.dart';

class ClientProfileVM = _ClientProfileVMBase with _$ClientProfileVM;

abstract class _ClientProfileVMBase with Store {
  _ClientProfileVMBase({
    this.name,
    this.email,
    this.phone1,
    this.nif,
    this.address,
    this.city,
    this.zipCode,
    this.photo,
    this.rg,
    this.propertyName,
    this.propertyCity,
    this.propertyAddress,
    this.propertyStateRegistration,
    this.bankId,
    this.bankAccount,
    this.bankAgency,
    this.bankType,
    this.date,
    this.number,
    this.district,
    this.complement
  });


  @observable
  String? name;

  @observable
  String? district;
  @observable
  String? districtError;
  @observable
  String? complement;
  @observable
  String? complementError;

  @observable
  String? propertyName;
  @observable
  String? propertyNameError;

  @observable
  String? propertyStateRegistration;
  @observable
  String? propertyStateRegistrationError;

  @observable
  String? photo;

  @observable
  String? zipCode;

  @observable
  String? zipCodeError;

  @observable
  String? nameError;

  @observable
  String? email;
  @observable
  String? emailError;

  @observable
  String? phone1;
  @observable
  String? phone1Error;

  @observable
  String? nif;
  @observable
  String? nifError;

  @observable
  String? rg;
  @observable
  String? rgError;

  @observable
  String? address;
  @observable
  String? addressError;
  @observable
  String? propertyAddress;
  @observable
  String? propertyAddressError;

  @observable
  int? city;
  @observable
  String? cityError;
  @observable
  int? propertyCity;
  @observable
  String? propertyCityError;


  //BANK
  @observable
  int? bankId;
  @observable
  String? bankIdError;

  @observable
  String? bankAccount;
  @observable
  String? bankAccountError;
  @observable
  int? bankType;
  @observable
  int? bankTypeError;
  @observable
  String? bankAgency;
  @observable
  String? bankAgencyError;

  @observable
  String? date;
  @observable
  String? dateError;

  @observable
  String? number;
  @observable
  String? numberError;


}
