import 'package:intl/intl.dart';

class LiveSponsor {
  int? id;
  int? position;
  String? imageurl;
  String? url;

  LiveSponsor({
    this.id,
    this.position,
    this.imageurl,
    this.url,
  });
  factory LiveSponsor.fromJson(Map<String, dynamic> json) {
    json;
    return LiveSponsor(
      id: json['id'],
      position: json['position'],
      imageurl: json['image_url'],
      url: json['url'],
    );
  }
}
