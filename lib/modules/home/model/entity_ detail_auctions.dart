import 'package:give_life/modules/home/model/contents_entity.dart';
import 'package:give_life/modules/home/model/lot_entity.dart';

class DetailsAuctionsEntity {
  int id;
  int? type;
  String? name;
  String? description;
  String? image;
  int isOnline;
  DateTime date;
  String time;
  String? local;
  String? address;
  String? auctionhouse;
  String city;
  String? phone;
  String? freePhone;
  String? catalog;
  String? regulation;
  List<ContentsEntity?>? contents;
  int isEnabled;
  String? landline;
  String? deskone;
  String? desktwo;
  String? deskthree;
  String? deskfour;
  String? deskfive;
  String? desksix;
  String? deskseven;
  String? deskeight;
  String? link;
  List<LotEntity>? lots;

  DetailsAuctionsEntity({
    required this.id,
    required this.isOnline,
    required this.date,
    required this.time,
    required this.city,
    this.description,
    required this.isEnabled,
    this.auctionhouse,
    this.phone,
    this.freePhone,
    this.catalog,
    this.regulation,
    this.type,
    this.name,
    this.image,
    this.local,
    this.address,
    this.lots,
    this.contents,
    this.landline,
    this.deskone,
    this.desktwo,
    this.deskthree,
    this.deskfour,
    this.deskfive,
    this.desksix,
    this.deskseven,
    this.deskeight,
    this.link,
  });

  factory DetailsAuctionsEntity.fromJson(Map<String, dynamic> json) {
    List<ContentsEntity> contents = <ContentsEntity>[];
    List<LotEntity> lots = [];
    if (json['lots'] != null) {
      for (var lot in json['lots']) {
        lots.add(LotEntity.fromJson(lot));
      }
    }
    for (var content in json['contents']) {
      contents.add(ContentsEntity.fromJson(content));
    }
    return DetailsAuctionsEntity(
      id: json['id'],
      type: json['type'],
      name: json['name'],
      description: json['description'],
      image: json['image'],
      isOnline: json['is_online'],
      date: DateTime.parse(json['date']).toLocal(),
      time: json['time'],
      address: json['address'],
      auctionhouse: json['auctionhouse'],
      city: json['city'],
      phone: json['phone'],
      local: json['local'],
      freePhone: json['free_phone'],
      regulation: json['regulation'],
      catalog: json['catalog'],
      isEnabled: json['is_enabled'],
      lots: lots,
      contents: contents,
      landline: json['landline'],
      deskone: json['desk_one'],
      desktwo: json['desk_two'],
      deskthree: json['desk_three'],
      deskfour: json['desk_four'],
      deskfive: json['desk_five'],
      desksix: json['desk_six'],
      deskseven: json['desk_seven'],
      deskeight: json['desk_eight'],
      link: json['link'],
    );
  }
}
