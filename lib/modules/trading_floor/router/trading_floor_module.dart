import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/trading_floor/controllers/details/details_controller.dart';
import 'package:give_life/modules/trading_floor/controllers/gallery/gallery_controller.dart';
import 'package:give_life/modules/trading_floor/controllers/shots/shots_controller.dart';
import 'package:give_life/modules/trading_floor/controllers/trading_floor_controller.dart';
import 'package:give_life/modules/trading_floor/factories/load_trading_floor.dart';
import 'package:give_life/modules/trading_floor/factories/make_save_shot.dart';
import 'package:give_life/modules/trading_floor/views/details/details_trading_view.dart';
import 'package:give_life/modules/trading_floor/views/gallery/gallery_trading_view.dart';
import 'package:give_life/modules/trading_floor/views/shots/shots_trading_view.dart';
import 'package:give_life/modules/trading_floor/views/trading_floor_view.dart';

class TradingFLoor extends Module {
  @override
  List<Bind> get binds => [
        Bind.singleton((i) => makeRemoteLoadLots()),
        Bind.singleton((i) => makeRemoteSaveshot()),
        Bind.singleton((i) => TradingFloorController(loadLots: i())),
        Bind.singleton((i) => DetailsController(loadLot: i(), saveShot: i())),
        Bind.singleton((i) => GalleryController(loadLot: i())),
        Bind.singleton((i) => ShotController(loadLot: i(), saveShot: i()))
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute('/',
            child: (_, __) => TradingFloorView(
                  controller: Modular.get<TradingFloorController>(),
                )),
        ChildRoute('/details/:id',
            child: (_, args) => DetailsTradingView(
                  id: int.parse(args.params['id']),
                  controller: Modular.get<DetailsController>(),
                )),
        ChildRoute('/gallery/:id/',
            child: (_, args) => GalleryView(
                  id: int.parse(args.params['id']),
                  controller: Modular.get<GalleryController>(),
                )),
        ChildRoute('/shots/:id/',
            child: (_, args) => ShotTradingView(
                  id: int.parse(args.params['id']),
                  controller: Modular.get<ShotController>(),
                ))
      ];
}
