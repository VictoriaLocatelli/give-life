import 'package:equatable/equatable.dart';

class ClientAccountEntity extends Equatable {

 
  ClientAccountEntity(
      {
      required this.name,
      required this.email,
      required this.token,
      required this.photo,
      this.personId});

 
  String name;
  String? photo;
  String email;
  final String token;
  final int? personId;

  void setPhoto(String? photo) {
    this.photo = photo;
  }

  void setName(String value) {
    this.name = value;
  }

  void setEmail(String value) {
    this.email = value;
  }

  String getPhoto() {
    return this.photo!;
  }

  @override
  List<Object?> get props => [name, email, token,personId, photo];

  ClientAccountEntity copyWith({
    String? name,
    String? photo,
    String? email,
    int? clientId,
    int? personId,
  }) {
    return ClientAccountEntity(
      name: name ?? this.name,
      email: email ?? this.email,
      photo: photo ?? this.photo,
      token: token,
      personId: personId ?? this.personId,
    );
  }
}