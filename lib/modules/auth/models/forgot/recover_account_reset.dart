import 'package:meta/meta.dart';

abstract class RecoverAccountReset {
  Future<String?> recover(InputRecoverAccountReset input);
}

@immutable
class InputRecoverAccountReset {
  const InputRecoverAccountReset(this.code, this.email, this.password);

  final String code;
  final String password;
  final String email;
}
