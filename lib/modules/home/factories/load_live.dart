import 'package:give_life/modules/home/model/live_sponsor.dart';

abstract class LoadLive {
  Future<List<LiveSponsor?>> loadLiveSponson();
}
