import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/shared/componets/widgets/fields/mask_all.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class AccountInputWidget extends StatelessWidget {
  const AccountInputWidget({super.key});

  @override
  Widget build(BuildContext context) {
    MaskAll mask = MaskAll(
        formatter: MaskTextInputFormatter(mask: "#########"),
        hint: "0000000000",
        textInputType: TextInputType.phone);
    final controller = Modular.get<ClientProfileController>();

    return Observer(builder: (_) {
      return CustomTextFormField(
        isRequired: false,
        label: 'Conta',
        initialValue: controller.bankAccount,
        onChanged: controller.bankAccountValidator,
        keyboardType: TextInputType.number,
        inputFormatters: [
          FilteringTextInputFormatter.digitsOnly,
          mask.formatter
        ],
      );
    });
  }
}
