import 'dart:convert';
import 'dart:developer';

import 'package:give_life/shared/app_config.dart';
import 'package:give_life/shared/data/contracts/local/load_client_current_account.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';
import 'package:http/http.dart';
import 'package:flutter_modular/flutter_modular.dart';

class HttpAdapter implements HttpClient {
  const HttpAdapter(this.client);

  final Client client;

  @override
  Future<dynamic> request({
    required String url,
    required String method,
    Map? body,
    Map? headers,
  }) async {
    final token = (await Modular.get<LoadClientCurrentAccount>().load())?.token;

    final defaultHeaders = headers?.cast<String, String>() ?? {}
      ..addAll({
        'accept': 'application/json',
        'content-type': 'application/json',
        'Authorization': 'Bearer $token',
        'app': AppConfig.getInstance().appKey
      });

    var response = Response('', 500);
    Future<Response>? futureResponse;
    final jsonBody = body != null ? jsonEncode(body) : null;
    final uri = Uri.parse(url);

    try {
      if (method == 'get') {
        futureResponse = client.get(uri, headers: defaultHeaders);
      } else if (method == 'post') {
        futureResponse =
            client.post(uri, headers: defaultHeaders, body: jsonBody);
      } else if (method == 'put') {
        futureResponse =
            client.put(uri, headers: defaultHeaders, body: jsonBody);
      }

      if (futureResponse != null) {
        response = await futureResponse.timeout(const Duration(seconds: 10));
      }
    } catch (error) {
      log('''
        ${response.request?.method} [${response.statusCode}] ${uri.path}
        error: $error
      ''');

      throw HttpError.serverError;
    }

    return _handleResponse(response);
  }

  dynamic _handleResponse(Response response) {
    final data = response.body.isNotEmpty ? jsonDecode(response.body) : null;

    if ([200, 201, 403].contains(response.statusCode)) return data;

    log('''
        ${response.request?.method} [${response.statusCode}] ${response.request?.url.path}
        info: ${data?['message']}
        data: $data
    ''');

    switch (response.statusCode) {
      case 204:
        return null;
      case 400:
        throw HttpError.badRequest;
      case 401:
        throw HttpError.unauthorized;
      case 404:
        throw HttpError.notFound;
      case 422:
        throw HttpError.unprocessableEntity;
      default:
        throw HttpError.serverError;
    }
  }
}
