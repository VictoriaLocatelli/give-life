import 'dart:ffi';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:give_life/modules/trading_floor/models/lot_entity.dart';
import 'package:give_life/modules/trading_floor/models/lot_images_entity.dart';
import 'package:give_life/modules/trading_floor/views/widget/youtube/youtube_players_widget.dart';

import 'package:give_life/shared/app_config.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:give_life/shared/componets/theme/app_colors.dart';

class CarrousselWidget extends StatefulWidget {
  const CarrousselWidget({super.key, required this.contents});
  final List<LotImagesEntity?> contents;

  @override
  State<CarrousselWidget> createState() => _CarrousselWidgetState();
}

class _CarrousselWidgetState extends State<CarrousselWidget> {
  LotImagesEntity? currentContent;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.contents.length > 0) {
      currentContent = widget.contents[0];
    }
  }

  ImageProvider<Object> _imageChoice(LotImagesEntity content) {
    if (content.urlVideo != null) {
      return AssetImage(AppImages.imageVideo.path);
    } else if (content.fileUrl != null) {
      return NetworkImage(content.fileUrl!);
    } else {
      return AssetImage(AppImages.noProfilePicture.path);
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, c) {
      return Container(
        margin: const EdgeInsets.all(3),
        padding: const EdgeInsets.all(3),
        width: c.maxWidth,
        height: 500,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
          border: Border.all(
            color: Color(0xffD9D9D9),
            style: BorderStyle.solid,
          ),
          color: AppColors.secondaryWhite,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            if (widget.contents.length > 0)
              Expanded(
                flex: 7,
                child: currentContent?.urlVideo == null
                    ? Container(
                        width: c.maxWidth,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.fitHeight,
                                image: _imageChoice(currentContent!))),
                      )
                    : YoutubePlayerWidget(url: currentContent!.urlVideo!),
              ),
            if (widget.contents.length > 0)
              Expanded(
                flex: 3,
                child: CarouselSlider(
                  items: widget.contents
                      .map((content) => GestureDetector(
                          onTap: () {
                            setState(() {
                              currentContent = content;
                            });
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: _imageChoice(content!))),
                          )))
                      .toList(),
                  options: CarouselOptions(autoPlay: true, height: 600),
                ),
              ),
          ],
        ),
      );
    });
  }
}
