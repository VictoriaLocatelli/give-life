import 'package:give_life/modules/auth/models/forgot/recover_account.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';
import 'package:meta/meta.dart';

class RemoteRecoverAccount implements RecoverAccount {
  const RemoteRecoverAccount({
    required this.httpClient,
    required this.url,
  });

  final String url;
  final HttpClient httpClient;

  @override
  Future<String?> recover(InputRecoverAccount input) async {
    final body = InputRemoteRecoverAccount.fromDomain(input).toMap();

    try {
      var resul =
          await httpClient.request(url: url, method: 'post', body: body);
      return resul["message"] as String;
    } on HttpError catch (error) {
      throw error == HttpError.unauthorized
          ? DomainError.invalidCredentials
          : DomainError.unexpected;
    }
  }
}

@immutable
class InputRemoteRecoverAccount {
  const InputRemoteRecoverAccount(this.email);

  final String email;

  factory InputRemoteRecoverAccount.fromDomain(InputRecoverAccount input) =>
      InputRemoteRecoverAccount(input.email);

  Map<String, dynamic> toMap() => {'email': email};
}
