// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'client_profile_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ClientProfileController on _ClientProfileControllerBase, Store {
  Computed<bool>? _$isFormValidComputed;

  @override
  bool get isFormValid =>
      (_$isFormValidComputed ??= Computed<bool>(() => super.isFormValid,
              name: '_ClientProfileControllerBase.isFormValid'))
          .value;

  late final _$_currentbBankAtom = Atom(
      name: '_ClientProfileControllerBase._currentbBank', context: context);

  @override
  BankEntity? get _currentbBank {
    _$_currentbBankAtom.reportRead();
    return super._currentbBank;
  }

  @override
  set _currentbBank(BankEntity? value) {
    _$_currentbBankAtom.reportWrite(value, super._currentbBank, () {
      super._currentbBank = value;
    });
  }

  late final _$_currentBankTypeAtom = Atom(
      name: '_ClientProfileControllerBase._currentBankType', context: context);

  @override
  DropdownTypeModel? get _currentBankType {
    _$_currentBankTypeAtom.reportRead();
    return super._currentBankType;
  }

  @override
  set _currentBankType(DropdownTypeModel? value) {
    _$_currentBankTypeAtom.reportWrite(value, super._currentBankType, () {
      super._currentBankType = value;
    });
  }

  late final _$nextAtom =
      Atom(name: '_ClientProfileControllerBase.next', context: context);

  @override
  int get next {
    _$nextAtom.reportRead();
    return super.next;
  }

  @override
  set next(int value) {
    _$nextAtom.reportWrite(value, super.next, () {
      super.next = value;
    });
  }

  late final _$_isLoadingAtom =
      Atom(name: '_ClientProfileControllerBase._isLoading', context: context);

  @override
  bool get _isLoading {
    _$_isLoadingAtom.reportRead();
    return super._isLoading;
  }

  @override
  set _isLoading(bool value) {
    _$_isLoadingAtom.reportWrite(value, super._isLoading, () {
      super._isLoading = value;
    });
  }

  late final _$_photoBase64Atom =
      Atom(name: '_ClientProfileControllerBase._photoBase64', context: context);

  @override
  ImageProvider<Object>? get _photoBase64 {
    _$_photoBase64Atom.reportRead();
    return super._photoBase64;
  }

  @override
  set _photoBase64(ImageProvider<Object>? value) {
    _$_photoBase64Atom.reportWrite(value, super._photoBase64, () {
      super._photoBase64 = value;
    });
  }

  late final _$_citiesAtom =
      Atom(name: '_ClientProfileControllerBase._cities', context: context);

  @override
  ObservableList<CityEntity> get _cities {
    _$_citiesAtom.reportRead();
    return super._cities;
  }

  @override
  set _cities(ObservableList<CityEntity> value) {
    _$_citiesAtom.reportWrite(value, super._cities, () {
      super._cities = value;
    });
  }

  late final _$_fetchProfileAsyncAction = AsyncAction(
      '_ClientProfileControllerBase._fetchProfile',
      context: context);

  @override
  Future<void> _fetchProfile() {
    return _$_fetchProfileAsyncAction.run(() => super._fetchProfile());
  }

  late final _$_fetchCitiesAsyncAction = AsyncAction(
      '_ClientProfileControllerBase._fetchCities',
      context: context);

  @override
  Future<void> _fetchCities() {
    return _$_fetchCitiesAsyncAction.run(() => super._fetchCities());
  }

  late final _$_ClientProfileControllerBaseActionController =
      ActionController(name: '_ClientProfileControllerBase', context: context);

  @override
  dynamic setNext(int value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.setNext');
    try {
      return super.setNext(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setIsLoading(bool value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.setIsLoading');
    try {
      return super.setIsLoading(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPhotoBase64(ImageProvider<Object>? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.setPhotoBase64');
    try {
      return super.setPhotoBase64(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? districtValidator(String? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.districtValidator');
    try {
      return super.districtValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? complementValidator(String? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.complementValidator');
    try {
      return super.complementValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? numberValidator(String? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.numberValidator');
    try {
      return super.numberValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? bankAccountValidator(String? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.bankAccountValidator');
    try {
      return super.bankAccountValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? bankAgencyValidator(String? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.bankAgencyValidator');
    try {
      return super.bankAgencyValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? bankTypeValidator(DropdownTypeModel? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.bankTypeValidator');
    try {
      return super.bankTypeValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? bankIdValidator(BankEntity? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.bankIdValidator');
    try {
      return super.bankIdValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? nameValidator(String? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.nameValidator');
    try {
      return super.nameValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? propertyNameValidator(String? value) {
    final _$actionInfo =
        _$_ClientProfileControllerBaseActionController.startAction(
            name: '_ClientProfileControllerBase.propertyNameValidator');
    try {
      return super.propertyNameValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? emailValidator(String? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.emailValidator');
    try {
      return super.emailValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? phone1Validator(String? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.phone1Validator');
    try {
      return super.phone1Validator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void rgValidator(String? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.rgValidator');
    try {
      return super.rgValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? nifValidator(String? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.nifValidator');
    try {
      return super.nifValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? addressValidator(String? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.addressValidator');
    try {
      return super.addressValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? propertyStateRegistrationValidator(String? value) {
    final _$actionInfo =
        _$_ClientProfileControllerBaseActionController.startAction(
            name:
                '_ClientProfileControllerBase.propertyStateRegistrationValidator');
    try {
      return super.propertyStateRegistrationValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? propertyAddressValidator(String? value) {
    final _$actionInfo =
        _$_ClientProfileControllerBaseActionController.startAction(
            name: '_ClientProfileControllerBase.propertyAddressValidator');
    try {
      return super.propertyAddressValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? cityValidator(CityEntity? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.cityValidator');
    try {
      return super.cityValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? propertyCityValidator(CityEntity? value) {
    final _$actionInfo =
        _$_ClientProfileControllerBaseActionController.startAction(
            name: '_ClientProfileControllerBase.propertyCityValidator');
    try {
      return super.propertyCityValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? zipValidator(String? value) {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.zipValidator');
    try {
      return super.zipValidator(value);
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clean() {
    final _$actionInfo = _$_ClientProfileControllerBaseActionController
        .startAction(name: '_ClientProfileControllerBase.clean');
    try {
      return super.clean();
    } finally {
      _$_ClientProfileControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
next: ${next},
isFormValid: ${isFormValid}
    ''';
  }
}
