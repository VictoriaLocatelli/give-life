import 'package:give_life/modules/home/model/contents_entity.dart';
import 'package:give_life/modules/home/model/lot_entity.dart';

class LiveAuctionsEntity {
  int id;
  int? type;
  String? name;
  String? description;
  String? image;
  int isOnline;
  DateTime date;
  String time;
  String? local;
  String? address;
  String? auctionhouse;
  String city;
  String? phone;
  String? freePhone;
  String? catalog;
  String? regulation;
  List<ContentsEntity?>? contents;
  int isEnabled;
  String? landline;
  String? deskone;
  String? desktwo;
  String? deskthree;
  String? deskfour;
  String? deskfive;
  String? desksix;
  String? deskseven;
  String? deskeight;

  List<LotEntity>? lots;

  LiveAuctionsEntity({
    required this.id,
    required this.isOnline,
    required this.date,
    required this.time,
    required this.city,
    this.description,
    required this.isEnabled,
    this.auctionhouse,
    this.phone,
    this.freePhone,
    this.catalog,
    this.regulation,
    this.type,
    this.name,
    this.image,
    this.local,
    this.address,
    this.lots,
    this.contents,
    this.landline,
    this.deskone,
    this.desktwo,
    this.deskthree,
    this.deskfour,
    this.deskfive,
    this.desksix,
    this.deskseven,
    this.deskeight,
  });

  factory LiveAuctionsEntity.fromJson(Map<String, dynamic> json) {
    List<ContentsEntity> contents = <ContentsEntity>[];
    List<LotEntity> lots = [];
    if (json['lots'] != null) {
      for (var lot in json['lots']) {
        lots.add(LotEntity.fromJson(lot));
      }
    }
    for (var content in json['contents']) {
      contents.add(ContentsEntity.fromJson(content));
    }
    return LiveAuctionsEntity(
      id: json['id'],
      type: json['type'],
      name: json['name'],
      description: json['description'],
      image: json['image'],
      isOnline: json['is_online'],
      date: DateTime.parse(json['date']).toLocal(),
      time: json['time'],
      address: json['address'],
      auctionhouse: json['auctionhouse'],
      city: json['city'],
      phone: json['phone'],
      local: json['local'],
      freePhone: json['free_phone'],
      regulation: json['regulation'],
      catalog: json['catalog'],
      isEnabled: json['is_enabled'],
      lots: lots,
      contents: contents,
      landline: json['landline'],
      deskone: json['deskone'],
      desktwo: json['desktwo'],
      deskthree: json['deskthree'],
      deskfour: json['deskfour'],
      deskfive: json['deskfive'],
      desksix: json['desksix'],
      deskseven: json['deskseven'],
      deskeight: json['deskeight'],
    );
  }
}
