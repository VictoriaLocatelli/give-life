import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/home/factories/load_live.dart';
import 'package:give_life/modules/home/usescases/remote_load_banners.dart';
import 'package:give_life/shared/data/factories/api_url_factory.dart';
import 'package:give_life/shared/data/http/http_client.dart';

LoadLive makeRemoteLoadBanners() => RemoteLoadBanners(
      url: makeApiUrl('/banners'),
      httpClient: Modular.get<HttpClient>(),
    );
