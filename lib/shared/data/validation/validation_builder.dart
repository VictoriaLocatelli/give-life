import 'package:brasil_fields/brasil_fields.dart';
import 'package:give_life/shared/componets/extensions/strings.dart';

class ValidationBuilder {
  ValidationBuilder(this.value);

  final String? value;

  final List<String?> _validations = [];

  static const _invalidField = 'Campo inválido';
  static const _requiredFiled = 'Campo obrigatório';

  ValidationBuilder required() {
    if (value?.removeWhiteSpace().isEmpty == true)
      _validations.add(_requiredFiled);

    return this;
  }

  ValidationBuilder email() {
    const emailRegex =
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
    if (value != null && RegExp(emailRegex).hasMatch(value!) == false) {
      _validations.add(_invalidField);
    }

    return this;
  }

  ValidationBuilder cpf() {
    if (CPFValidator.isValid(value) == false) _validations.add('CPF inválido');

    return this;
  }

  ValidationBuilder cnpj() {
    if (CNPJValidator.isValid(value) == false)
      _validations.add('CNPJ inválido');

    return this;
  }

  ValidationBuilder min(int size) {
    final fieldError = (value ?? '').length < size
        ? 'Requer no mínimo $size caracteres'
        : null;
    if (fieldError != null) _validations.add(fieldError);

    return this;
  }

  ValidationBuilder max(int size) {
    final fieldError = (value ?? '').length > size
        ? 'Este campo tem limite máximo de $size caracteres'
        : null;
    if (fieldError != null) _validations.add(fieldError);

    return this;
  }

  ValidationBuilder compare(String? compareValue) {
    final fieldError = value != compareValue
        ? 'Os campos não conferem. Tente outra vez'
        : null;
    if (fieldError != null) _validations.add(fieldError);

    return this;
  }

  String? build() =>
      _validations.firstWhere((e) => e?.isNotEmpty == true, orElse: () => null);
}
