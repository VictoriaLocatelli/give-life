import 'package:equatable/equatable.dart';

class CityEntity extends Equatable {
  final int id;
  String name;
  final String ufSigla;

  CityEntity({
    required this.id,
    required this.name,
    required this.ufSigla,
  });
  void setName(String value) => this.name = value;
  @override
  List<Object> get props => [id, name, ufSigla];

  @override
  String toString() => '$name - $ufSigla';
}
