import 'package:give_life/modules/help/models/load_email.dart';
import 'package:mobx/mobx.dart';

part 'help_controller.g.dart';

class HelpController = _HelpControllerBase with _$HelpController;

abstract class _HelpControllerBase with Store {
  _HelpControllerBase({
    required LoadEmail loadCurrentAccount,
  }) : _loadCurrentAccount = loadCurrentAccount;

  final LoadEmail _loadCurrentAccount;

  String email = '';
  String phone = '';

  @observable
  bool _isLoading = false;

  @computed
  bool get isLoading => _isLoading;

  set isLoading(bool value) => _isLoading = value;

  Future<void> loadData() async {
    try {
      isLoading = true;
      final account = await _loadCurrentAccount.load();
      email = account['EMAIL'];
      phone = account['CONTATO'];
      isLoading = false;
    } catch (e) {
      print(e);
    }
  }

  String phoneFormat(String value) {
    final pattern = RegExp(r'(\d{2})(\d{4})(\d+)');

    return value.replaceAllMapped(
        pattern, (Match m) => '(${m[1]}) ${m[2]}-${m[3]}');
  }
}
