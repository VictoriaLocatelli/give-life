import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class ImageCropped {
  ImageCropped({required this.context, required this.pickedFile});
  final BuildContext context;
  final XFile pickedFile;

  Future<CroppedFile?> cropImage() async {
    return await ImageCropper().cropImage(
      sourcePath: pickedFile.path,
      compressFormat: ImageCompressFormat.png,
      compressQuality: 50,
      maxWidth: 220,
      maxHeight: 220,
      uiSettings: [
        AndroidUiSettings(
          showCropGrid: true,
          hideBottomControls: false,
            activeControlsWidgetColor: Colors.blue,
            toolbarTitle: 'Cortar imagem',
            toolbarColor: Theme.of(context).primaryColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        IOSUiSettings(
          title: 'Cortar imagem',
        ),
        WebUiSettings(
          context: context,
          presentStyle: CropperPresentStyle.dialog,
          boundary: const CroppieBoundary(
            width: 220,
            height: 220,
          ),
          viewPort: const CroppieViewPort(width: 480, height: 480, type: 'circle'),
          enableExif: true,
          enableZoom: true,
          showZoomer: true,
        ),
      ],
    );
  }
}
