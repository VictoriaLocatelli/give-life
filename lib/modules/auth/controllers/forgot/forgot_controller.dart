import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/auth/models/forgot/recover_account.dart';
import 'package:give_life/modules/auth/models/forgot/recover_account_code.dart';
import 'package:give_life/modules/auth/models/forgot/recover_account_reset.dart';
import 'package:give_life/shared/componets/utils/show_snack_bar.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/validation/validation_builder.dart';
import 'package:mobx/mobx.dart';

part 'forgot_controller.g.dart';

class ForgotController = _ForgotControllerBase with _$ForgotController;

abstract class _ForgotControllerBase with Store {
  _ForgotControllerBase({required this.recoverAccount});

  final RecoverAccount recoverAccount;

  @observable
  String? _email;
  @observable
  String? _emailError;
  @observable
  String? _emailTwo;
  @observable
  String? _code;
  @observable
  String? _codeError;
  @observable
  bool _isLoading = false;

  @observable
  BuildContext? _contexts;
  BuildContext? get contexts => _contexts;
  void set contexts(BuildContext? value) => _contexts = value;

  String? get email => _email;
  String? get emailTwo => _emailTwo;
  String? get emailError => _emailError;
  String? get code => _code;
  String? get codeError => _codeError;
  void set code(String? value) {
    _code = value;
    if (_code != null && _code!.length >= 6) {
      _codeError = null;
    } else
      _codeError = '';
  }

  String? _passwordConf;
  String? get passwordConf => _passwordConf;
  void set passwordConf(String? value) => _passwordConf = value;

  String? _password;
  String? get password => _password;
  void set password(String? value) => _password = value;

  void set emailTwo(String? value) => _emailTwo = value;
  bool get isLoading => _isLoading;

  @action
  String? validateEmail(String? value) {
    _email = value;
    emailTwo = value;
    _emailError = ValidationBuilder(value).required().email().build();

    return _emailError;
  }

  @action
  void _toggleIsLoading() => _isLoading = !_isLoading;

  @action
  void _clean() => _email = null;

  Future<void> reset() async {
    _toggleIsLoading();
    var recoverCode = Modular.get<RecoverAccountReset>();
    try {
      String? message = await recoverCode
          .recover(InputRecoverAccountReset(_code!, emailTwo!, passwordConf!));
      if (message != null) {
        ShowSnackBar.success(message).show();
        Future.delayed(Duration(seconds: 3), () {
          Modular.to.navigate('/auth');
        });
      }
    } on DomainError catch (error) {
      final message = error == DomainError.unexpected
          ? 'Não foi possível finalizar o processo. Tente novamente mais tarde.'
          : error.description;
      Future.delayed(Duration(seconds: 2), () {
        Modular.to.navigate('/auth/forgot');
      });
    }
    _toggleIsLoading();
  }

  Future<void> checkCode() async {
    _toggleIsLoading();
    var recoverCode = Modular.get<RecoverAccountCode>();

    try {
      String? message =
          await recoverCode.recover(InputRecoverAccountCode(_code!));
      if (message != null) {
        ShowSnackBar.success("Codigo validado com sucesso!").show();
        Future.delayed(Duration(seconds: 3), () {
          Modular.to.navigate('/auth/forgotReset');
        });
      }
    } on DomainError catch (error) {
      final message = error == DomainError.unexpected
          ? 'Código informado é inválido'
          : error.description;
      ShowSnackBar.warning(message).show();
    }
    _toggleIsLoading();
  }

  Future<void> recover() async {
    _toggleIsLoading();

    try {
      String? message =
          await recoverAccount.recover(InputRecoverAccount(_email!));
      _clean();
      if (message != null) {
        ShowSnackBar.success(message).show();
        Future.delayed(Duration(seconds: 3), () {
          Modular.to.navigate('/auth/forgotCode');
        });
      }
    } on DomainError catch (error) {
      final message = error == DomainError.unexpected
          ? 'Email não cadastrado'
          : error.description;
      ShowSnackBar.warning(message).show();
    }

    _toggleIsLoading();
  }

  @computed
  bool get isFormValid => _email != null && _emailError == null;
  @computed
  bool get isFormValidCode => _code != null && _codeError == null;
}
