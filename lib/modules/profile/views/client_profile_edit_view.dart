import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/modules/profile/widgets/number_input_widget.dart';
import 'package:give_life/modules/profile/widgets/widgets.dart';

import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:give_life/shared/componets/constants/dimensions.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';
import 'package:give_life/shared/componets/widgets/navigation_drawer.dart';
import 'package:im_stepper/stepper.dart';

class ClientProfileEditView extends StatefulWidget {
  const ClientProfileEditView({super.key, required this.controller});

  final ClientProfileController controller;

  @override
  State<ClientProfileEditView> createState() => _ClientProfileEditViewState();
}

class _ClientProfileEditViewState extends State<ClientProfileEditView> {
  @override
  void initState() {
    super.initState();
    widget.controller.loadData();
  }

  @override
  Widget build(BuildContext context) {
    final controller = widget.controller;
    ImageProvider<Object>? resul;
    return GestureDetector(
      onTap: () => FocusScope.of(context).focusedChild?.unfocus(),
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(180),
          child: AppBar(
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(150),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 50),
                    child: Observer(builder: (context) {
                      if (widget.controller.isLoading) return Container();
                      ImageProvider<Object>? resul;
                      if (widget.controller.photoBase64 != null) {
                        resul = widget.controller.photoBase64;
                      } else if (widget.controller.photo != null) {
                        resul = NetworkImage(widget.controller.photo!);
                      } else {
                        resul = AssetImage(AppImages.noProfilePicture.path);
                      }
                      return LayoutBuilder(builder: (context, c) {
                        return Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Container(
                              margin: EdgeInsets.all(0),
                              width: c.maxWidth * .35,
                              child: Container(
                                child: Stack(
                                  children: [
                                    Positioned(
                                      right: 0,
                                      bottom: 0,
                                      child: IconButton(
                                          onPressed: () async => await widget
                                              .controller
                                              .cropImage(context),
                                          icon: Icon(
                                            Icons.camera_alt,
                                            size: 35,
                                            color: Theme.of(context)
                                                .primaryColorLight,
                                          )),
                                    )
                                  ],
                                ),
                                margin: EdgeInsets.all(0),
                                height: 100,
                                width: 100,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: resul ??
                                        AssetImage(
                                            AppImages.noProfilePicture.path),
                                  ), //Imagem a ser trocada
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(8.0),
                              margin: EdgeInsets.all(0),
                              width: c.maxWidth * .65,
                              child: AutoSizeText(
                                "${widget.controller.name}",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 22, color: Colors.white),
                                maxLines: 3,
                              ),
                            )
                          ],
                        );
                      });
                    }),
                  ),
                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(50)),
                    ),
                  ),
                  Divider(
                    height: 1,
                  )
                ],
              ),
            ),
          ),
        ),
        drawer: const NavigationDrawer(),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          margin: genericPaddingPage,
          padding: const EdgeInsets.all(1),
          child: Observer(builder: (_) {
            if (controller.isLoading)
              return const Center(child: CircularProgressIndicator());

            return Form(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    IconStepper(
                      stepRadius: 21,
                      enableNextPreviousButtons: false,
                      enableStepTapping: false,
                      lineLength: 70,
                      activeStepColor: Theme.of(context).primaryColor,
                      stepColor: Color(0xffbababa),
                      lineColor: Colors.black26,
                      lineDotRadius: 2,
                      alignment: Alignment.bottomLeft,
                      icons: [
                        Icon(
                          Icons.person,
                          color: Colors.white,
                        ),
                        Icon(
                          Icons.home,
                          color: Colors.white,
                        ),
                        Icon(
                          Icons.credit_card,
                          color: Colors.white,
                        ),
                        /* Icon(Icons.article, color: Colors.white), */
                      ],
                      activeStep: widget.controller.next,
                      onStepReached: (index) => print(index),
                    ),
                    [
                      Column(children: [
                        const NameInputWidget(),
                        const EmailInputWidget(),
                        const Phone1InputWidget(),
                        const NifInputWidget(),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Expanded(
                              child: const InputDateWidget(),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(child: const ZipCodeInputWidget())
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Expanded(
                              flex: 7,
                              child: const AddressInputWidget(),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              flex: 3,
                              child: const NumberInputWidget(),
                            ),
                          ],
                        ),
                        const DistComplWidget(),
                        const CityInputWidget(),
                      ]),
                      //DADOS DA PROPRIEDADE
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(
                            "Dados da propriedade",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          const PropertyNameWidget(),
                          SizedBox(
                            height: 5,
                          ),
                          const PropertyAddressInputWidget(),
                          SizedBox(
                            height: 5,
                          ),
                          const PropertyCityInputWidget(),
                          SizedBox(
                            height: 5,
                          ),
                          const PropertyStateRegistrationWidget(),
                        ],
                      ),
                      //DADOS BANCÁRIO
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(
                            "Dados bancários",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ),
                          BanksInputWidget(),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Expanded(flex: 3, child: AgencyInputWidget()),
                              SizedBox(width: 5),
                              Expanded(flex: 7, child: AccountInputWidget())
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          AccountTypeWidget()
                        ],
                      ),
                      Container(
                        color: Colors.blue,
                        width: 200,
                        height: 200,
                      ),
                    ].elementAt(widget.controller.next),
                    widget.controller.next > 1
                        ? const ButtonsWidget()
                        : const ButtonsNextWidget(),
                  ],
                ),
              ),
            );
          }),
        ),
      ),
    );
  }
}
