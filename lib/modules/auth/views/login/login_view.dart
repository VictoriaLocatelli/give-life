import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/auth/controllers/login/login_controller.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';

import 'package:give_life/shared/componets/constants/spacing.dart';
import 'package:give_life/shared/componets/widgets/app_version_with_logo.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';
import 'package:give_life/shared/core/entity/client_account_entitie.dart';
import 'package:give_life/shared/data/contracts/local/load_client_current_account.dart';

class LoginView extends StatefulWidget {
  LoginView({
    Key? key,
    required this.controller,
  }) : super(key: key);
  final LoginController controller;

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  bool toggle = false;
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;
    return RefreshIndicator(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).focusedChild?.unfocus(),
          child: Theme(
            data: Theme.of(context).copyWith(
              textTheme: Theme.of(context).textTheme.copyWith(
                    headline4: textTheme.headline4
                        ?.copyWith(color: colorScheme.onPrimary),
                    bodyText2: textTheme.bodyText2
                        ?.copyWith(color: colorScheme.onPrimary),
                    caption: textTheme.caption
                        ?.copyWith(color: colorScheme.onPrimary),
                  ),
            ),
            child: Scaffold(
              backgroundColor: Colors.white,
              body: Observer(builder: (context) {
                final textTheme = Theme.of(context).textTheme;

                if (widget.controller.isLoading) {
                  return Center(child: CircularProgressIndicator());
                }

                return LayoutBuilder(builder: (context, c) {
                  return Container(
                    height: c.maxHeight,
                    width: c.maxWidth,
                    padding: const EdgeInsets.only(bottom: Spacing.medium),
                    child: ListView(
                      children: [
                        Container(
                          height: c.maxHeight * .3 - 0,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  fit: BoxFit.fitHeight,
                                  image: AssetImage(AppImages.logo.path))),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: Spacing.large),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              const SizedBox(height: Spacing.small),
                              Text(
                                'Entrar',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                              Text('Insira os dados para acessar.',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 15),
                                  textAlign: TextAlign.left),
                              const SizedBox(height: Spacing.xLarge),
                              const _EmailInput(),
                              const _PasswordInput(),
                              const _ForgotPasswordButton(),
                              const SizedBox(height: Spacing.large),
                              const _SuccessButton(),
                              const SizedBox(height: Spacing.small),
                              OutlinedButton(
                                  style: OutlinedButton.styleFrom(
                                    primary: colorScheme.secondary,
                                    side: BorderSide(
                                        color: colorScheme.secondary),
                                  ),
                                  onPressed: widget.controller.goToSignUp,
                                  child: Text('Criar Conta')),
                              const SizedBox(height: Spacing.large),
                              const SizedBox(height: Spacing.xxxLarge),
                              const SizedBox(height: Spacing.xxxLarge),
                              const AppVersionWithLogo(
                                color: Colors.black,
                                textColor: Colors.black,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                });
              }),
            ),
          ),
        ),
        onRefresh: () async {
          setState(() {
            toggle = false;
          });
        });
  }
}

class _SuccessButton extends StatelessWidget {
  const _SuccessButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<LoginController>();

    return Observer(builder: (_) {
      return RawKeyboardListener(
        onKey: (event) {
          if (event.isKeyPressed(LogicalKeyboardKey.enter)) {
            controller.auth();
          }
        },
        focusNode: FocusNode(onKey: (node, event) {
          if (event.isKeyPressed(LogicalKeyboardKey.enter)) {
            return KeyEventResult.handled;
          }
          return KeyEventResult.ignored;
        }),
        child: ElevatedButton(
          onPressed: controller.isFormValid ? controller.auth : null,
          child: const Text('Entrar'),
        ),
      );
    });
  }
}

class _ForgotPasswordButton extends StatelessWidget {
  const _ForgotPasswordButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: TextButton(
        onPressed: Modular.get<LoginController>().goToForget,
        child: Text(
          'Esqueceu sua senha?',
          style: TextStyle(
              color: Colors.blue.shade600, fontWeight: FontWeight.normal),
        ),
      ),
    );
  }
}

class _PasswordInput extends StatelessWidget {
  const _PasswordInput({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<LoginController>();

    return Observer(builder: (_) {
      return CustomTextFormField(
        label: 'Senha',
        initialValue: controller.password,
        keyboardType: TextInputType.visiblePassword,
        obscureText: controller.isObscurePassword,
        onChanged: controller.validatePassword,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey.shade200,
          errorText: controller.passwordError,
          hintText: 'Informe uma senha',
          prefixIcon: const Icon(Icons.lock_outline),
          suffixIcon: IconButton(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            onPressed: controller.toggleObscurePassword,
            icon: Icon(controller.isObscurePassword
                ? Icons.visibility_off_outlined
                : Icons.visibility_outlined),
          ),
        ),
      );
    });
  }
}

class _EmailInput extends StatelessWidget {
  const _EmailInput({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<LoginController>();

    return Observer(builder: (_) {
      return CustomTextFormField(
        label: 'E-mail',
        initialValue: controller.email,
        keyboardType: TextInputType.emailAddress,
        onChanged: controller.validateEmail,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.grey.shade200,
          errorText: controller.emailError,
          hintText: 'Informe um e-mail',
          prefixIcon: const Icon(Icons.email_outlined),
        ),
      );
    });
  }
}
