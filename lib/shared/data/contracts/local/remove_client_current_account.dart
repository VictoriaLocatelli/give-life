abstract class RemoveClientCurrentAccount {
  Future<void> remove();
}