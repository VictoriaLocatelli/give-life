import 'package:give_life/modules/home/model/lot_image_entity.dart';

class LotEntity {
  int? id;
  int? type;
  String? name;
  String? description;
  String? age;
  String? bloodgrade;
  String? tatrp;
  String? ce;
  String? coat;
  String? salesman;
  String? location;
  String? gender;
  int? quantity;
  int? averageweight;
  int? isenabled;
  int? auctionid;
  int? animalfamilyid;
  int? animalcategoryid;
  int? animalbreedid;
  double? initvalue;
  double? incvalueone;
  double? incvaluetwo;
  double? incvaluethree;
  double? incvaluefour;
  String? targetbid;
  String? valuea;
  String? valueb;
  String? disputetime;
  String? hourinit;
  String? hourend;
  String? observation;
  int? status;
  String? paymentform;
  String? createdat;
  String? updatedat;
  String? tradingfloorid;
  String? shotid;
  String? condition;
  int? typelot;
  String? video;
  String? genealogy;
  String? dateexpired;
  String? family;
  String? category;
  String? breed;
  int? bloodGrade;
  DateTime? date;
  int? initValue;
  String? landline;

  List<LotImageEntity?>? images;

  LotEntity({
    this.id,
    this.type,
    this.name,
    this.description,
    this.age,
    this.bloodgrade,
    this.tatrp,
    this.ce,
    this.coat,
    this.salesman,
    this.location,
    this.gender,
    this.quantity,
    this.averageweight,
    this.isenabled,
    this.auctionid,
    this.animalfamilyid,
    this.animalcategoryid,
    this.animalbreedid,
    this.initvalue,
    this.incvalueone,
    this.incvaluetwo,
    this.incvaluethree,
    this.incvaluefour,
    this.targetbid,
    this.valuea,
    this.valueb,
    this.disputetime,
    this.hourinit,
    this.hourend,
    this.observation,
    this.status,
    this.paymentform,
    this.createdat,
    this.updatedat,
    this.tradingfloorid,
    this.shotid,
    this.condition,
    this.typelot,
    this.video,
    this.genealogy,
    this.dateexpired,
    this.family,
    this.category,
    this.breed,
    this.bloodGrade,
    this.images,
    this.date,
    this.landline,
    required this.initValue,
  });

  LotEntity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    name = json['name'];
    description = json['description'];
    age = json['age'];
    bloodgrade = json['blood_grade'];
    tatrp = json['tat_rp'];
    ce = json['ce'];
    coat = json['coat'];
    salesman = json['salesman'];
    location = json['location'];
    gender = json['gender'];
    quantity = json['quantity'];
    averageweight = json['average_weight'];
    isenabled = json['is_enabled'];
    auctionid = json['auction_id'];
    animalfamilyid = json['animal_family_id'];
    animalcategoryid = json['animal_category_id'];
    animalbreedid = json['animal_breed_id'];
    initvalue = json['init_value'] != null
        ? double.parse(json['init_value'].toString())
        : json['init_value'];
    incvalueone = json['inc_value_one'] != null
        ? double.parse(json['inc_value_one'].toString())
        : json['inc_value_one'];
    incvaluetwo = json['inc_value_two'] != null
        ? double.parse(json['inc_value_two'].toString())
        : json['inc_value_two'];
    incvaluethree = json['inc_value_three'] != null
        ? double.parse(json['inc_value_three'].toString())
        : json['inc_value_three'];
    incvaluefour = json['inc_value_four'] != null
        ? double.parse(json['inc_value_four'].toString())
        : json['inc_value_four'];
    targetbid = json['target_bid'];
    valuea = json['value_a'];
    valueb = json['value_b'];
    disputetime = json['dispute_time'];
    hourinit = json['hour_init'];
    hourend = json['hour_end'];
    observation = json['observation'];
    status = json['status'];
    paymentform = json['payment_form'];
    createdat = json['created_at'];
    updatedat = json['updated_at'];
    tradingfloorid = json['trading_floor_id'];
    shotid = json['shot_id'];
    condition = json['condition'];
    typelot = json['type_lot'];
    video = json['video'];
    genealogy = json['genealogy'];
    dateexpired = json['date_expired'];
    family = json['family'];
    category = json['category'];
    breed = json['breed'];
    date = json['date'];
    landline = json['landline'];

    bloodGrade = json['bloodGrade'];
    if (json['images'] != null) {
      images = <LotImageEntity>[];
      json['images'].forEach((v) {
        images!.add(LotImageEntity.fromJson(v));
      });
      initValue = json['init_value'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['type'] = type;
    data['name'] = name;
    data['description'] = description;
    data['age'] = age;
    data['blood_grade'] = bloodgrade;
    data['tat_rp'] = tatrp;
    data['ce'] = ce;
    data['coat'] = coat;
    data['salesman'] = salesman;
    data['location'] = location;
    data['gender'] = gender;
    data['quantity'] = quantity;
    data['average_weight'] = averageweight;
    data['is_enabled'] = isenabled;
    data['auction_id'] = auctionid;
    data['animal_family_id'] = animalfamilyid;
    data['animal_category_id'] = animalcategoryid;
    data['animal_breed_id'] = animalbreedid;
    data['init_value'] = initvalue;
    data['inc_value_one'] = incvalueone;
    data['inc_value_two'] = incvaluetwo;
    data['inc_value_three'] = incvaluethree;
    data['inc_value_four'] = incvaluefour;
    data['target_bid'] = targetbid;
    data['value_a'] = valuea;
    data['value_b'] = valueb;
    data['dispute_time'] = disputetime;
    data['hour_init'] = hourinit;
    data['hour_end'] = hourend;
    data['observation'] = observation;
    data['status'] = status;
    data['payment_form'] = paymentform;
    data['created_at'] = createdat;
    data['updated_at'] = updatedat;
    data['trading_floor_id'] = tradingfloorid;
    data['shot_id'] = shotid;
    data['condition'] = condition;
    data['type_lot'] = typelot;
    data['video'] = video;
    data['genealogy'] = genealogy;
    data['date_expired'] = dateexpired;
    data['family'] = family;
    data['category'] = category;
    data['breed'] = breed;

    data['images'] =
        images != null ? images!.map((v) => v?.toJson()).toList() : null;
    return data;
  }
}
