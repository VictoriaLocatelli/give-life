import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';

class InputDateWidget extends StatelessWidget {
  const InputDateWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final ClientProfileController controller =
        Modular.get<ClientProfileController>();
    TextEditingController textDate = TextEditingController();
    TextEditingController currentDate = TextEditingController();
    textDate.text = controller.date;
    return Transform.translate(
      offset: Offset(0, -3),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("Data de nascimento"),
          TextFormField(
            readOnly: true,
            onTap: () async {
              DateTime? value;
              value = await showDatePicker(
                context: context,
                initialDatePickerMode: DatePickerMode.day,
                initialDate: DateTime.now(),
                firstDate: DateTime(1900),
                lastDate: DateTime.now(),
              );

              if (value != null) {
                textDate.text = controller.formatDate(value);

                controller.dateValidator(value.toIso8601String());
              }
            },
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              errorText: null,
              suffixIcon: Icon(Icons.date_range),
            ),
            controller: textDate,
          ),
        ],
      ),
    );
  }
}
