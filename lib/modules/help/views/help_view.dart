import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:give_life/modules/help/controllers/help_controller.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:give_life/shared/componets/constants/spacing.dart';
import 'package:give_life/shared/componets/utils/launch_link.dart';
import 'package:give_life/shared/componets/utils/scaffold_messenger.dart';

class HelpView extends StatefulWidget {
  const HelpView({Key? key, required this.controller}) : super(key: key);
  final HelpController controller;

  @override
  State<HelpView> createState() => _HelpViewState();
}

class _HelpViewState extends State<HelpView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.controller.loadData();
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;
    final textButtonStyle = TextButton.styleFrom(
      backgroundColor: Colors.transparent,
      alignment: Alignment.centerLeft,
    );
    return GestureDetector(
      onTap: () => FocusScope.of(context).focusedChild?.unfocus(),
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: const Text("Ajuda"),
          ),
          body: Observer(
            builder: (context) {
              if (widget.controller.isLoading) {
                return const Center(child: CircularProgressIndicator());
              }
              return ListView(
                shrinkWrap: true,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  CircleAvatar(
                    radius: 90,
                    child: Hero(
                      tag: 'Support',
                      child: Image.asset(AppImages.support.path),
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: Spacing.large),
                    child: Divider(),
                  ),
                  TextButton.icon(
                    label: Text(
                      widget.controller.email,
                      style: textTheme.bodyText2
                          ?.copyWith(height: 1, fontSize: 22),
                    ),
                    icon: const Icon(Icons.email_outlined,
                        color: Colors.grey, size: 28),
                    style: textButtonStyle,
                    onPressed: () async => launchEmail(
                      widget.controller.email,
                      onFailLaunchingUrl: (value) => showSnackBar(
                        context,
                        content: Text(value),
                        type: SnackBarType.error,
                      ),
                    ),
                  ),
                  const SizedBox(height: Spacing.xxSmall),
                  TextButton.icon(
                    label: Text(
                      widget.controller.phoneFormat(widget.controller.phone),
                      style: textTheme.bodyText2
                          ?.copyWith(height: 1, fontSize: 22),
                    ),
                    icon: Icon(Icons.whatsapp),
                    style: textButtonStyle,
                    onPressed: () async => launchWhatsApp(
                      widget.controller.phone,
                      onFailLaunchingUrl: (value) => showSnackBar(
                        context,
                        content: Text(value),
                        type: SnackBarType.error,
                      ),
                    ),
                  ),
                ],
              );
            },
          )),
    );
  }
}
