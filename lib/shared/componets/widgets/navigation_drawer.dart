import 'package:flutter/material.dart';

import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/shared/app_config.dart';
import 'package:give_life/shared/componets/theme/app_colors.dart';
import 'package:give_life/shared/core/entity/client_account_entitie.dart';
import 'package:give_life/shared/data/contracts/local/load_client_current_account.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants/constants.dart';

import 'app_version_with_logo.dart';

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;

    return Drawer(
      backgroundColor: Color.fromARGB(248, 138, 138, 138),
      child: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: IntrinsicHeight(
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height,
            ),
            child: FutureBuilder<ClientAccountEntity?>(
              future: Modular.get<LoadClientCurrentAccount>().load(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(child: CircularProgressIndicator());
                }

                final account = snapshot.data;

                final accountName = account != null
                    ? Text(
                        account.name,
                        style: textTheme.bodyText1?.copyWith(
                          color: colorScheme.onPrimary,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    : TextButton(
                        onPressed: () => Modular.to.popAndPushNamed('/auth/'),
                        child: Text(
                          'Acessar',
                          style: textTheme.button
                              ?.copyWith(color: colorScheme.onPrimary),
                        ),
                      );

                final accountEmail = account != null
                    ? Text(
                        account.email,
                        style: textTheme.bodyText2?.copyWith(
                          color: colorScheme.onPrimary,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    : null;

                return Center(
                  child: Column(
                    children: [
                      UserAccountsDrawerHeader(
                        decoration: BoxDecoration(
                            color: Color.fromARGB(248, 99, 99, 99)),
                        currentAccountPicture: GestureDetector(
                          onTap: () => Modular.to.navigate('/profile/'),
                          child: CircleAvatar(
                            foregroundImage: imagens(account),
                            child: Stack(
                              clipBehavior: Clip.none,
                              children: [
                                Positioned(
                                    left: 35,
                                    bottom: -20,
                                    child: IconButton(
                                        onPressed: () {},
                                        icon: Icon(
                                          Icons.edit,
                                          color: Colors.white,
                                        )))
                              ],
                            ),
                          ),
                        ),
                        accountName: accountName,
                        accountEmail: accountEmail,
                      ),
                      ListTile(
                        title: const Text('Agenda de Leilões',
                            style: TextStyle(color: Colors.white)),
                        leading: const Icon(
                          Icons.home_outlined,
                          color: Colors.white,
                          size: 30,
                        ),
                        onTap: () {
                          Modular.to.pop();
                          Modular.to.navigate('/home/');
                        },
                      ),
                      if (account != null) const _OptionForAuthUser(),
                      ExpansionTile(
                        leading: const Icon(Icons.settings_outlined,
                            color: Colors.white),
                        title: const Text(
                          'Configurações',
                          style: TextStyle(color: Colors.white),
                        ),
                        childrenPadding:
                            const EdgeInsets.only(left: Spacing.xLarge),
                        collapsedIconColor: Colors.white,
                        iconColor: Colors.white,
                        children: [
                          if (account != null)
                            ListTile(
                              title: const Text('Perfil',
                                  style: TextStyle(color: Colors.white)),
                              onTap: () => Modular.to.navigate('/profile/'),
                            ),
                          ListTile(
                            title: const Text('Politica de Privacidade',
                                style: TextStyle(color: Colors.white)),
                            onTap: () async {
                              await launch(AppConfig.getInstance().apiTermsUrl);
                            },
                          ),
                          ListTile(
                            title: const Text('Ajuda',
                                style: TextStyle(color: Colors.white)),
                            onTap: () => Modular.to.pushNamed('/help/'),
                          ),
                        ],
                      ),
                      const Spacer(),
                      const Align(
                        alignment: Alignment.bottomCenter,
                        child: AppVersionWithLogo(
                          color: Colors.white,
                          textColor: Colors.white,
                        ),
                      ),
                      const SizedBox(height: Spacing.small),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  imagens(ClientAccountEntity? account) {
    if (account != null) {
      if (account.photo != null && account.photo != " ") {
        String url = account.photo!;
        url;
        return NetworkImage(url);
      } else {
        return AssetImage(AppImages.noProfilePicture.path);
      }
    } else {
      return AssetImage(AppImages.noProfilePicture.path);
    }
  }
}

class _OptionForAuthUser extends StatelessWidget {
  const _OptionForAuthUser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ListTile(
          title: const Text('Pregão', style: TextStyle(color: Colors.white)),
          leading: Image.asset(
            AppImages.hammer.path,
            width: 30,
            height: 30,
          ),
          onTap: () => Modular.to.navigate('/trading/'),
        ),
        ListTile(
          title:
              const Text('Venda Direta', style: TextStyle(color: Colors.white)),
          leading: Image.asset(AppImages.quotation.path),
          onTap: () => Modular.to.navigate('/direct/'),
        ),
      ],
    );
  }
}
