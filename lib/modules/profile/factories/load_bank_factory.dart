import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/contracts/load_banks.dart';
import 'package:give_life/modules/profile/usecases/remote_load_banks.dart';
import 'package:give_life/shared/data/factories/api_url_factory.dart';
import 'package:give_life/shared/data/http/http_client.dart';

LoadBanks makeLoadBank() => RemoteLoadBanks(
      url: makeApiUrl('/banks'),
      httpClient: Modular.get<HttpClient>(),
    );
