import 'package:give_life/modules/auth/models/forgot/recover_account_reset.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';
import 'package:meta/meta.dart';

class RemoteRecoverAccountReset implements RecoverAccountReset {
  const RemoteRecoverAccountReset({
    required this.httpClient,
    required this.url,
  });

  final String url;
  final HttpClient httpClient;

  @override
  Future<String?> recover(InputRecoverAccountReset input) async {
    final body = InputRemoteRecoverAccountReset.fromDomain(input).toMap();
    body;
    try {
      var resul =
          await httpClient.request(url: url, method: 'post', body: body);
      return resul['message'] as String;
    } on HttpError catch (error) {
      throw error == HttpError.unauthorized
          ? DomainError.invalidCredentials
          : DomainError.unexpected;
    }
  }
}

@immutable
class InputRemoteRecoverAccountReset {
  const InputRemoteRecoverAccountReset(this.code, this.email, this.password);

  final String code;
  final String email;
  final String password;

  factory InputRemoteRecoverAccountReset.fromDomain(
          InputRecoverAccountReset input) =>
      InputRemoteRecoverAccountReset(input.code, input.email, input.password);

  Map<String, dynamic> toMap() =>
      {'code': code, 'email': email, 'password': password};
}
