import 'package:equatable/equatable.dart';
import 'package:give_life/modules/profile/models/account_profile_model.dart';
import 'package:give_life/modules/profile/models/document_model.dart';
import 'package:give_life/modules/profile/models/property_model.dart';

class ClientProfileEntity extends Equatable {
  const ClientProfileEntity({
    required this.id,
    required this.name,
    required this.email,
    this.photo,
    this.phone1,
    this.personId,
    this.phone2,
    this.nif,
    this.address,
    this.cityId,
    this.zipCode,
    this.rg,
    this.district,
    this.complement,
    this.profession,
    this.property,
    this.account,
    this.date,
    this.number,
    required this.documents,
  });

  final int id;
  final int? personId;
  final String name;
  final String? date;
  final String? photo;
  final String? zipCode;
  final String email;
  final String? phone1;
  final String? phone2;
  final String? nif;
  final String? rg;
  final String? address;
  final String? district;
  final String? complement;
  final String? profession;
  final String? number;
  final int? cityId;
  final PropertyModel? property;
  final AccountModel? account;
  final List<DocumentModel> documents;

  @override
  List<Object?> get props => [
        id,
        name,
        email,
        phone1,
        phone2,
        nif,
        address,
        cityId,
        photo,
        zipCode,
        rg,
        district,
        complement,
        profession,
        property,
        account,
        documents,
        date,
        number
      ];
}
