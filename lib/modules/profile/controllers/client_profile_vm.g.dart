// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'client_profile_vm.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ClientProfileVM on _ClientProfileVMBase, Store {
  late final _$nameAtom =
      Atom(name: '_ClientProfileVMBase.name', context: context);

  @override
  String? get name {
    _$nameAtom.reportRead();
    return super.name;
  }

  @override
  set name(String? value) {
    _$nameAtom.reportWrite(value, super.name, () {
      super.name = value;
    });
  }

  late final _$districtAtom =
      Atom(name: '_ClientProfileVMBase.district', context: context);

  @override
  String? get district {
    _$districtAtom.reportRead();
    return super.district;
  }

  @override
  set district(String? value) {
    _$districtAtom.reportWrite(value, super.district, () {
      super.district = value;
    });
  }

  late final _$districtErrorAtom =
      Atom(name: '_ClientProfileVMBase.districtError', context: context);

  @override
  String? get districtError {
    _$districtErrorAtom.reportRead();
    return super.districtError;
  }

  @override
  set districtError(String? value) {
    _$districtErrorAtom.reportWrite(value, super.districtError, () {
      super.districtError = value;
    });
  }

  late final _$complementAtom =
      Atom(name: '_ClientProfileVMBase.complement', context: context);

  @override
  String? get complement {
    _$complementAtom.reportRead();
    return super.complement;
  }

  @override
  set complement(String? value) {
    _$complementAtom.reportWrite(value, super.complement, () {
      super.complement = value;
    });
  }

  late final _$complementErrorAtom =
      Atom(name: '_ClientProfileVMBase.complementError', context: context);

  @override
  String? get complementError {
    _$complementErrorAtom.reportRead();
    return super.complementError;
  }

  @override
  set complementError(String? value) {
    _$complementErrorAtom.reportWrite(value, super.complementError, () {
      super.complementError = value;
    });
  }

  late final _$propertyNameAtom =
      Atom(name: '_ClientProfileVMBase.propertyName', context: context);

  @override
  String? get propertyName {
    _$propertyNameAtom.reportRead();
    return super.propertyName;
  }

  @override
  set propertyName(String? value) {
    _$propertyNameAtom.reportWrite(value, super.propertyName, () {
      super.propertyName = value;
    });
  }

  late final _$propertyNameErrorAtom =
      Atom(name: '_ClientProfileVMBase.propertyNameError', context: context);

  @override
  String? get propertyNameError {
    _$propertyNameErrorAtom.reportRead();
    return super.propertyNameError;
  }

  @override
  set propertyNameError(String? value) {
    _$propertyNameErrorAtom.reportWrite(value, super.propertyNameError, () {
      super.propertyNameError = value;
    });
  }

  late final _$propertyStateRegistrationAtom = Atom(
      name: '_ClientProfileVMBase.propertyStateRegistration', context: context);

  @override
  String? get propertyStateRegistration {
    _$propertyStateRegistrationAtom.reportRead();
    return super.propertyStateRegistration;
  }

  @override
  set propertyStateRegistration(String? value) {
    _$propertyStateRegistrationAtom
        .reportWrite(value, super.propertyStateRegistration, () {
      super.propertyStateRegistration = value;
    });
  }

  late final _$propertyStateRegistrationErrorAtom = Atom(
      name: '_ClientProfileVMBase.propertyStateRegistrationError',
      context: context);

  @override
  String? get propertyStateRegistrationError {
    _$propertyStateRegistrationErrorAtom.reportRead();
    return super.propertyStateRegistrationError;
  }

  @override
  set propertyStateRegistrationError(String? value) {
    _$propertyStateRegistrationErrorAtom
        .reportWrite(value, super.propertyStateRegistrationError, () {
      super.propertyStateRegistrationError = value;
    });
  }

  late final _$photoAtom =
      Atom(name: '_ClientProfileVMBase.photo', context: context);

  @override
  String? get photo {
    _$photoAtom.reportRead();
    return super.photo;
  }

  @override
  set photo(String? value) {
    _$photoAtom.reportWrite(value, super.photo, () {
      super.photo = value;
    });
  }

  late final _$zipCodeAtom =
      Atom(name: '_ClientProfileVMBase.zipCode', context: context);

  @override
  String? get zipCode {
    _$zipCodeAtom.reportRead();
    return super.zipCode;
  }

  @override
  set zipCode(String? value) {
    _$zipCodeAtom.reportWrite(value, super.zipCode, () {
      super.zipCode = value;
    });
  }

  late final _$zipCodeErrorAtom =
      Atom(name: '_ClientProfileVMBase.zipCodeError', context: context);

  @override
  String? get zipCodeError {
    _$zipCodeErrorAtom.reportRead();
    return super.zipCodeError;
  }

  @override
  set zipCodeError(String? value) {
    _$zipCodeErrorAtom.reportWrite(value, super.zipCodeError, () {
      super.zipCodeError = value;
    });
  }

  late final _$nameErrorAtom =
      Atom(name: '_ClientProfileVMBase.nameError', context: context);

  @override
  String? get nameError {
    _$nameErrorAtom.reportRead();
    return super.nameError;
  }

  @override
  set nameError(String? value) {
    _$nameErrorAtom.reportWrite(value, super.nameError, () {
      super.nameError = value;
    });
  }

  late final _$emailAtom =
      Atom(name: '_ClientProfileVMBase.email', context: context);

  @override
  String? get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String? value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  late final _$emailErrorAtom =
      Atom(name: '_ClientProfileVMBase.emailError', context: context);

  @override
  String? get emailError {
    _$emailErrorAtom.reportRead();
    return super.emailError;
  }

  @override
  set emailError(String? value) {
    _$emailErrorAtom.reportWrite(value, super.emailError, () {
      super.emailError = value;
    });
  }

  late final _$phone1Atom =
      Atom(name: '_ClientProfileVMBase.phone1', context: context);

  @override
  String? get phone1 {
    _$phone1Atom.reportRead();
    return super.phone1;
  }

  @override
  set phone1(String? value) {
    _$phone1Atom.reportWrite(value, super.phone1, () {
      super.phone1 = value;
    });
  }

  late final _$phone1ErrorAtom =
      Atom(name: '_ClientProfileVMBase.phone1Error', context: context);

  @override
  String? get phone1Error {
    _$phone1ErrorAtom.reportRead();
    return super.phone1Error;
  }

  @override
  set phone1Error(String? value) {
    _$phone1ErrorAtom.reportWrite(value, super.phone1Error, () {
      super.phone1Error = value;
    });
  }

  late final _$nifAtom =
      Atom(name: '_ClientProfileVMBase.nif', context: context);

  @override
  String? get nif {
    _$nifAtom.reportRead();
    return super.nif;
  }

  @override
  set nif(String? value) {
    _$nifAtom.reportWrite(value, super.nif, () {
      super.nif = value;
    });
  }

  late final _$nifErrorAtom =
      Atom(name: '_ClientProfileVMBase.nifError', context: context);

  @override
  String? get nifError {
    _$nifErrorAtom.reportRead();
    return super.nifError;
  }

  @override
  set nifError(String? value) {
    _$nifErrorAtom.reportWrite(value, super.nifError, () {
      super.nifError = value;
    });
  }

  late final _$rgAtom = Atom(name: '_ClientProfileVMBase.rg', context: context);

  @override
  String? get rg {
    _$rgAtom.reportRead();
    return super.rg;
  }

  @override
  set rg(String? value) {
    _$rgAtom.reportWrite(value, super.rg, () {
      super.rg = value;
    });
  }

  late final _$rgErrorAtom =
      Atom(name: '_ClientProfileVMBase.rgError', context: context);

  @override
  String? get rgError {
    _$rgErrorAtom.reportRead();
    return super.rgError;
  }

  @override
  set rgError(String? value) {
    _$rgErrorAtom.reportWrite(value, super.rgError, () {
      super.rgError = value;
    });
  }

  late final _$addressAtom =
      Atom(name: '_ClientProfileVMBase.address', context: context);

  @override
  String? get address {
    _$addressAtom.reportRead();
    return super.address;
  }

  @override
  set address(String? value) {
    _$addressAtom.reportWrite(value, super.address, () {
      super.address = value;
    });
  }

  late final _$addressErrorAtom =
      Atom(name: '_ClientProfileVMBase.addressError', context: context);

  @override
  String? get addressError {
    _$addressErrorAtom.reportRead();
    return super.addressError;
  }

  @override
  set addressError(String? value) {
    _$addressErrorAtom.reportWrite(value, super.addressError, () {
      super.addressError = value;
    });
  }

  late final _$propertyAddressAtom =
      Atom(name: '_ClientProfileVMBase.propertyAddress', context: context);

  @override
  String? get propertyAddress {
    _$propertyAddressAtom.reportRead();
    return super.propertyAddress;
  }

  @override
  set propertyAddress(String? value) {
    _$propertyAddressAtom.reportWrite(value, super.propertyAddress, () {
      super.propertyAddress = value;
    });
  }

  late final _$propertyAddressErrorAtom =
      Atom(name: '_ClientProfileVMBase.propertyAddressError', context: context);

  @override
  String? get propertyAddressError {
    _$propertyAddressErrorAtom.reportRead();
    return super.propertyAddressError;
  }

  @override
  set propertyAddressError(String? value) {
    _$propertyAddressErrorAtom.reportWrite(value, super.propertyAddressError,
        () {
      super.propertyAddressError = value;
    });
  }

  late final _$cityAtom =
      Atom(name: '_ClientProfileVMBase.city', context: context);

  @override
  int? get city {
    _$cityAtom.reportRead();
    return super.city;
  }

  @override
  set city(int? value) {
    _$cityAtom.reportWrite(value, super.city, () {
      super.city = value;
    });
  }

  late final _$cityErrorAtom =
      Atom(name: '_ClientProfileVMBase.cityError', context: context);

  @override
  String? get cityError {
    _$cityErrorAtom.reportRead();
    return super.cityError;
  }

  @override
  set cityError(String? value) {
    _$cityErrorAtom.reportWrite(value, super.cityError, () {
      super.cityError = value;
    });
  }

  late final _$propertyCityAtom =
      Atom(name: '_ClientProfileVMBase.propertyCity', context: context);

  @override
  int? get propertyCity {
    _$propertyCityAtom.reportRead();
    return super.propertyCity;
  }

  @override
  set propertyCity(int? value) {
    _$propertyCityAtom.reportWrite(value, super.propertyCity, () {
      super.propertyCity = value;
    });
  }

  late final _$propertyCityErrorAtom =
      Atom(name: '_ClientProfileVMBase.propertyCityError', context: context);

  @override
  String? get propertyCityError {
    _$propertyCityErrorAtom.reportRead();
    return super.propertyCityError;
  }

  @override
  set propertyCityError(String? value) {
    _$propertyCityErrorAtom.reportWrite(value, super.propertyCityError, () {
      super.propertyCityError = value;
    });
  }

  late final _$bankIdAtom =
      Atom(name: '_ClientProfileVMBase.bankId', context: context);

  @override
  int? get bankId {
    _$bankIdAtom.reportRead();
    return super.bankId;
  }

  @override
  set bankId(int? value) {
    _$bankIdAtom.reportWrite(value, super.bankId, () {
      super.bankId = value;
    });
  }

  late final _$bankIdErrorAtom =
      Atom(name: '_ClientProfileVMBase.bankIdError', context: context);

  @override
  String? get bankIdError {
    _$bankIdErrorAtom.reportRead();
    return super.bankIdError;
  }

  @override
  set bankIdError(String? value) {
    _$bankIdErrorAtom.reportWrite(value, super.bankIdError, () {
      super.bankIdError = value;
    });
  }

  late final _$bankAccountAtom =
      Atom(name: '_ClientProfileVMBase.bankAccount', context: context);

  @override
  String? get bankAccount {
    _$bankAccountAtom.reportRead();
    return super.bankAccount;
  }

  @override
  set bankAccount(String? value) {
    _$bankAccountAtom.reportWrite(value, super.bankAccount, () {
      super.bankAccount = value;
    });
  }

  late final _$bankAccountErrorAtom =
      Atom(name: '_ClientProfileVMBase.bankAccountError', context: context);

  @override
  String? get bankAccountError {
    _$bankAccountErrorAtom.reportRead();
    return super.bankAccountError;
  }

  @override
  set bankAccountError(String? value) {
    _$bankAccountErrorAtom.reportWrite(value, super.bankAccountError, () {
      super.bankAccountError = value;
    });
  }

  late final _$bankTypeAtom =
      Atom(name: '_ClientProfileVMBase.bankType', context: context);

  @override
  int? get bankType {
    _$bankTypeAtom.reportRead();
    return super.bankType;
  }

  @override
  set bankType(int? value) {
    _$bankTypeAtom.reportWrite(value, super.bankType, () {
      super.bankType = value;
    });
  }

  late final _$bankTypeErrorAtom =
      Atom(name: '_ClientProfileVMBase.bankTypeError', context: context);

  @override
  int? get bankTypeError {
    _$bankTypeErrorAtom.reportRead();
    return super.bankTypeError;
  }

  @override
  set bankTypeError(int? value) {
    _$bankTypeErrorAtom.reportWrite(value, super.bankTypeError, () {
      super.bankTypeError = value;
    });
  }

  late final _$bankAgencyAtom =
      Atom(name: '_ClientProfileVMBase.bankAgency', context: context);

  @override
  String? get bankAgency {
    _$bankAgencyAtom.reportRead();
    return super.bankAgency;
  }

  @override
  set bankAgency(String? value) {
    _$bankAgencyAtom.reportWrite(value, super.bankAgency, () {
      super.bankAgency = value;
    });
  }

  late final _$bankAgencyErrorAtom =
      Atom(name: '_ClientProfileVMBase.bankAgencyError', context: context);

  @override
  String? get bankAgencyError {
    _$bankAgencyErrorAtom.reportRead();
    return super.bankAgencyError;
  }

  @override
  set bankAgencyError(String? value) {
    _$bankAgencyErrorAtom.reportWrite(value, super.bankAgencyError, () {
      super.bankAgencyError = value;
    });
  }

  late final _$dateAtom =
      Atom(name: '_ClientProfileVMBase.date', context: context);

  @override
  String? get date {
    _$dateAtom.reportRead();
    return super.date;
  }

  @override
  set date(String? value) {
    _$dateAtom.reportWrite(value, super.date, () {
      super.date = value;
    });
  }

  late final _$dateErrorAtom =
      Atom(name: '_ClientProfileVMBase.dateError', context: context);

  @override
  String? get dateError {
    _$dateErrorAtom.reportRead();
    return super.dateError;
  }

  @override
  set dateError(String? value) {
    _$dateErrorAtom.reportWrite(value, super.dateError, () {
      super.dateError = value;
    });
  }

  late final _$numberAtom =
      Atom(name: '_ClientProfileVMBase.number', context: context);

  @override
  String? get number {
    _$numberAtom.reportRead();
    return super.number;
  }

  @override
  set number(String? value) {
    _$numberAtom.reportWrite(value, super.number, () {
      super.number = value;
    });
  }

  late final _$numberErrorAtom =
      Atom(name: '_ClientProfileVMBase.numberError', context: context);

  @override
  String? get numberError {
    _$numberErrorAtom.reportRead();
    return super.numberError;
  }

  @override
  set numberError(String? value) {
    _$numberErrorAtom.reportWrite(value, super.numberError, () {
      super.numberError = value;
    });
  }

  @override
  String toString() {
    return '''
name: ${name},
district: ${district},
districtError: ${districtError},
complement: ${complement},
complementError: ${complementError},
propertyName: ${propertyName},
propertyNameError: ${propertyNameError},
propertyStateRegistration: ${propertyStateRegistration},
propertyStateRegistrationError: ${propertyStateRegistrationError},
photo: ${photo},
zipCode: ${zipCode},
zipCodeError: ${zipCodeError},
nameError: ${nameError},
email: ${email},
emailError: ${emailError},
phone1: ${phone1},
phone1Error: ${phone1Error},
nif: ${nif},
nifError: ${nifError},
rg: ${rg},
rgError: ${rgError},
address: ${address},
addressError: ${addressError},
propertyAddress: ${propertyAddress},
propertyAddressError: ${propertyAddressError},
city: ${city},
cityError: ${cityError},
propertyCity: ${propertyCity},
propertyCityError: ${propertyCityError},
bankId: ${bankId},
bankIdError: ${bankIdError},
bankAccount: ${bankAccount},
bankAccountError: ${bankAccountError},
bankType: ${bankType},
bankTypeError: ${bankTypeError},
bankAgency: ${bankAgency},
bankAgencyError: ${bankAgencyError},
date: ${date},
dateError: ${dateError},
number: ${number},
numberError: ${numberError}
    ''';
  }
}
