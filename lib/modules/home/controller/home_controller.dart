import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/home/factories/load_auctions.dart';
import 'package:give_life/modules/home/model/entity_%20auctions.dart';
import 'package:mobx/mobx.dart';

part 'home_controller.g.dart';

class HomeController = HomeControllerBase with _$HomeController;

abstract class HomeControllerBase with Store {
  HomeControllerBase({required this.loadAuctions});

  final LoadAuctions loadAuctions;

  List<AuctionsEntity>? auctions;
  List<AuctionsEntity>? auction;
  List<AuctionsEntity>? auctionsOnline;
  @observable
  bool _isLoading = true;

  bool get isLoading => _isLoading;

  set isLoading(bool value) => _isLoading = value;

  Future<void> loadData() async {
    isLoading = true;
    try {
      auctions = await loadAuctions.load();
      auction = auctions
          ?.where((element) => element.isOnline == 0 && element.isEnabled == 1)
          .toList();
      auctionsOnline = auctions
          ?.where((element) => element.isOnline == 1 && element.isEnabled == 1)
          .toList();
      isLoading = false;
    } catch (e) {
      isLoading = false;
      print(e);
    }
  }

  toDetail(int id) {
    Modular.to.pushNamed('/home/details/$id');
  }
}
