import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/shared/componets/constants/spacing.dart';

class ButtonsNextWidget extends StatelessWidget {
  const ButtonsNextWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox(width: Spacing.large),
            Observer(
              builder: (_) {
                return controller.next > 0
                    ? Expanded(
                        child: OutlinedButton(
                          onPressed: () {
                            int v = controller.next -= 1;
                            controller.setNext(v);
                          },
                          child: const Text('< ANTERIOR'),
                        ),
                      )
                    : Container();
              },
            ),
            const SizedBox(width: Spacing.small),
            Expanded(
              child: ElevatedButton(
                onPressed: () {
                  int v = controller.next += 1;
                  controller.setNext(v);
                },
                child: const Text('PRÓXIMO >'),
              ),
            ),
            const SizedBox(width: Spacing.xSmall),
          ],
        ),
      ],
    );
  }
}
