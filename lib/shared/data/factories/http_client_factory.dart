import 'package:give_life/shared/data/http/http_adapter.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:http/http.dart';

HttpClient makeHttpAdapter() => HttpAdapter(Client());
