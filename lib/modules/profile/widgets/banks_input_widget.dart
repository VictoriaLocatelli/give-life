import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/modules/profile/entity/bank_entity.dart';
import 'package:give_life/shared/componets/widgets/fields/dropdown_dialog_search_select_field.dart';

class BanksInputWidget extends StatelessWidget {
  const BanksInputWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();

    return DropdownDialogSearchSelectField<BankEntity?>(
      isRequired: false,
      label: 'Banco',
      validator: controller.bankIdValidator,
      items: controller.banks,
      selectedItem: controller.currentbBank,
    );
  }
}
