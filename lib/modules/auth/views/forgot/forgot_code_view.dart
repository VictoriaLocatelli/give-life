import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';

import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/shared/componets/constants/spacing.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';

import '../../controllers/forgot/forgot_controller.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';

class ForgotCodeView extends StatelessWidget {
  const ForgotCodeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return GestureDetector(
      onTap: () => FocusScope.of(context).focusedChild?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Recuperar senha'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(15),
          child: Container(
            height: double.infinity,
            width: double.infinity,
            padding: const EdgeInsets.symmetric(
              vertical: Spacing.large,
              horizontal: Spacing.medium,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Verificação de código',
                  textAlign: TextAlign.center,
                  style: textTheme.headline6
                      ?.copyWith(fontWeight: FontWeight.w900),
                ),
                const SizedBox(height: Spacing.small),
                const Text(
                  'Informe o codigo que foi enviado no seu e-mail.',
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: Spacing.medium),
                Center(
                  child: Container(
                    width: 200,
                    height: 200,
                    child: _CodeInput(),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _SendButton extends StatefulWidget {
  const _SendButton({Key? key}) : super(key: key);

  @override
  State<_SendButton> createState() => _SendButtonState();
}

class _SendButtonState extends State<_SendButton> {
  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ForgotController>();
    String value = '0';
    setState(() {
      value;
    });
    return Observer(builder: (_) {
      return ElevatedButton(
        onPressed: value.length >= 6 ? controller.checkCode : null,
        child: controller.isLoading
            ? const CircularProgressIndicator()
            : const Text('Enviar'),
      );
    });
  }
}

class _CodeInput extends StatefulWidget {
  _CodeInput({Key? key}) : super(key: key);

  @override
  State<_CodeInput> createState() => _CodeInputState();
}

class _CodeInputState extends State<_CodeInput> {
  int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * 60 * 5;
  String value = '0';

  @override
  Widget build(BuildContext context) {
    void onEnd() {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        duration: Duration(seconds: 20),
        action: SnackBarAction(
            textColor: Colors.white,
            label: "Voltar",
            onPressed: () {
              Modular.to.navigate('forgot');
            }),
        content: Text(
          "Seu tempo expirou! Solicite um novo código \n",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Color.fromARGB(213, 255, 38, 74),
      ));
      Future.delayed(Duration(seconds: 20), () {
        Modular.to.navigate('forgot');
      });
    }

    final controllerTime =
        CountdownTimerController(endTime: endTime, onEnd: onEnd);
    final controller = Modular.get<ForgotController>();
    String? codeErro = null;

    setState(() {
      value;
    });
    return Observer(builder: (_) {
      return Column(
        children: [
          Flexible(
            flex: 5,
            child: CustomTextFormField(
              maxLength: 6,
              label: 'Código',
              onChanged: (text) {
                controller.code = text;
                controller.contexts = context;
                setState(() {
                  value = text;
                });
              },
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.grey.shade200,
                prefixIcon: const Icon(Icons.code),
                errorText: codeErro,
              ),
            ),
          ),
          Flexible(
            flex: 1,
            child: Transform.translate(
              offset: Offset(0, -40),
              child: CountdownTimer(
                controller: controllerTime,
                onEnd: onEnd,
                endTime: endTime,
                endWidget: Container(),
              ),
            ),
          ),
          Flexible(
              flex: 3,
              child: ElevatedButton(
                onPressed: value.length >= 6 ? controller.checkCode : null,
                child: controller.isLoading
                    ? const CircularProgressIndicator()
                    : const Text('Verificar código'),
              )),
        ],
      );
    });
  }
}
