import 'package:give_life/modules/direct_selling/models/lot_entity.dart';

abstract class LoadLots {
  Future<List<LotEntity>?> loadAll();
  Future<LotEntity?> load(int id);
}
