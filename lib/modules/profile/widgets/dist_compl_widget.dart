import 'package:flutter/cupertino.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/shared/componets/widgets/fields/fields.dart';

class DistComplWidget extends StatelessWidget {
  const DistComplWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();

    return Observer(builder: (context) {
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: CustomTextFormField(
              isRequired: false,
              label: "Bairro",
              initialValue: controller.district,
              validator: controller.districtValidator,
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
            child: CustomTextFormField(
              isRequired: false,
              label: "Complemento",
              initialValue: controller.complement,
              validator: controller.complementValidator,
            ),
          ),
        ],
      );
    });
  }
}
