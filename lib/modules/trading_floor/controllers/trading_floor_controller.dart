import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/trading_floor/factories/make_load_lots.dart';
import 'package:give_life/modules/trading_floor/models/lot_entity.dart';

import 'package:mobx/mobx.dart';

part 'trading_floor_controller.g.dart';

class TradingFloorController = TradingFloorControllerBase
    with _$TradingFloorController;

abstract class TradingFloorControllerBase with Store {
  TradingFloorControllerBase({
    required LoadLots loadLots,
  }) : _loadLots = loadLots;

  final LoadLots _loadLots;
  List<LotEntity>? lotAll = [];
  List<LotEntity> lotAllEnable = [];
  List<LotEntity>? lotAllOthers = [];

  String email = '';
  String phone = '';

  @observable
  bool _isLoading = false;

  @computed
  bool get isLoading => _isLoading;

  set isLoading(bool value) => _isLoading = value;

  Future<void> loadData() async {
    try {
      isLoading = true;
      lotAll = (await _loadLots.loadAll())
          ?.where((LotEntity lot) => lot.tradingFloor!.modality == 'P')
          .toList();
      if (lotAll != null) {
        for (int i = 0; i < lotAll!.length; i++) {
          if (lotAll![i].status == 2) {
            lotAllEnable!.add(lotAll![i]);
          } else {
            lotAllOthers!.add(lotAll![i]);
          }
        }
      }
      lotAllEnable;
      isLoading = false;
    } catch (e) {
      print(e);
      isLoading = false;
    }
  }

  toDatails(int id) {
    Modular.to.pushNamed('/trading/details/$id');
  }

  String phoneFormat(String value) {
    final pattern = RegExp(r'(\d{2})(\d{5})(\d+)');

    return value.replaceAllMapped(
        pattern, (Match m) => '(${m[1]}) ${m[2]}-${m[3]}');
  }
}
