import 'package:give_life/modules/profile/contracts/load_banks.dart';
import 'package:give_life/modules/profile/entity/bank_entity.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';

class RemoteLoadBanks implements LoadBanks {
  const RemoteLoadBanks({
    required this.url,
    required this.httpClient,
  });

  final String url;
  final HttpClient httpClient;

  @override
  Future<List<BankEntity>> load() async {
    try {
      final response = await httpClient.request(url: url, method: 'get');
      response;

      final List data = response['data'];
      final result =
          data.map((e) => RemoteBankSerializer.fromMap(e).toEntity()).toList();

      result;
      return result;
    } on HttpError {
      throw DomainError.unexpected;
    }
  }
}

//SERIALIZER

class RemoteBankKeys {
  static const String id = 'id';
  static const String nome = 'zip_code';
}

class RemoteBankSerializer {
  RemoteBankSerializer._(
      {this.id,
      this.code,
      this.name,
      this.createdat,
      this.updatedat,
      this.isenabled});
  int? id;
  String? code;
  String? name;
  String? createdat;
  String? updatedat;
  int? isenabled;

  factory RemoteBankSerializer.fromMap(Map<String, dynamic> map) {
    try {
      final id = map['id'] as int?;
      final name = map['name'] as String?;
      final createdat = map['created_at'] as String?;
      final updatedat = map['updated_at'] as String?;
      final code = map['code'] as String?;
      final isenabled = map['is_enabled'] as int?;

      return RemoteBankSerializer._(
          id: id,
          name: name,
          createdat: createdat,
          updatedat: updatedat,
          isenabled: isenabled,
          code: code);
    } catch (error) {
      throw HttpError.invalidData;
    }
  }

  BankEntity toEntity() => BankEntity(
      id: id,
      code: code,
      name: name,
      createdat: createdat,
      updatedat: updatedat,
      isenabled: isenabled);
}
