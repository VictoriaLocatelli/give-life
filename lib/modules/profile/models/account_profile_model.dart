class AccountModel {
    int? bankid;
    String? account;
    String? agency;
    int? type;
    String? clientsince;

    AccountModel({this.bankid, this.account, this.agency, this.type, this.clientsince}); 

    AccountModel.fromJson(Map<String, dynamic> json) {
        bankid = json['bank_id'];
        account = json['account'];
        agency = json['agency'];
        type = json['type'];
        clientsince = json['client_since'];
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data={};
        data['bank_id'] = bankid;
        data['account'] = account;
        data['agency'] = agency;
        data['type'] = type;
        data['client_since'] = clientsince;
        return data;
    }
}