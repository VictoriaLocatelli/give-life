import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/trading_floor/factories/make_load_lots.dart';
import 'package:give_life/modules/trading_floor/factories/save_shot.dart';
import 'package:give_life/modules/trading_floor/models/input_save_shot.dart';
import 'package:give_life/modules/trading_floor/models/lot_entity.dart';
import 'package:give_life/modules/trading_floor/models/lot_images_entity.dart';
import 'package:give_life/modules/trading_floor/views/widget/dialogs_widget.dart';
import 'package:intl/intl.dart';

import 'package:mobx/mobx.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

part 'details_controller.g.dart';

class DetailsController = DetailsControllerBase with _$DetailsController;

abstract class DetailsControllerBase with Store {
  DetailsControllerBase({required LoadLots loadLot, required this.saveShot})
      : _loadLot = loadLot;
  final SaveShot saveShot;
  final LoadLots _loadLot;
  LotEntity? lot;
  int? mainId;
  String email = '';
  String phone = '';

  String getFormatMoney(dynamic value) => value != null
      ? NumberFormat.currency(symbol: 'R\$', locale: 'pt_BR').format(value)
      : '';

  String getFormatDate(String? value) =>
      DateFormat('dd/MM/yyyy').format(DateTime.parse(value!));

  String getFormatHour(String? value) =>
      DateFormat('HH:mm').format(DateTime.parse('2000-01-01 ' + value!));

  @observable
  bool _isLoading = false;

  @observable
  double? bid;

  @action
  void setBid(double value) {
    double current = lot?.lastShot?.value != null
        ? (value + lot!.lastShot!.value!)
        : (value + lot!.initValue!);

    bid = current;

    setButton(true); //Clicado!
  }

  @observable
  bool isButton = false; //Para bloqueado

  @action
  void setButton(bool value) => isButton = value;

  @computed
  bool get isLoading => _isLoading;

  set isLoading(bool value) => _isLoading = value;

  Future<void> loadData(int id) async {
    mainId = id;
    try {
      isLoading = true;
      setButton(false);
      lot = (await _loadLot.load(id));
      lot?.images?.add(LotImagesEntity(urlVideo: lot?.video));
      isLoading = false;
      print(lot?.shots);
    } catch (e) {
      print(e);
    }
  }

  late BuildContext pageContext;

  Future<void> toSaveShot(InputSaveShot data) async {
    var resul = await saveShot.save(data);
    if (resul['message'] != null && !resul['message'].contains('inse')) {
      alertModal(pageContext, resul['message']);
    } else {
      showModal(context: pageContext, action: toBack);
    }
  }

  Future<void> lastShots(int id) async {
    lastBidsModal(context: pageContext, lot: lot!, action: toBack);
  }

  toBack() {
    loadData(mainId!);
  }

  toShot(int id) {
    confirmModal(
        context: pageContext, bid: bid!, lot: lot!, action: toSaveShot);
  }

  toDatails(int id) {
    Modular.to.navigate('/trading/shots/$id');
  }

  toGallery(int id) {
    Modular.to.pushNamed('/trading/gallery/$id');
  }

  String phoneFormat(String? value) {
    final pattern = RegExp(r'(\d{2})(\d{5})(\d+)');

    return value != null
        ? value.replaceAllMapped(
            pattern, (Match m) => '(${m[1]}) ${m[2]}-${m[3]}')
        : " ";
  }

  // modais

  // String getFormatMoney(dynamic value) =>
  //   NumberFormat.currency(symbol: 'R\$', locale: 'pt_BR').format(value);
}
