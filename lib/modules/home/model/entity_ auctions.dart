class AuctionsEntity {
  int id;
  int? type;
  String? name;
  String? description;
  int isEnabled;
  String? image;
  int isOnline;
  DateTime date;
  String time;
  String? local;
  String? address;
  String auctionhouse;
  String city;
  String phone;
  String? link;

  AuctionsEntity({
    required this.id,
    required this.isOnline,
    required this.isEnabled,
    required this.date,
    required this.time,
    required this.auctionhouse,
    required this.city,
    required this.phone,
    this.type,
    this.name,
    this.description,
    this.image,
    this.local,
    this.address,
    this.link,
  });

  factory AuctionsEntity.fromJson(Map<String, dynamic> json) {
    return AuctionsEntity(
        id: json['id'],
        type: json['type'],
        name: json['name'],
        description: json['description'],
        image: json['image'],
        isOnline: json['is_online'],
        date: DateTime.parse(json['date']).toLocal(),
        time: json['time'],
        address: json['address'],
        auctionhouse: json['auctionhouse'],
        city: json['city'],
        phone: json['phone'],
        isEnabled: json['is_enabled'],
        link: json['link']);
  }
}
