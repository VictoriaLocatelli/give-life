/* import 'package:flutter/material.dart';

class AppColors {
<<<<<<< HEAD
  static const Color primary = Color(0xFF1113426);
  static const Color secondary = Color(0xFF2566e2);
=======
  static const Color primary = Color(0xFF111342);
  static const Color secondary = Color(0xFF0D158F);
>>>>>>> ea23e31ad3e1d2992deda023f884a8102f7817c2
  static const Color background = Color(0xffFEFEFE);
  static const Color barrierColor = Colors.black87;
  static const Color rectangle = Color.fromARGB(255, 185, 202, 233);
  static const Color testcolor = Color(0xFFDCDCDC);

  static const Color black = Color(0xff302F3C);
  static const Color gray = Color(0xffA1A1A1);
  static const Color red = Color(0xffEA0000);
  static const Color white = Colors.white;
  static const Color green = Colors.green;
  static const Color buttonGreenColor = Color(0xff65a509);
  static const Color grayButton = Color(0xff495168);

  static const Color secondaryWhite = Color(0xfff6f4f5);

  static const Color text = Color(0xff302f3c);
  static const Color textSecondary = Color(0xff8D8D8E);
  static const Color button = secondary;
  static const Color icon = Color(0xffACABB1);
  static const Color border = Color(0xffC7C7C7);

  static const Color dividerColor = Color(0xffd7d7d7);
}
 */

import 'package:flutter/material.dart';

class AppColors {
  static const Color primary = Color(0xFF1113426);
  static const Color secondary = Color(0xFF2566e2);
  static const Color background = Color(0xffFEFEFE);
  static const Color barrierColor = Colors.black87;
  static const Color rectangle = Color.fromARGB(255, 185, 202, 233);
  static const Color testcolor = Color(0xFFDCDCDC);

  static const Color black = Color(0xff302F3C);
  static const Color gray = Color(0xffA1A1A1);
  static const Color red = Color(0xffEA0000);
  static const Color white = Colors.white;
  static const Color green = Colors.green;
  static const Color buttonGreenColor = Color(0xff65a509);
  static const Color grayButton = Color(0xff495168);

  static const Color secondaryWhite = Color(0xfff6f4f5);

  static const Color text = Color(0xff302f3c);
  static const Color textSecondary = Color(0xff8D8D8E);
  static const Color button = secondary;
  static const Color icon = Color(0xffACABB1);
  static const Color border = Color(0xffC7C7C7);

  static const Color dividerColor = Color(0xffd7d7d7);
}
