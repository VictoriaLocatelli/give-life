import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/direct_selling/factories/make_load_lots.dart';
import 'package:give_life/modules/direct_selling/factories/save_shot.dart';
import 'package:give_life/modules/direct_selling/usescases/remote_save_shot.dart';
import 'package:give_life/shared/data/factories/api_url_factory.dart';
import 'package:give_life/shared/data/http/http_client.dart';

SaveShot makeRemoteSaveshot() => RemoteSaveShot(
      url: makeApiUrl('/shots'),
      httpClient: Modular.get<HttpClient>(),
    );
