import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/help/controllers/help_controller.dart';
import 'package:give_life/modules/help/factories/load_email.dart';
import 'package:give_life/modules/help/views/help_view.dart';

class HelpModule extends Module {
  @override
  List<Module> get imports => const [];

  @override
  List<Bind> get binds => [
        Bind.singleton((i) => makeRemoteLoadEmail()),
        Bind.singleton((i) => HelpController(loadCurrentAccount: i()))
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute('/',
            child: (_, __) => HelpView(
                  controller: Modular.get<HelpController>(),
                ))
      ];
}
