import 'package:equatable/equatable.dart';
import 'package:give_life/modules/profile/models/account_profile_model.dart';
import 'package:give_life/modules/profile/models/property_model.dart';

abstract class UpdateClientProfile {
  Future<void> update(InputUpdateClientProfile input);
}

class InputUpdateClientProfile extends Equatable {
  InputUpdateClientProfile(
      {required this.name,
      required this.email,
      required this.phone1,
      required this.nif,
      required this.address,
      required this.cityId,
      required this.zipCode,
      this.photo,
      this.phone2,
      this.rg,
      this.district,
      this.complement,
      this.profession,
      this.property,
      this.accountModel,
      this.date,
      this.number});

  final String name;
  final String? date;
  final String zipCode;
  final String email;
  final String phone1;
  final String? phone2;
  final String nif;
  final String address;
  final int cityId;
  String? photo;
  final String? rg;
  final String? district;
  final String? complement;
  final String? profession;
  final PropertyModel? property;
  final AccountModel? accountModel;
  final String? number;

  @override
  List<Object?> get props => [
        name,
        email,
        phone1,
        phone2,
        nif,
        address,
        cityId,
        photo,
        zipCode,
        rg,
        district,
        complement,
        profession,
        property,
        accountModel,
        date,
        number
      ];

  @override
  bool? get stringify => true;
}
