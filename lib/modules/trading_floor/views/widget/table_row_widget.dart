import 'package:flutter/material.dart';

TableRow tableRow(String listaNomes) {
  return TableRow(
    children: listaNomes.split('|').map((name) {
      return Container(
        alignment: Alignment.center,
        child: Text(
          name,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(fontSize: 15.0),
        ),
        padding: EdgeInsets.symmetric(horizontal: 3.0, vertical: 6),
      );
    }).toList(),
  );
}