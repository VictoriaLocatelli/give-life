import 'dart:async';

import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/shared/core/entity/client_account_entitie.dart';
import 'package:give_life/shared/data/contracts/local/load_client_current_account.dart';

class AuthGuard extends RouteGuard {
  AuthGuard() : super(redirectTo: '/auth/');

  @override
  FutureOr<bool> canActivate(String path, ParallelRoute route) async {
    await Future.delayed(const Duration(seconds: 1));
    LoadClientCurrentAccount auth = Modular.get<LoadClientCurrentAccount>();
    ClientAccountEntity? account = await auth.load();

    final isAuth = account != null && account.token.isNotEmpty;

    return isAuth;
  }
}
