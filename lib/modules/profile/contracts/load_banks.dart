import 'package:give_life/modules/profile/entity/bank_entity.dart';

abstract class LoadBanks {
  Future<List<BankEntity>> load();
}
