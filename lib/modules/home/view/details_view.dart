import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/home/controller/details_controller.dart';
import 'package:give_life/modules/home/view/widgets/dialogs_widget.dart';
import 'package:give_life/shared/app_config.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:give_life/shared/componets/theme/app_colors.dart';
import 'package:give_life/shared/componets/utils/launch_link.dart';
import 'package:give_life/shared/componets/utils/launch_url.dart';
import 'package:give_life/shared/componets/utils/scaffold_messenger.dart';
import 'package:give_life/shared/componets/widgets/fields/network_fade_image_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailsView extends StatefulWidget {
  const DetailsView({Key? key, required this.controller, required this.id})
      : super(key: key);

  final int id;
  final DetailsController controller;

  @override
  State<DetailsView> createState() => _DetailsViewState();
}

class _DetailsViewState extends State<DetailsView> {
  @override
  void initState() {
    super.initState();
    widget.controller.loadData(widget.id);
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;
    final textButtonStyle = TextButton.styleFrom(
      backgroundColor: Colors.transparent,
      alignment: Alignment.centerLeft,
    );
    return Observer(builder: (context) {
      widget.controller.pageContext = context;
      if (widget.controller.isLoading)
        return const Center(child: CircularProgressIndicator());
      String baseUrl = AppConfig.getInstance().apiStorageUrl;
      String url = '$baseUrl${widget.controller.auction?.image}';
      bool hasImage = true;

      if (widget.controller.auction?.image == null) {
        hasImage = false;
      }

      String phone = widget.controller.getPhone();
      int? day = widget.controller.auction?.date.day;
      int? month = widget.controller.auction?.date.month;
      int? year = widget.controller.auction?.date.year;
      String catalog = widget.controller.auction?.catalog ?? "";
      String baseStorageUrl = AppConfig.getInstance().apiStorageUrl;
      String regulation = widget.controller.auction?.regulation ?? "";
      String? monthString = month != null && month < 10 ? '0$month' : '$month';
      String? dayString = day != null && day < 10 ? '0$day' : '$day';
      String? t = widget.controller.auction?.time;
      List<String> timel = [''];
      timel = t != null ? t.split(":") : ['', ''];
      String time = "${timel[0]}:${timel[1]}";
      String city = widget.controller.auction?.city ?? '';
      String description = widget.controller.auction?.description ?? "";
      String freePhone = widget.controller.auction?.freePhone ?? "";
      String address = widget.controller.auction?.address ?? '';

      TextStyle styleTitle = TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 25,
      );
      TextStyle styleTitle2 = TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 15,
      );

      return Scaffold(
        appBar: AppBar(
          title: Text("Detalhes do Leilão"),
          centerTitle: true,
        ),
        body: Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child: LayoutBuilder(builder: (context, c) {
            return Container(
                width: c.maxWidth,
                child: SingleChildScrollView(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Column(
                      children: [
                        Container(
                          width: c.maxWidth,
                          height: c.maxWidth + 60,
                          child: widget.controller.auction?.image != null
                              ? NetWorkFadeImageWidget(
                                  url: url,
                                  widget: Center(
                                    child: Text(
                                      "$dayString/$monthString/$year ás $time",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 22),
                                    ),
                                  ),
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(AppImages
                                              .noProfilePicture.path))),
                                ),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Text(
                                  '${widget.controller.auction?.name}',
                                  style: styleTitle,
                                ),
                                LayoutBuilder(builder: (context, con) {
                                  return Container(
                                      width: con.maxWidth,
                                      child: Text(
                                        '${widget.controller.auction?.description ?? ""}',
                                        maxLines: 3,
                                      ));
                                }),
                                SizedBox(
                                  height: 15,
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: c.maxWidth,
                          child: Column(
                            children: [
                              Image.asset(AppImages.barra.path, height: 1.7)
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          width: c.maxWidth,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 0),
                                  child: Container(
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          AppImages.localization.path,
                                          width: 25,
                                          color: AppColors.black,
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Container(
                                                width: 250,
                                                child: AutoSizeText(
                                                    '$address, $city')),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                TextButton.icon(
                                  label: Text(
                                    widget.controller.phoneFormat(
                                        widget.controller.auction!.phone! ??
                                            " "),
                                    style: textTheme.bodyText1
                                        ?.copyWith(fontSize: 14),
                                  ),
                                  icon: Icon(
                                    Icons.whatsapp,
                                    color: Colors.green,
                                  ),
                                  style: textButtonStyle,
                                  onPressed: () async => launchWhatsApp(
                                    widget.controller.auction!.phone!,
                                    onFailLaunchingUrl: (value) => showSnackBar(
                                      context,
                                      content: Text(value),
                                      type: SnackBarType.error,
                                    ),
                                  ),
                                ),
                                TextButton.icon(
                                  label: Text(
                                    widget.controller.phoneFormat(
                                        widget.controller.auction!.landline!),
                                    style: textTheme.bodyText1
                                        ?.copyWith(fontSize: 14),
                                  ),
                                  icon: Icon(Icons.phone),
                                  style: textButtonStyle,
                                  onPressed: () async => LaunchUrl.phone(
                                    widget.controller.auction!.phone!,
                                    onFailLaunchingUrl: (value) => showSnackBar(
                                      context,
                                      content: Text(value),
                                      type: SnackBarType.error,
                                    ),
                                  ),
                                ),
                                // Row(
                                //   mainAxisSize: MainAxisSize.min,
                                //   children: [
                                //     Image.asset(
                                //       AppImages.phone.path,
                                //       width: 22,
                                //     ),
                                //     SizedBox(
                                //       width: 10,
                                //     ),
                                //     Text(
                                //       '${widget.controller.auction!.landline}',
                                //       textAlign: TextAlign.left,
                                //     ),
                                //   ],
                                // ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: 215,
                              child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                        AppColors.button),
                                  ),
                                  onPressed: () {
                                    print("$catalog print catálogo");
                                    if (catalog != "") {
                                      launch(baseStorageUrl + catalog);
                                    } else {
                                      alertModal(context,
                                          "Este leilão não possui catálogo ");
                                    }
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text('          CATÁLOGO         '),
                                      Image.asset(AppImages.catalog.path),
                                    ],
                                  )),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: 215,
                              child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                        AppColors.button),
                                  ),
                                  onPressed: () {
                                    //print("$regulation print catálogo");
                                    if (regulation != "") {
                                      launch(baseStorageUrl + regulation);
                                    } else {
                                      alertModal(context,
                                          "Este leilão não possui regulamento!");
                                    }
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text('     REGULAMENTO      '),
                                      Image.asset(AppImages.regulamention.path),
                                    ],
                                  )),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: 215,
                              child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                        AppColors.button),
                                  ),
                                  onPressed: () {
                                    Modular.to.pushNamed(
                                        "/home/gallery/${widget.controller.auction!.id}");
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text('MAIS INFORMAÇÕES '),
                                      Image.asset(AppImages.info.path),
                                    ],
                                  )),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: 215,
                              child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                        AppColors.button),
                                  ),
                                  onPressed: () {
                                    if (widget.controller.lots.isEmpty) {
                                      alertModal(context,
                                          "Este leilão não possui lotes!");
                                    } else {
                                      listLots(
                                          context: context,
                                          message:
                                              "Este leilão não possui lotes!",
                                          lots: widget.controller.lots);
                                    }
                                  },
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text('     LOTES DO LEILÃO    '),
                                      Image.asset(
                                        AppImages.arrow.path,
                                        width: 13,
                                      ),
                                    ],
                                  )),
                            ),

                            /* SizedBox(
                              height: 20,
                            ),
                            Container(
                                width: 330,
                                child: AutoSizeText(
                                  'Conteúdo do Leilão',
                                  style: styleTitle,
                                )),
                            SizedBox(
                              height: 20,
                            ),
                            if (widget.controller.auction?.contents != null)
                              CarrousselWidget(
                                contents: widget.controller.auction!.contents!,
                              ) */
                          ],
                        ),
                      ],
                    ),
                  ],
                )));
          }),
        ),
      );
    });
  }
}
