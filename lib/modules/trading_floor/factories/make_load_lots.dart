import 'package:give_life/modules/trading_floor/models/lot_entity.dart';

abstract class LoadLots {
  Future<List<LotEntity>?> loadAll();
  Future<LotEntity?> load(int id);
}
