import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:give_life/modules/home/controller/details_controller.dart';
import 'package:give_life/modules/home/model/date_config_pattern.dart';
import 'package:give_life/modules/home/model/entity_%20detail_auctions.dart';
import 'package:give_life/modules/home/model/lot_entity.dart';
import 'package:give_life/modules/home/view/widgets/detail_list.dart';
import 'package:give_life/modules/home/view/widgets/youtube_players_widget.dart';
import 'package:give_life/shared/componets/constants/spacing.dart';
import 'package:give_life/shared/componets/theme/app_colors.dart';
import 'package:intl/intl.dart';

TextStyle styleInfoLot = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
TextStyle styleTitleLot = TextStyle(fontSize: 27, fontWeight: FontWeight.bold);
TextStyle styleSubTitle = TextStyle(
    fontSize: 16, fontWeight: FontWeight.bold, color: AppColors.black);

class DetailsLotView extends StatelessWidget {
  const DetailsLotView({super.key, required this.lotId});
  final int lotId;

  String dateFormat(dynamic date) {
    String resul = '';
    if (date is DateTime) {
      resul = DateFormat(DateConfigPattern.pattern, DateConfigPattern.locale)
          .format(date);
    } else if (date is String) {
      resul = DateFormat(DateConfigPattern.pattern, DateConfigPattern.locale)
          .format(DateTime.parse(date).toLocal());
    }
    return resul;
  }

  String hourFormat({hour, date}) {
    String resul = '${date}T$hour';
    return hour != null && date != null
        ? DateFormat('Hm').format(DateTime.parse(resul).toLocal())
        : "";
  }

  @override
  Widget build(BuildContext context) {
    final DetailsAuctionsEntity? auction =
        Modular.get<DetailsController>().auction;
    LotEntity? lot = auction?.lots?.firstWhere((lot) => lot.id == lotId);

    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;
    final textButtonStyle = TextButton.styleFrom(
      backgroundColor: Colors.transparent,
      alignment: Alignment.centerLeft,
    );

    return Scaffold(
      appBar: AppBar(centerTitle: true, title: Text("Detalhe do Lote")),
      body: LayoutBuilder(builder: (context, c) {
        return Container(
          width: c.maxWidth,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  child: lot?.video != null
                      ? YoutubePlayerWidget(
                          url: lot!.video!,
                        )
                      : Container(),
                ),
                Container(
                  //width: c.maxWidth,
                  height: 100,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                const SizedBox(height: Spacing.xxSmall),
                                Text(
                                  '${lot?.name}',
                                  style: styleTitleLot,
                                ),
                              ],
                            ),
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                const SizedBox(height: Spacing.xxSmall),
                                Text(
                                  "R\$",
                                  style: TextStyle(
                                      fontSize: 27,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  '${lot?.initValue!}',
                                  style: styleTitleLot,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(3),
                  padding: const EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5.0),
                    ),
                    border: Border.all(
                      width: 1,
                      color: Color(0xffD9D9D9),
                      style: BorderStyle.solid,
                    ),
                    color: AppColors.secondaryWhite,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const SizedBox(height: Spacing.xxSmall),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        child: Row(
                          children: [
                            Text('Descrição:',
                                textAlign: TextAlign.left,
                                style: styleSubTitle),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        child: Row(
                          children: [
                            Text(
                              lot?.description != null
                                  ? '${lot!.description}'
                                  : ' ',
                              textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                DetailsList(
                  lot: lot!,
                ),
                Container(
                  margin: const EdgeInsets.all(3),
                  padding: const EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5.0),
                    ),
                    border: Border.all(
                      width: 1,
                      color: Color(0xffD9D9D9),
                      style: BorderStyle.solid,
                    ),
                    color: AppColors.secondaryWhite,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const SizedBox(height: Spacing.xxSmall),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        child: Row(
                          children: [
                            Text('Vendedor:',
                                textAlign: TextAlign.left,
                                style: styleSubTitle),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        child: Row(
                          children: [
                            Text(
                              lot.salesman != null ? '${lot.salesman}' : " ",
                              textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(3),
                  padding: const EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5.0),
                    ),
                    border: Border.all(
                      width: 1,
                      color: Color(0xffD9D9D9),
                      style: BorderStyle.solid,
                    ),
                    color: AppColors.secondaryWhite,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const SizedBox(height: Spacing.xxSmall),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        child: Row(
                          children: [
                            Text('Localização:',
                                textAlign: TextAlign.left,
                                style: styleSubTitle),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        child: Row(
                          children: [
                            Text(
                              lot.location != null ? '${lot.location}' : " ",
                              textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(3),
                  padding: const EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5.0),
                    ),
                    border: Border.all(
                      width: 1,
                      color: Color(0xffD9D9D9),
                      style: BorderStyle.solid,
                    ),
                    color: AppColors.secondaryWhite,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const SizedBox(height: Spacing.xxSmall),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        child: Row(
                          children: [
                            Text('Condições de pagamento:',
                                textAlign: TextAlign.left,
                                style: styleSubTitle),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        child: Row(
                          children: [
                            Text(
                              lot.paymentform != null
                                  ? '${lot.paymentform}/${lot.paymentform} '
                                  : " ",
                              textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(3),
                  padding: const EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5.0),
                    ),
                    border: Border.all(
                      width: 1,
                      color: Color(0xffD9D9D9),
                      style: BorderStyle.solid,
                    ),
                    color: AppColors.secondaryWhite,
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const SizedBox(height: Spacing.xxSmall),
                      //if (lot.observation != null)
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        child: Row(
                          children: [
                            Text('Observação:',
                                textAlign: TextAlign.left,
                                style: styleSubTitle),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        child: Row(
                          children: [
                            // if (lot.observation != null)
                            Text(
                              lot.observation != null
                                  ? '${lot.observation}'
                                  : " ",
                              textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
