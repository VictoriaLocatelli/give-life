import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/modules/profile/widgets/image_cropped.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class PhotoInput extends StatefulWidget {
  const PhotoInput({Key? key}) : super(key: key);

  @override
  State<PhotoInput> createState() => PhotoInputState();
}

ImageProvider? avatar;
Image? photoBase64;

class PhotoInputState extends State<PhotoInput> {
  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();
    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;

    ImageProvider<Object> imagens() {
      final controller = Modular.get<ClientProfileController>();
      var account = controller.photo;
      if (account != null) {
        if (account != " ") {
          avatar = NetworkImage(account);
        } else {
          avatar = AssetImage(AppImages.noProfilePicture.path);
        }
      } else {
        avatar = AssetImage(AppImages.noProfilePicture.path);
      }
      return avatar!;
    }

    return Hero(
      tag: imagens(),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 120,
                ),
                IconButton(
                    onPressed: () async {
                      final ImagePicker _picker = ImagePicker();
                      // Pick an image

                      final XFile? image = await _picker.pickImage(
                          preferredCameraDevice: CameraDevice.front,
                          source: ImageSource.gallery);
                      if (image != null) {
                        CroppedFile? resultFile = await ImageCropped(
                                context: context, pickedFile: image)
                            .cropImage();
                        if (resultFile != null) {
                          resultFile.readAsBytes().then((value) {
                            var photo = base64.encode(value);
                            controller.photo = "data:image/png;base64,$photo";
                            photoBase64 = Image.memory(base64Decode(photo));

                            setState(() {
                              photoBase64;
                            });
                          });
                        }
                      }
                    },
                    icon: Icon(size: 35, Icons.camera_alt)),
              ],
            )
          ],
        ),
        height: 150,
        width: 150,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              fit: BoxFit.contain,
              image: photoBase64 != null
                  ? photoBase64!.image
                  : avatar!), //Imagem a ser trocada
        ),
      ),
    );
  }
}
