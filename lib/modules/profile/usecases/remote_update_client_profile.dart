import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/contracts/load_client_profile.dart';
import 'package:give_life/modules/profile/entity/update_client_profile.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/contracts/local/load_client_current_account.dart';
import 'package:give_life/shared/data/contracts/local/save_client_current_account.dart';
import 'package:give_life/shared/data/http/http.dart';

class RemoteUpdateClientProfile implements UpdateClientProfile {
  const RemoteUpdateClientProfile({
    required this.url,
    required this.httpClient,
  });

  final String url;
  final HttpClient httpClient;

  @override
  Future<void> update(InputUpdateClientProfile input) async {
    try {
      final body = InputRemoteUpdateClientProfile.fromDomain(input);
      final account = await Modular.get<LoadClientCurrentAccount>().load();
      body['person_id'] = account?.personId;

      if (body['logo'].contains('http')) {
        body.remove('logo');
      }
      body;
      await httpClient.request(url: url, method: 'put', body: body);
      final response = await Modular.get<LoadClientProfile>().load();
      response;
      if (response.photo != null) account!.setPhoto(response.photo!);
      //account!.setPhoto(AppConfig.getInstance().apiStorageUrl + response.photo!);

      account?.setName(response.name);
      account?.setEmail(response.email);
      if (account != null) {
        await Modular.get<SaveClientCurrentAccount>().save(account);
      }
    } on HttpError {
      throw DomainError.unexpected;
    }
  }
}

class InputRemoteUpdateClientProfile {
  static Map<String, dynamic> fromDomain(InputUpdateClientProfile input) => {
        'name': input.name,
        'email': input.email,
        'birthday': input.date,
        'phone': input.phone1,
        'nif': input.nif,
        'state_registration': input.rg,
        'address': input.address,
        'number': input.number,
        'district': input.district,
        'complement': input.complement,
        'city_id': input.cityId,
        'logo': input.photo,
        'zip_code': input.zipCode,
        'property': input.property?.toJson(),
        'account': input.accountModel?.toJson(),
      };
}
