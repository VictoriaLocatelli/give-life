
class InputSaveShot{
  InputSaveShot({required this.lotId,required this.value, required this.comesFrom});
  final int lotId;
  final double value;
  final String comesFrom;

  toJson(){
    return {
      "lot_id": lotId,
      "value": value,
      "comes_from": comesFrom
    };
  }
}