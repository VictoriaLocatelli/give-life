import 'dart:convert';
import 'dart:developer' as developer;

import 'package:give_life/shared/core/entity/client_account_entitie.dart';
import 'package:give_life/shared/data/contracts/cache/cache_storage.dart';
import 'package:give_life/shared/data/contracts/local/save_client_current_account.dart';
import 'package:give_life/shared/data/serializers/local/local_client_account_serializer.dart';

class LocalSaveCurrentAccount implements SaveClientCurrentAccount {
  const LocalSaveCurrentAccount({required this.secureStorage});

  final CacheStorage secureStorage;

  @override
  Future<void> save(ClientAccountEntity account) async {
    try {
      final accountMap = InputLocalSaveCurrentAccount.fromDomain(account);
      return await secureStorage.save(
          key: 'account', value: jsonEncode(accountMap));
    } catch (error) {
      developer.log('[LocalSaveCurrentAccount] $error');
      throw error;
    }
  }
}

class InputLocalSaveCurrentAccount {
  static Map<String, dynamic> fromDomain(ClientAccountEntity account) => {
        LocalClientAccountKeys.name: account.name,
        LocalClientAccountKeys.photo: account.photo,
        LocalClientAccountKeys.email: account.email,
        LocalClientAccountKeys.token: account.token,
        LocalClientAccountKeys.personId: account.personId,
      };
}
