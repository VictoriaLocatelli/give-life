import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/shared/componets/widgets/fields/dropdown_dialog_search_select_field.dart';
import 'package:give_life/shared/core/entity/city_entity.dart';

class PropertyCityInputWidget extends StatelessWidget {
  const PropertyCityInputWidget({super.key});
//property_city_input_widget
  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();

    return DropdownDialogSearchSelectField<CityEntity>(
      isRequired: false,
      label: 'Cidade',
      validator: controller.propertyCityValidator,
      items: controller.getCities(),
      selectedItem: controller.propertyCity,
    );
  }
}
