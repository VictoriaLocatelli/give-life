import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:brasil_fields/brasil_fields.dart' as brasil_fields;
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';

class RgInputWidget extends StatelessWidget {
  const RgInputWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();

    return Observer(builder: (_) {
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            flex: 5,
            child: CustomTextFormField(
              label: 'CPF',
              initialValue: controller.nif,
              onChanged: controller.nifValidator,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                errorText: controller.nifError,
              ),
              inputFormatters: [
                FilteringTextInputFormatter.digitsOnly,
                brasil_fields.CpfOuCnpjFormatter(),
              ],
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
            flex: 5,
            child: CustomTextFormField(
              onChanged: controller.rgValidator,
              isRequired: false,
              label: 'RG',
              initialValue: controller.rg,
              keyboardType: TextInputType.text,
            ),
          ),
        ],
      );
    });
  }
}
