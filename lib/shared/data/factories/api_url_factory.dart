import 'package:give_life/shared/app_config.dart';

String makeApiUrl(String path) => '${AppConfig.getInstance().apiBaseUrl}$path';
