import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:give_life/modules/direct_selling/models/shot_entity.dart';
import 'package:give_life/modules/direct_selling/views/widget/table_row_widget.dart';
import 'package:give_life/shared/componets/theme/theme.dart';

class LastBidsTradingFlorWidget extends StatelessWidget {
  final List<dynamic>? shots;

  const LastBidsTradingFlorWidget({Key? key, required this.shots})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('${shots} dentro dos shots');
    return Container(
      width: double.infinity,
      margin: EdgeInsets.fromLTRB(15, 0, 15, 10),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(5.0),
        ),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, .05),
            offset: Offset(0.0, 4.0),
            blurRadius: 30.0,
          ),
        ],
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                'Lances',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
              border: Border.all(
                width: 1,
                color: Color(0xffD9D9D9),
                style: BorderStyle.solid,
              ),
            ),
            child: Table(
                columnWidths: {
                  0: FlexColumnWidth(6),
                },
                border: TableBorder(
                  verticalInside: BorderSide(
                    color: Color(0xffD9D9D9),
                    style: BorderStyle.solid,
                    width: 1.0,
                  ),
                ),
                children: [
                  TableRow(
                    children: 'Valor'.split('-').map((name) {
                      return Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: AppColors.primary,
                        ),
                        child: Text(
                          name,
                          style: TextStyle(fontSize: 15.0, color: Colors.white),
                        ),
                        padding:
                            EdgeInsets.symmetric(horizontal: 3.0, vertical: 10),
                      );
                    }).toList(),
                  ),
                  if (shots != null)
                    for (int i = 0; i < shots!.length; i++)
                      tableRow('${shots?[i].getMoney(shots?[i].value)}'),
                ]),
          )
        ],
      ),
    );
  }
}
