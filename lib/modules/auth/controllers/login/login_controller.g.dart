// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$LoginController on _LoginControllerBase, Store {
  Computed<bool>? _$isFormValidComputed;

  @override
  bool get isFormValid =>
      (_$isFormValidComputed ??= Computed<bool>(() => super.isFormValid,
              name: '_LoginControllerBase.isFormValid'))
          .value;

  late final _$_emailAtom =
      Atom(name: '_LoginControllerBase._email', context: context);

  @override
  String? get _email {
    _$_emailAtom.reportRead();
    return super._email;
  }

  @override
  set _email(String? value) {
    _$_emailAtom.reportWrite(value, super._email, () {
      super._email = value;
    });
  }

  late final _$_passwordAtom =
      Atom(name: '_LoginControllerBase._password', context: context);

  @override
  String? get _password {
    _$_passwordAtom.reportRead();
    return super._password;
  }

  @override
  set _password(String? value) {
    _$_passwordAtom.reportWrite(value, super._password, () {
      super._password = value;
    });
  }

  late final _$_emailErrorAtom =
      Atom(name: '_LoginControllerBase._emailError', context: context);

  @override
  String? get _emailError {
    _$_emailErrorAtom.reportRead();
    return super._emailError;
  }

  @override
  set _emailError(String? value) {
    _$_emailErrorAtom.reportWrite(value, super._emailError, () {
      super._emailError = value;
    });
  }

  late final _$_passwordErrorAtom =
      Atom(name: '_LoginControllerBase._passwordError', context: context);

  @override
  String? get _passwordError {
    _$_passwordErrorAtom.reportRead();
    return super._passwordError;
  }

  @override
  set _passwordError(String? value) {
    _$_passwordErrorAtom.reportWrite(value, super._passwordError, () {
      super._passwordError = value;
    });
  }

  late final _$_isObscurePasswordAtom =
      Atom(name: '_LoginControllerBase._isObscurePassword', context: context);

  @override
  bool get _isObscurePassword {
    _$_isObscurePasswordAtom.reportRead();
    return super._isObscurePassword;
  }

  @override
  set _isObscurePassword(bool value) {
    _$_isObscurePasswordAtom.reportWrite(value, super._isObscurePassword, () {
      super._isObscurePassword = value;
    });
  }

  late final _$_isLoadingAtom =
      Atom(name: '_LoginControllerBase._isLoading', context: context);

  @override
  bool get _isLoading {
    _$_isLoadingAtom.reportRead();
    return super._isLoading;
  }

  @override
  set _isLoading(bool value) {
    _$_isLoadingAtom.reportWrite(value, super._isLoading, () {
      super._isLoading = value;
    });
  }

  late final _$_LoginControllerBaseActionController =
      ActionController(name: '_LoginControllerBase', context: context);

  @override
  void _setIsLoading(bool value) {
    final _$actionInfo = _$_LoginControllerBaseActionController.startAction(
        name: '_LoginControllerBase._setIsLoading');
    try {
      return super._setIsLoading(value);
    } finally {
      _$_LoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void toggleObscurePassword() {
    final _$actionInfo = _$_LoginControllerBaseActionController.startAction(
        name: '_LoginControllerBase.toggleObscurePassword');
    try {
      return super.toggleObscurePassword();
    } finally {
      _$_LoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? validateEmail(String? value) {
    final _$actionInfo = _$_LoginControllerBaseActionController.startAction(
        name: '_LoginControllerBase.validateEmail');
    try {
      return super.validateEmail(value);
    } finally {
      _$_LoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? validatePassword(String? value) {
    final _$actionInfo = _$_LoginControllerBaseActionController.startAction(
        name: '_LoginControllerBase.validatePassword');
    try {
      return super.validatePassword(value);
    } finally {
      _$_LoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _clean() {
    final _$actionInfo = _$_LoginControllerBaseActionController.startAction(
        name: '_LoginControllerBase._clean');
    try {
      return super._clean();
    } finally {
      _$_LoginControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isFormValid: ${isFormValid}
    ''';
  }
}
