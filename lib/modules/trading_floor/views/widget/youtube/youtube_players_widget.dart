import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:give_life/shared/componets/theme/theme.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class YoutubePlayerWidget extends StatefulWidget {
  const YoutubePlayerWidget({Key? key, required this.url}) : super(key: key);
  final String url;

  @override
  State<YoutubePlayerWidget> createState() => _YoutubePlayerWidgetState();
}

class _YoutubePlayerWidgetState extends State<YoutubePlayerWidget> {
  @override
  Widget build(BuildContext context) {
    final videoID = YoutubePlayer.convertUrlToId(widget.url);
    YoutubePlayerController youtube_controller = YoutubePlayerController(
      initialVideoId: videoID ?? "",
      flags: const YoutubePlayerFlags(
        autoPlay: false,
      ),
    );
    return YoutubePlayer(
      controller: youtube_controller,
      showVideoProgressIndicator: true,
      onReady: () => debugPrint('Redy'),
      bottomActions: [
        CurrentPosition(),
        ProgressBar(
          isExpanded: true,
          colors: const ProgressBarColors(
              playedColor: AppColors.primary, handleColor: AppColors.secondary),
        ),
        FullScreenButton(
          controller: youtube_controller,
        ),
        const PlaybackSpeedButton(),
      ],
    );
  }
}
