import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:give_life/modules/trading_floor/controllers/shots/shots_controller.dart';
import 'package:give_life/modules/trading_floor/views/widget/last_bids_trading_flor_widget.dart';
import 'package:give_life/modules/trading_floor/views/widget/youtube/youtube_players_widget.dart';

import 'package:give_life/shared/componets/constants/spacing.dart';
import 'package:give_life/shared/componets/theme/theme.dart';
import 'package:give_life/shared/componets/utils/launch_link.dart';
import 'package:give_life/shared/componets/utils/scaffold_messenger.dart';
import 'package:give_life/shared/componets/widgets/bottom_widget.dart';

import 'package:custom_radio_grouped_button/custom_radio_grouped_button.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../../../../shared/componets/constants/app_images.dart';

class ShotTradingView extends StatelessWidget {
  const ShotTradingView({
    super.key,
    required this.id,
    required this.controller,
  });
  final int id;
  final ShotController controller;

  @override
  Widget build(BuildContext context) {
    controller.loadData(id);
    controller.pageContext = context;
    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;
    final textButtonStyle = TextButton.styleFrom(
      backgroundColor: Colors.transparent,
      alignment: Alignment.centerLeft,
    );
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pregão"),
      ),
      body: Observer(builder: (context) {
        if (controller.isLoading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        ImageProvider<Object> img = controller.lot?.images?[0].fileUrl != null
            ? NetworkImage(controller.lot!.images![0].fileUrl!)
            : AssetImage(AppImages.noProfilePicture.path)
                as ImageProvider<Object>;
        return LayoutBuilder(builder: (context, c) {
          return Container(
            width: c.maxWidth,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    child: controller.lot!.video != null
                        ? YoutubePlayerWidget(
                            url: controller.lot!.video!,
                          )
                        : Container(),
                  ),
                  Container(
                    width: c.maxWidth,
                    height: 80,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              const SizedBox(height: Spacing.xxSmall),
                              Text(
                                '${controller.lot!.salesman}',
                                textAlign: TextAlign.left,
                                style: const TextStyle(
                                  color: AppColors.black,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              const SizedBox(height: Spacing.xxSmall),
                              TextButton.icon(
                                label: Text(
                                  controller.lot!.tradingFloor!.desk_one != null
                                      ? controller.phoneFormat(controller
                                          .lot!.tradingFloor!.desk_one)
                                      : " ",
                                  style: textTheme.bodyText2
                                      ?.copyWith(height: 1, fontSize: 16),
                                ),
                                icon: Icon(Icons.whatsapp),
                                style: textButtonStyle,
                                onPressed: () async => launchWhatsApp(
                                  controller.phone,
                                  onFailLaunchingUrl: (value) => showSnackBar(
                                    context,
                                    content: Text(value),
                                    type: SnackBarType.error,
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: c.maxWidth,
                    height: 50,
                    child: Container(
                      margin: const EdgeInsets.all(3),
                      padding: const EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5.0),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: AppColors.black,
                            offset: Offset(0.0, 2.0),
                            blurRadius: 7.0,
                          ),
                        ],
                        color: AppColors.secondaryWhite,
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const SizedBox(height: Spacing.xxSmall),
                          Text(
                            '${controller.lot!.name}',
                            textAlign: TextAlign.left,
                            style: const TextStyle(
                              color: AppColors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            controller.lot!.shots != null
                                ? '${controller.getFormatMoney(controller.lot!.lastShot?.value)}'
                                : " ",
                            textAlign: TextAlign.left,
                            style: const TextStyle(
                              color: AppColors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: c.maxWidth,
                    height: 200,
                    child: Container(
                      margin: const EdgeInsets.all(3),
                      padding: const EdgeInsets.all(3),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                              margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                color: AppColors.secondaryWhite,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(5.0),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, .05),
                                    offset: Offset(0.0, 4.0),
                                    blurRadius: 30.0,
                                  ),
                                ],
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    child: Align(
                                      alignment: Alignment.bottomLeft,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            left: 5.0, bottom: 5),
                                        child: Text(
                                          'Faça seu lance agora',
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                              color: AppColors.secondary),
                                        ),
                                      ),
                                    ),
                                  ),
                                  IgnorePointer(
                                    ignoring: controller.lot!.status == 4 ||
                                            controller.lot!.status == 5 ||
                                            controller.lot!.status == 6
                                        ? true
                                        : false,
                                    child: CustomRadioButton(
                                      elevation: 0,
                                      height: 50,
                                      width: MediaQuery.of(context).size.width /
                                              2 -
                                          40,
                                      wrapAlignment: WrapAlignment.spaceBetween,
                                      enableButtonWrap: true,
                                      unSelectedColor:
                                          controller.lot!.status == 4 ||
                                                  controller.lot!.status == 5 ||
                                                  controller.lot!.status == 6
                                              ? Colors.grey
                                              : Theme.of(context).canvasColor,
                                      buttonLables: [
                                        if (controller.lot!.incValueOne != 0)
                                          '+${controller.getFormatMoney(controller.lot?.incValueOne)}',
                                        if (controller.lot!.incValueTwo != 0)
                                          '+${controller.getFormatMoney(controller.lot?.incValueTwo)}',
                                        if (controller.lot!.incValueThree != 0)
                                          '+${controller.getFormatMoney(controller.lot?.incValueThree)}',
                                        if (controller.lot!.incValueFour != 0)
                                          '+${controller.getFormatMoney(controller.lot?.incValueFour)}',
                                      ],
                                      buttonValues: <double>[
                                        if (controller.lot!.incValueOne != 0)
                                          controller.lot!.incValueOne!,
                                        if (controller.lot!.incValueTwo != 0)
                                          controller.lot!.incValueTwo!,
                                        if (controller.lot!.incValueThree != 0)
                                          controller.lot!.incValueThree!,
                                        if (controller.lot!.incValueFour != 0)
                                          controller.lot!.incValueFour!
                                      ],
                                      buttonTextStyle: ButtonTextStyle(
                                        selectedColor: AppColors.background,
                                        unSelectedColor: Colors.black,
                                        textStyle: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                        ),
                                      ),
                                      radioButtonValue: (value) =>
                                          controller.setBid(value),
                                      enableShape: true,
                                      customShape: RoundedRectangleBorder(
                                        side: BorderSide(),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      selectedColor: AppColors.secondary,
                                    ),
                                  )
                                ],
                              )),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: c.maxWidth,
                    height: 100,
                    child: Column(
                      children: [
                        const SizedBox(height: Spacing.small),
                        Observer(builder: (context) {
                          return OutlinedButton(
                              style: OutlinedButton.styleFrom(
                                  backgroundColor: controller.isButton
                                      ? AppColors.secondary
                                      : AppColors.gray),
                              onPressed: controller.isButton
                                  ? () => controller.toShot(controller.lot!.id!)
                                  : null,
                              child: Text('FAÇA SUA PROPOSTA',
                                  style: const TextStyle(
                                    color: AppColors.white,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  )));
                        }),
                      ],
                    ),
                  ),
                  Container(
                      width: c.maxWidth,
                      height: 300,
                      child: LastBidsTradingFlorWidget(
                        shots: controller.lot?.shots,
                      )),
                ],
              ),
            ),
          );
        });
        //
      }),
    );
  }
}
