import 'package:flutter/material.dart';
import 'package:give_life/shared/componets/constants/spacing.dart';

ThemeData buildThemeData() {
  final primarySwatch = _buildPrimarySwatch();
  final secondarySwatch = _buildSecondarySwatch();
  final textColor = Colors.grey.shade900;

  const iconSize = 24.0;
  const minButtonHeight = 50.0;

  const roundedRectElementsRadius = BorderRadius.all(Radius.circular(8));
  const roundedCornersShape =
      RoundedRectangleBorder(borderRadius: roundedRectElementsRadius);
  const roundedCornersShapeButtons = RoundedRectangleBorder(
    borderRadius: roundedRectElementsRadius,
  );

  const brightness = Brightness.light;

  final textTheme = _buildTextTheme('Roboto', textColor: textColor);

  final colorScheme = ColorScheme.fromSwatch(
    brightness: brightness,
    primarySwatch: primarySwatch,
    accentColor: secondarySwatch,
  );

  // Themes
  const cardTheme = CardTheme(
    elevation: 1,
    margin: EdgeInsets.zero,
    clipBehavior: Clip.antiAlias,
    shape: roundedCornersShape,
  );

  final elevatedButtonTheme = ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      elevation: 0,
      primary: secondarySwatch,
      minimumSize: const Size.fromHeight(minButtonHeight),
      shape: roundedCornersShapeButtons,
    ),
  );

  final outlinedButtonTheme = OutlinedButtonThemeData(
    style: OutlinedButton.styleFrom(
      elevation: 0,
      minimumSize: const Size.fromHeight(minButtonHeight),
      shape: roundedCornersShapeButtons,
      primary: textColor,
      side: BorderSide(color: primarySwatch, width: 1),
    ),
  );

  const floatingActionButtonTheme = FloatingActionButtonThemeData(
    foregroundColor: Colors.white,
  );

  const iconTheme = IconThemeData(color: Colors.grey, size: iconSize);

  final inputDecorationTheme = InputDecorationTheme(
    alignLabelWithHint: true,
    contentPadding: const EdgeInsets.symmetric(
      horizontal: Spacing.small,
      vertical: Spacing.xSmall,
    ),
    enabledBorder: _buildOutlineInputBorder(Colors.grey.shade300),
    focusedBorder: _buildOutlineInputBorder(Colors.grey),
    errorBorder: _buildOutlineInputBorder(colorScheme.error),
    focusedErrorBorder: _buildOutlineInputBorder(colorScheme.error),
  );

  final appBarTheme = AppBarTheme(
    elevation: 0,
    centerTitle: true,
    backgroundColor: primarySwatch,
    foregroundColor: Colors.white,
  );

  return ThemeData(
    brightness: brightness,
    primaryColor: primarySwatch,
    colorScheme: colorScheme,
    textTheme: textTheme,
    primaryTextTheme: textTheme,
    cardTheme: cardTheme,
    elevatedButtonTheme: elevatedButtonTheme,
    outlinedButtonTheme: outlinedButtonTheme,
    iconTheme: iconTheme,
    inputDecorationTheme: inputDecorationTheme,
    appBarTheme: appBarTheme,
    floatingActionButtonTheme: floatingActionButtonTheme,
  );
}

OutlineInputBorder _buildOutlineInputBorder(Color color, [double width = 1.0]) {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(8),
    borderSide: BorderSide(color: color, width: width),
  );
}

MaterialColor _buildPrimarySwatch() {
  const defaultPrimary = Color(0xFF111342);

  return MaterialColor(
    defaultPrimary.value,
    const {
      50: Color(0xFFD2F3FF),
      100: Color(0xFFB1DCEB),
      200: Color(0xFF90C1D3),
      300: Color(0xFF327E96),
      400: Color(0xFF135B6F),
      500: defaultPrimary,
      600: Color(0xFF003343),
      700: Color(0xFF013947),
      800: Color(0xFF013441),
      900: Color(0xFF012F3A),
    },
  );
}

MaterialColor _buildSecondarySwatch() {
  const defaultSecondary = Color(0xFF2566e2);

  return MaterialColor(
    defaultSecondary.value,
    const {
      50: Color(0xFFEAFAE6),
      100: Color(0xFFCAF1C0),
      200: Color(0xFFA4E796),
      300: Color(0xFF79DD68),
      400: Color(0xFF51D541),
      500: defaultSecondary,
      600: Color(0xFF00BD00),
      700: Color(0xFF00A800),
      800: Color(0xFF009400),
      900: Color(0xFF007100),
    },
  );
}

TextTheme _buildTextTheme(String fontFamily, {required Color textColor}) {
  return TextTheme(
    headline1: TextStyle(
      fontFamily: fontFamily,
      fontSize: 96,
      height: 1.17,
      fontWeight: FontWeight.w400,
      color: textColor,
    ),
    headline2: TextStyle(
      fontFamily: fontFamily,
      fontSize: 60,
      height: 1.2,
      fontWeight: FontWeight.w500,
      color: textColor,
    ),
    headline3: TextStyle(
      fontFamily: fontFamily,
      fontSize: 48,
      height: 1,
      fontWeight: FontWeight.w500,
      color: textColor,
    ),
    headline4: TextStyle(
      fontFamily: fontFamily,
      fontSize: 32,
      height: 1.19,
      fontWeight: FontWeight.w400,
      color: textColor,
    ),
    headline5: TextStyle(
      fontFamily: fontFamily,
      fontSize: 24,
      height: 1.17,
      fontWeight: FontWeight.w400,
      color: textColor,
    ),
    headline6: TextStyle(
      fontFamily: fontFamily,
      fontSize: 20,
      height: 1.2,
      fontWeight: FontWeight.w400,
      color: textColor,
    ),
    subtitle1: TextStyle(
      fontFamily: fontFamily,
      fontSize: 16,
      height: 1.25,
      fontWeight: FontWeight.w700,
      color: textColor,
    ),
    subtitle2: TextStyle(
      fontFamily: fontFamily,
      fontSize: 14,
      height: 1.14,
      fontWeight: FontWeight.w500,
      color: textColor,
    ),
    bodyText1: TextStyle(
      fontFamily: fontFamily,
      fontSize: 16,
      height: 1.5,
      fontWeight: FontWeight.w400,
      color: textColor,
    ),
    bodyText2: TextStyle(
      fontFamily: fontFamily,
      fontSize: 14,
      height: 1.57,
      fontWeight: FontWeight.w400,
      color: textColor,
    ),
    button: TextStyle(
      fontFamily: fontFamily,
      fontSize: 16,
      height: 1.25,
      fontWeight: FontWeight.w700,
      color: textColor,
    ),
    caption: TextStyle(
      fontFamily: fontFamily,
      fontSize: 12,
      height: 1.33,
      fontWeight: FontWeight.w400,
      color: textColor,
    ),
    overline: TextStyle(
      fontFamily: fontFamily,
      fontSize: 10,
      height: 1,
      fontWeight: FontWeight.w400,
      color: textColor,
    ),
  );
}
