// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gallery_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$GalleryController on GalleryControllerBase, Store {
  Computed<bool>? _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: 'GalleryControllerBase.isLoading'))
          .value;

  late final _$_isLoadingAtom =
      Atom(name: 'GalleryControllerBase._isLoading', context: context);

  @override
  bool get _isLoading {
    _$_isLoadingAtom.reportRead();
    return super._isLoading;
  }

  @override
  set _isLoading(bool value) {
    _$_isLoadingAtom.reportWrite(value, super._isLoading, () {
      super._isLoading = value;
    });
  }

  late final _$isButtonAtom =
      Atom(name: 'GalleryControllerBase.isButton', context: context);

  @override
  bool get isButton {
    _$isButtonAtom.reportRead();
    return super.isButton;
  }

  @override
  set isButton(bool value) {
    _$isButtonAtom.reportWrite(value, super.isButton, () {
      super.isButton = value;
    });
  }

  late final _$GalleryControllerBaseActionController =
      ActionController(name: 'GalleryControllerBase', context: context);

  @override
  void setButton(bool value) {
    final _$actionInfo = _$GalleryControllerBaseActionController.startAction(
        name: 'GalleryControllerBase.setButton');
    try {
      return super.setButton(value);
    } finally {
      _$GalleryControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isButton: ${isButton},
isLoading: ${isLoading}
    ''';
  }
}
