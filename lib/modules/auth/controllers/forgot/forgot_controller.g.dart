// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'forgot_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ForgotController on _ForgotControllerBase, Store {
  Computed<bool>? _$isFormValidComputed;

  @override
  bool get isFormValid =>
      (_$isFormValidComputed ??= Computed<bool>(() => super.isFormValid,
              name: '_ForgotControllerBase.isFormValid'))
          .value;
  Computed<bool>? _$isFormValidCodeComputed;

  @override
  bool get isFormValidCode =>
      (_$isFormValidCodeComputed ??= Computed<bool>(() => super.isFormValidCode,
              name: '_ForgotControllerBase.isFormValidCode'))
          .value;

  late final _$_emailAtom =
      Atom(name: '_ForgotControllerBase._email', context: context);

  @override
  String? get _email {
    _$_emailAtom.reportRead();
    return super._email;
  }

  @override
  set _email(String? value) {
    _$_emailAtom.reportWrite(value, super._email, () {
      super._email = value;
    });
  }

  late final _$_emailErrorAtom =
      Atom(name: '_ForgotControllerBase._emailError', context: context);

  @override
  String? get _emailError {
    _$_emailErrorAtom.reportRead();
    return super._emailError;
  }

  @override
  set _emailError(String? value) {
    _$_emailErrorAtom.reportWrite(value, super._emailError, () {
      super._emailError = value;
    });
  }

  late final _$_emailTwoAtom =
      Atom(name: '_ForgotControllerBase._emailTwo', context: context);

  @override
  String? get _emailTwo {
    _$_emailTwoAtom.reportRead();
    return super._emailTwo;
  }

  @override
  set _emailTwo(String? value) {
    _$_emailTwoAtom.reportWrite(value, super._emailTwo, () {
      super._emailTwo = value;
    });
  }

  late final _$_codeAtom =
      Atom(name: '_ForgotControllerBase._code', context: context);

  @override
  String? get _code {
    _$_codeAtom.reportRead();
    return super._code;
  }

  @override
  set _code(String? value) {
    _$_codeAtom.reportWrite(value, super._code, () {
      super._code = value;
    });
  }

  late final _$_codeErrorAtom =
      Atom(name: '_ForgotControllerBase._codeError', context: context);

  @override
  String? get _codeError {
    _$_codeErrorAtom.reportRead();
    return super._codeError;
  }

  @override
  set _codeError(String? value) {
    _$_codeErrorAtom.reportWrite(value, super._codeError, () {
      super._codeError = value;
    });
  }

  late final _$_isLoadingAtom =
      Atom(name: '_ForgotControllerBase._isLoading', context: context);

  @override
  bool get _isLoading {
    _$_isLoadingAtom.reportRead();
    return super._isLoading;
  }

  @override
  set _isLoading(bool value) {
    _$_isLoadingAtom.reportWrite(value, super._isLoading, () {
      super._isLoading = value;
    });
  }

  late final _$_contextsAtom =
      Atom(name: '_ForgotControllerBase._contexts', context: context);

  @override
  BuildContext? get _contexts {
    _$_contextsAtom.reportRead();
    return super._contexts;
  }

  @override
  set _contexts(BuildContext? value) {
    _$_contextsAtom.reportWrite(value, super._contexts, () {
      super._contexts = value;
    });
  }

  late final _$_ForgotControllerBaseActionController =
      ActionController(name: '_ForgotControllerBase', context: context);

  @override
  String? validateEmail(String? value) {
    final _$actionInfo = _$_ForgotControllerBaseActionController.startAction(
        name: '_ForgotControllerBase.validateEmail');
    try {
      return super.validateEmail(value);
    } finally {
      _$_ForgotControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _toggleIsLoading() {
    final _$actionInfo = _$_ForgotControllerBaseActionController.startAction(
        name: '_ForgotControllerBase._toggleIsLoading');
    try {
      return super._toggleIsLoading();
    } finally {
      _$_ForgotControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _clean() {
    final _$actionInfo = _$_ForgotControllerBaseActionController.startAction(
        name: '_ForgotControllerBase._clean');
    try {
      return super._clean();
    } finally {
      _$_ForgotControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isFormValid: ${isFormValid},
isFormValidCode: ${isFormValidCode}
    ''';
  }
}
