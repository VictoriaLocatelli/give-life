class DropdownTypeModel{
  const DropdownTypeModel({required this.name, required this.value});
  final String name;
  final int value;
}