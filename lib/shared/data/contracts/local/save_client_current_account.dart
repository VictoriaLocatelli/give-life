import 'package:give_life/shared/core/entity/client_account_entitie.dart';

abstract class SaveClientCurrentAccount {
  Future<void> save(ClientAccountEntity account);
}
