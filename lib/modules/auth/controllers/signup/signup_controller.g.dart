// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$SignUpController on _SignUpControllerBase, Store {
  Computed<bool>? _$isFormValidComputed;

  @override
  bool get isFormValid =>
      (_$isFormValidComputed ??= Computed<bool>(() => super.isFormValid,
              name: '_SignUpControllerBase.isFormValid'))
          .value;

  late final _$_nameAtom =
      Atom(name: '_SignUpControllerBase._name', context: context);

  @override
  String? get _name {
    _$_nameAtom.reportRead();
    return super._name;
  }

  @override
  set _name(String? value) {
    _$_nameAtom.reportWrite(value, super._name, () {
      super._name = value;
    });
  }

  late final _$_phoneAtom =
      Atom(name: '_SignUpControllerBase._phone', context: context);

  @override
  String? get _phone {
    _$_phoneAtom.reportRead();
    return super._phone;
  }

  @override
  set _phone(String? value) {
    _$_phoneAtom.reportWrite(value, super._phone, () {
      super._phone = value;
    });
  }

  late final _$_emailAtom =
      Atom(name: '_SignUpControllerBase._email', context: context);

  @override
  String? get _email {
    _$_emailAtom.reportRead();
    return super._email;
  }

  @override
  set _email(String? value) {
    _$_emailAtom.reportWrite(value, super._email, () {
      super._email = value;
    });
  }

  late final _$_passwordAtom =
      Atom(name: '_SignUpControllerBase._password', context: context);

  @override
  String? get _password {
    _$_passwordAtom.reportRead();
    return super._password;
  }

  @override
  set _password(String? value) {
    _$_passwordAtom.reportWrite(value, super._password, () {
      super._password = value;
    });
  }

  late final _$_passwordConfirmationAtom = Atom(
      name: '_SignUpControllerBase._passwordConfirmation', context: context);

  @override
  String? get _passwordConfirmation {
    _$_passwordConfirmationAtom.reportRead();
    return super._passwordConfirmation;
  }

  @override
  set _passwordConfirmation(String? value) {
    _$_passwordConfirmationAtom.reportWrite(value, super._passwordConfirmation,
        () {
      super._passwordConfirmation = value;
    });
  }

  late final _$_nameErrorAtom =
      Atom(name: '_SignUpControllerBase._nameError', context: context);

  @override
  String? get _nameError {
    _$_nameErrorAtom.reportRead();
    return super._nameError;
  }

  @override
  set _nameError(String? value) {
    _$_nameErrorAtom.reportWrite(value, super._nameError, () {
      super._nameError = value;
    });
  }

  late final _$_phoneErrorAtom =
      Atom(name: '_SignUpControllerBase._phoneError', context: context);

  @override
  String? get _phoneError {
    _$_phoneErrorAtom.reportRead();
    return super._phoneError;
  }

  @override
  set _phoneError(String? value) {
    _$_phoneErrorAtom.reportWrite(value, super._phoneError, () {
      super._phoneError = value;
    });
  }

  late final _$_emailErrorAtom =
      Atom(name: '_SignUpControllerBase._emailError', context: context);

  @override
  String? get _emailError {
    _$_emailErrorAtom.reportRead();
    return super._emailError;
  }

  @override
  set _emailError(String? value) {
    _$_emailErrorAtom.reportWrite(value, super._emailError, () {
      super._emailError = value;
    });
  }

  late final _$_passwordErrorAtom =
      Atom(name: '_SignUpControllerBase._passwordError', context: context);

  @override
  String? get _passwordError {
    _$_passwordErrorAtom.reportRead();
    return super._passwordError;
  }

  @override
  set _passwordError(String? value) {
    _$_passwordErrorAtom.reportWrite(value, super._passwordError, () {
      super._passwordError = value;
    });
  }

  late final _$_passwordConfirmationErrorAtom = Atom(
      name: '_SignUpControllerBase._passwordConfirmationError',
      context: context);

  @override
  String? get _passwordConfirmationError {
    _$_passwordConfirmationErrorAtom.reportRead();
    return super._passwordConfirmationError;
  }

  @override
  set _passwordConfirmationError(String? value) {
    _$_passwordConfirmationErrorAtom
        .reportWrite(value, super._passwordConfirmationError, () {
      super._passwordConfirmationError = value;
    });
  }

  late final _$_isLoadingAtom =
      Atom(name: '_SignUpControllerBase._isLoading', context: context);

  @override
  bool get _isLoading {
    _$_isLoadingAtom.reportRead();
    return super._isLoading;
  }

  @override
  set _isLoading(bool value) {
    _$_isLoadingAtom.reportWrite(value, super._isLoading, () {
      super._isLoading = value;
    });
  }

  late final _$checkboxAtom =
      Atom(name: '_SignUpControllerBase.checkbox', context: context);

  @override
  bool? get checkbox {
    _$checkboxAtom.reportRead();
    return super.checkbox;
  }

  @override
  set checkbox(bool? value) {
    _$checkboxAtom.reportWrite(value, super.checkbox, () {
      super.checkbox = value;
    });
  }

  late final _$_obscurePasswordAtom =
      Atom(name: '_SignUpControllerBase._obscurePassword', context: context);

  @override
  bool get _obscurePassword {
    _$_obscurePasswordAtom.reportRead();
    return super._obscurePassword;
  }

  @override
  set _obscurePassword(bool value) {
    _$_obscurePasswordAtom.reportWrite(value, super._obscurePassword, () {
      super._obscurePassword = value;
    });
  }

  late final _$_obscurePasswordConfirmationAtom = Atom(
      name: '_SignUpControllerBase._obscurePasswordConfirmation',
      context: context);

  @override
  bool get _obscurePasswordConfirmation {
    _$_obscurePasswordConfirmationAtom.reportRead();
    return super._obscurePasswordConfirmation;
  }

  @override
  set _obscurePasswordConfirmation(bool value) {
    _$_obscurePasswordConfirmationAtom
        .reportWrite(value, super._obscurePasswordConfirmation, () {
      super._obscurePasswordConfirmation = value;
    });
  }

  late final _$_SignUpControllerBaseActionController =
      ActionController(name: '_SignUpControllerBase', context: context);

  @override
  dynamic setCheckbox(bool? value) {
    final _$actionInfo = _$_SignUpControllerBaseActionController.startAction(
        name: '_SignUpControllerBase.setCheckbox');
    try {
      return super.setCheckbox(value);
    } finally {
      _$_SignUpControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _toggleIsLoading() {
    final _$actionInfo = _$_SignUpControllerBaseActionController.startAction(
        name: '_SignUpControllerBase._toggleIsLoading');
    try {
      return super._toggleIsLoading();
    } finally {
      _$_SignUpControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void toggleObscurePassword() {
    final _$actionInfo = _$_SignUpControllerBaseActionController.startAction(
        name: '_SignUpControllerBase.toggleObscurePassword');
    try {
      return super.toggleObscurePassword();
    } finally {
      _$_SignUpControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void toggleObscurePasswordConfirmation() {
    final _$actionInfo = _$_SignUpControllerBaseActionController.startAction(
        name: '_SignUpControllerBase.toggleObscurePasswordConfirmation');
    try {
      return super.toggleObscurePasswordConfirmation();
    } finally {
      _$_SignUpControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? validateName(String? value) {
    final _$actionInfo = _$_SignUpControllerBaseActionController.startAction(
        name: '_SignUpControllerBase.validateName');
    try {
      return super.validateName(value);
    } finally {
      _$_SignUpControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? validateEmail(String? value) {
    final _$actionInfo = _$_SignUpControllerBaseActionController.startAction(
        name: '_SignUpControllerBase.validateEmail');
    try {
      return super.validateEmail(value);
    } finally {
      _$_SignUpControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? validatePhone(String? value) {
    final _$actionInfo = _$_SignUpControllerBaseActionController.startAction(
        name: '_SignUpControllerBase.validatePhone');
    try {
      return super.validatePhone(value);
    } finally {
      _$_SignUpControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? validatePassword(String? value) {
    final _$actionInfo = _$_SignUpControllerBaseActionController.startAction(
        name: '_SignUpControllerBase.validatePassword');
    try {
      return super.validatePassword(value);
    } finally {
      _$_SignUpControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? validatePasswordConfirmation(String? value) {
    final _$actionInfo = _$_SignUpControllerBaseActionController.startAction(
        name: '_SignUpControllerBase.validatePasswordConfirmation');
    try {
      return super.validatePasswordConfirmation(value);
    } finally {
      _$_SignUpControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
checkbox: ${checkbox},
isFormValid: ${isFormValid}
    ''';
  }
}
