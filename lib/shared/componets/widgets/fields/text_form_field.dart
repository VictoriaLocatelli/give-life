import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:give_life/shared/componets/constants/spacing.dart';

class CustomTextFormField extends StatelessWidget {
  const CustomTextFormField({
    Key? key,
    this.label,
    this.onChanged,
    this.validator,
    this.obscureText = false,
    this.keyboardType = TextInputType.name,
    this.maxLength,
    this.inputFormatters,
    this.isRequired = true,
    this.initialValue,
    this.onTap,
    this.decoration,
    this.maxLines = 1,
  }) : super(key: key);

  final String? label;
  final void Function(String)? onChanged;
  final String? Function(String?)? validator;
  final bool obscureText;
  final TextInputType keyboardType;
  final int? maxLength;
  final List<TextInputFormatter>? inputFormatters;
  final bool isRequired;
  final String? initialValue;
  final Function()? onTap;
  final InputDecoration? decoration;
  final int? maxLines;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;

    return Padding(
      padding: const EdgeInsets.only(bottom: Spacing.small),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (label != null)
            Text.rich(TextSpan(
              text: label,
              style: TextStyle(color: Colors.black),
              children: [
                if (isRequired)
                  TextSpan(
                    text: ' *',
                    style:
                        textTheme.caption?.copyWith(color: colorScheme.error),
                  ),
              ],
            )),
          const SizedBox(height: Spacing.xxSmall),
          TextFormField(
            initialValue: initialValue,
            validator: validator,
            keyboardType: keyboardType,
            onChanged: onChanged,
            obscureText: obscureText,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            maxLength: maxLength,
            decoration: decoration,
            inputFormatters: inputFormatters,
            onTap: onTap,
            maxLines: maxLines,
          ),
        ],
      ),
    );
  }
}
