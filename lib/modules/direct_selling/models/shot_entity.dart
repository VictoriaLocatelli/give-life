import 'dart:convert';

import 'package:give_life/modules/trading_floor/models/client_entity.dart';
import 'package:intl/intl.dart';

class Shot {
  int? id;
  int? lotId;
  int? clientId;
  double? value;
  double? oldValue;
  String? date;
  String? time;
  String? comesFrom;
  String? createdAt;
  String? updatedAt;
  ClientEntity? client;
  String? condition;

  Shot(
      {this.id,
      this.lotId,
      this.clientId,
      this.value,
      this.oldValue,
      this.date,
      this.time,
      this.comesFrom,
      this.createdAt,
      this.updatedAt,
      this.client,
      required this.condition});

  factory Shot.fromJson(Map<String, dynamic> json) {
    Shot data = Shot(
      id: json['id'],
      lotId: json['lot_id'],
      clientId: json['client_id'],
      value: json['value'] != null && json['value'] is int
          ? json['value'].toDouble()
          : json['value'],
      oldValue: json['old_value'] != null && json['old_value'] is int
          ? json['old_value'].toDouble()
          : json['old_value'],
      date: json['date'],
      time: json['time'],
      comesFrom: json['comes_from'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
      client:
          json['client'] != null ? ClientEntity.fromJson(json['client']) : null,
      condition: json['condition'],
    );
    return data;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['lot_id'] = lotId;
    data['client_id'] = clientId;
    data['value'] = value;
    data['old_value'] = oldValue;
    data['date'] = date;
    data['time'] = time;
    data['comes_from'] = comesFrom;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    if (client != null) {
      data['client'] = client?.toJson();
    }
    return data;
  }

  String getMoney(dynamic value) => _getFormatMoney(value);

  String _getFormatMoney(dynamic value) =>
      NumberFormat.currency(symbol: 'R\$', locale: 'pt_BR').format(value);
}
