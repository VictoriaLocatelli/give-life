import 'package:give_life/modules/profile/entity/client_profile_entity.dart';

abstract class LoadClientProfile {
  Future<ClientProfileEntity> load();
}
