import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/home/controller/details_controller.dart';
import 'package:give_life/modules/home/factories/load_auctions.dart';
import 'package:give_life/modules/home/model/contents_entity.dart';
import 'package:give_life/modules/home/view/widgets/video_page.dart';
import 'package:give_life/shared/app_config.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';

class GalleryView extends StatelessWidget {
  GalleryView({super.key, required this.id});
  final String id;
  Function(int id)? action;
  List<ContentsEntity>? contents;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mais Informações"),
        bottom: PreferredSize(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: Text(
                'Confira alguns itens que estarão disponíveis neste leilão.',
                style: TextStyle(fontSize: 15),
              ),
            ),
          ),
          preferredSize: Size.fromHeight(50),
        ),
      ),
      body: FutureBuilder(
          future: Modular.get<LoadAuctions>().details(int.parse(id)),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.none &&
                snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            } else {
              if (snapshot.hasData) {
                List<ContentsEntity?>? contents = snapshot.data?.contents;

                return contents != null
                    ? Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8, vertical: 8),
                        child: GridView.count(
                          shrinkWrap: true,
                          crossAxisCount: 2,
                          crossAxisSpacing: 5,
                          mainAxisSpacing: 5,
                          childAspectRatio: .8,
                          children: [
                            ...contents
                                .map((e) => ContentGallery(
                                      content: e!,
                                    ))
                                .toList()
                          ],
                        ),
                      )
                    : Container();
              } else {
                return Container();
              }
            }
          }),
    );
  }
}

class ContentGallery extends StatelessWidget {
  const ContentGallery({super.key, required this.content});
  final ContentsEntity content;

  @override
  Widget build(BuildContext context) {
    String url;
    return GestureDetector(
        child: Card(
            child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.black,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  flex: 6,
                  child: Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              fit: BoxFit.contain,
                              image: content.filename != null
                                  ? NetworkImage(
                                      AppConfig.getInstance().apiStorageUrl +
                                          content.filename!)
                                  : content.type == 2
                                      ? AssetImage(AppImages.paly.path)
                                      : AssetImage(
                                              AppImages.noProfilePicture.path)
                                          as ImageProvider<Object>))),
                ),
                Expanded(
                    flex: 4,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text("${content.title ?? ''}",
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                              )),
                          Text(
                            "${content.description ?? ''}",
                            style: TextStyle(fontSize: 10),
                            maxLines: 2,
                          ),
                        ],
                      ),
                    ))
              ],
            ),
          ),
        )),
        onTap: () => {
              if (content.type == 2)
                {
                  url = content.url!,
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => VideoPage(url: url),
                  ))
                }
            });
  }
}
