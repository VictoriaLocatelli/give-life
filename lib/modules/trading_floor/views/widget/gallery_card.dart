//import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/trading_floor/models/lot_entity.dart';
import 'package:give_life/modules/trading_floor/models/lot_images_entity.dart';
import 'package:give_life/shared/app_config.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:give_life/shared/componets/theme/theme.dart';
import 'package:intl/intl.dart';

class GalleryCard extends StatelessWidget {
  GalleryCard({super.key, required this.image, this.action});
  final LotImagesEntity image;
  Function(int id)? action;

  @override
  Widget build(BuildContext context) {
    ImageProvider<Object> img = image.fileUrl != null
        ? NetworkImage(image.fileUrl!)
        : AssetImage(AppImages.noProfilePicture.path) as ImageProvider<Object>;
    return GestureDetector(
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              flex: 8,
              child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(fit: BoxFit.contain, image: img)),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                margin: const EdgeInsets.all(0),
                padding: const EdgeInsets.all(0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      '${image.filename}',
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                        color: AppColors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      onTap: () => action!(image.id!),
    );
  }
}
