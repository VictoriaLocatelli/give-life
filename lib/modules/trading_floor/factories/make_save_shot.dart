import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/trading_floor/factories/make_load_lots.dart';
import 'package:give_life/modules/trading_floor/factories/save_shot.dart';
import 'package:give_life/modules/trading_floor/usescases/remote_save_shot.dart';
import 'package:give_life/shared/data/factories/api_url_factory.dart';
import 'package:give_life/shared/data/http/http_client.dart';

SaveShot makeRemoteSaveshot() => RemoteSaveShot(
      url: makeApiUrl('/shots'),
      httpClient: Modular.get<HttpClient>(),
    );
