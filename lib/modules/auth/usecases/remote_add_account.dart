import 'package:flutter/foundation.dart';
import 'package:give_life/modules/auth/models/signup/add_account.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';

class RemoteAddAccount implements AddAccount {
  const RemoteAddAccount({
    required this.httpClient,
    required this.url,
  });

  final String url;
  final HttpClient httpClient;

  @override
  Future<void> add(InputAddAccount input) async {
    try {
      final body = InputRemoteAddAccount.fromDomain(input).toMap();

      await httpClient.request(url: url, method: 'post', body: body);
    } on HttpError catch (error) {
      switch (error) {
        case HttpError.unauthorized:
          throw DomainError.invalidCredentials;
        case HttpError.unprocessableEntity:
          throw ("Este e-mail já possui cadastro.");
        default:
          throw DomainError.unexpected;
      }
    }
  }
}

@immutable
class InputRemoteAddAccount {
  const InputRemoteAddAccount({
    required this.name,
    required this.email,
    required this.phone,
    required this.password,
  });

  final String name;
  final String email;
  final String phone;
  final String password;

  factory InputRemoteAddAccount.fromDomain(InputAddAccount input) =>
      InputRemoteAddAccount(
        name: input.name,
        email: input.email,
        phone: input.phone,
        password: input.password,
      );

  Map<String, dynamic> toMap() => {
        'name': name,
        'email': email,
        'phone': phone,
        'password': password,
      };
}
