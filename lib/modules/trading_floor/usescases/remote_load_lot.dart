import 'package:give_life/modules/trading_floor/factories/make_load_lots.dart';
import 'package:give_life/modules/trading_floor/models/lot_entity.dart';

import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';

class RemoteLoadLot implements LoadLots {
  const RemoteLoadLot({
    required this.url,
    required this.httpClient,
  });

  final String url;
  final HttpClient httpClient;

  @override
  Future<List<LotEntity>?> loadAll() async {
    try {
      List resul = (await httpClient.request(url: url, method: 'get'))['data'];

      final body = resul.map((lot) => LotEntity.fromJson(lot)).toList();
      return body;
    } on HttpError {
      throw DomainError.unexpected;
    }
  }

  @override
  Future<LotEntity?> load(int id) async {
    try {
      Map<String, dynamic> resul =
          (await httpClient.request(url: url + '/$id', method: 'get'))['data'];
      LotEntity? body = LotEntity.fromJson(resul);
      return body;
    } on HttpError {
      throw DomainError.unexpected;
    }
  }
}
