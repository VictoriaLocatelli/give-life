import 'package:flutter/foundation.dart';

import 'package:equatable/equatable.dart';
import 'package:give_life/shared/core/entity/client_account_entitie.dart';

@immutable
abstract class Authentication {
  const Authentication();

  Future<ClientAccountEntity> auth(InputAuthentication params);
}

@immutable
class InputAuthentication extends Equatable {
  const InputAuthentication({
    required this.email,
    required this.secret,
  });

  final String email;
  final String secret;

  @override
  List<Object?> get props => [email, secret];
}
