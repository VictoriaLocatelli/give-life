import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/home/model/entity_%20auctions.dart';
import 'package:give_life/shared/app_config.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:give_life/shared/componets/theme/app_colors.dart';

class OnlineAuctionsWidget extends StatelessWidget {
  const OnlineAuctionsWidget({super.key, required this.auction});
  final AuctionsEntity auction;

  @override
  Widget build(BuildContext context) {
    ImageProvider<Object> img = auction.image != null
        ? NetworkImage(AppConfig.getInstance().apiStorageUrl + auction.image!)
        : AssetImage(AppImages.logo.path) as ImageProvider<Object>;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
      child: GestureDetector(
        onTap: () => Modular.to.pushNamed('/home/live/${auction.id}'),
        child: Container(
          width: 210,
          height: 230,
          child: LayoutBuilder(builder: (context, constraints) {
            return Stack(clipBehavior: Clip.none, children: [
              Positioned(
                width: constraints.maxWidth,
                top: 0,
                child: Card(
                  child: Container(
                    width: 215,
                    height: 215,
                    decoration: BoxDecoration(
                      image: DecorationImage(fit: BoxFit.cover, image: img),
                    ),
                  ),
                ),
              ),
              Positioned(
                  width: constraints.maxWidth,
                  bottom: 0,
                  child: Transform.translate(
                    offset: Offset(0, 3),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 150,
                          height: 35,
                          child: ElevatedButton(
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.all(AppColors.red),
                              ),
                              onPressed: () => Modular.to
                                  .pushNamed('/home/live/${auction.id}'),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Image.asset(AppImages.live.path),
                                  Text('  AO VIVO   '),
                                  Image.asset(
                                    AppImages.arrow.path,
                                    width: 13,
                                  ),
                                ],
                              )),
                        ),
                      ],
                    ),
                  )),
            ]);
          }),
        ),
      ),
    );
  }
}
