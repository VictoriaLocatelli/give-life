class ResponsibleModel {
    String? name;
    String? nif;
    String? email;
    String? stateregistration;
    String? zipcode;
    String? address;
    String? number;
    String? complement;
    String? district;
    String? phone;
    String? birthday;
    String? profession;
    int? civilstatus;
    int? cityid;

    ResponsibleModel({this.name, this.nif, this.email, this.stateregistration, this.zipcode, this.address, this.number, this.complement, this.district, this.phone, this.birthday, this.profession, this.civilstatus, this.cityid}); 

    ResponsibleModel.fromJson(Map<String, dynamic> json) {
        name = json['name'];
        nif = json['nif'];
        email = json['email'];
        stateregistration = json['state_registration'];
        zipcode = json['zip_code'];
        address = json['address'];
        number = json['number'];
        complement = json['complement'];
        district = json['district'];
        phone = json['phone'];
        birthday = json['birthday'];
        profession = json['profession'];
        civilstatus = json['civil_status'];
        cityid = json['city_id'];
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = Map<String, dynamic>();
        data['name'] = name;
        data['nif'] = nif;
        data['email'] = email;
        data['state_registration'] = stateregistration;
        data['zip_code'] = zipcode;
        data['address'] = address;
        data['number'] = number;
        data['complement'] = complement;
        data['district'] = district;
        data['phone'] = phone;
        data['birthday'] = birthday;
        data['profession'] = profession;
        data['civil_status'] = civilstatus;
        data['city_id'] = cityid;
        return data;
    }
}
