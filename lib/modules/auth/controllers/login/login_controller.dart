import 'dart:convert';

import 'package:give_life/modules/auth/authentication.dart';
import 'package:give_life/shared/app_config.dart';
import 'package:give_life/shared/componets/utils/show_snack_bar.dart';
import 'package:give_life/shared/data/contracts/local/save_client_current_account.dart';
import 'package:give_life/shared/data/validation/validation_builder.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'login_controller.g.dart';

class LoginController = _LoginControllerBase with _$LoginController;

abstract class _LoginControllerBase with Store {
  _LoginControllerBase({
    required Authentication authentication,
    required SaveClientCurrentAccount saveCurrentAccount,
    void Function()? callback,
  })  : _authentication = authentication,
        _saveCurrentAccount = saveCurrentAccount,
        _callback = callback;

  final Authentication _authentication;
  final SaveClientCurrentAccount _saveCurrentAccount;
  final void Function()? _callback;

  @observable
  String? _email;
  String? get email => _email;

  @observable
  String? _password;
  String? get password => _password;

  @observable
  String? _emailError;
  @observable
  String? _passwordError;
  @observable
  bool _isObscurePassword = true;
  @observable
  bool _isLoading = false;

  String? get emailError => _emailError;
  String? get passwordError => _passwordError;
  bool get isLoading => _isLoading;
  bool get isObscurePassword => _isObscurePassword;

  @action
  void _setIsLoading(bool value) => _isLoading = value;
  @action
  void toggleObscurePassword() => _isObscurePassword = !_isObscurePassword;

  @action
  String? validateEmail(String? value) {
    _email = value;
    _emailError = ValidationBuilder(value).required().email().build();

    return _emailError;
  }

  @action
  String? validatePassword(String? value) {
    _password = value;
    _passwordError = ValidationBuilder(value).required().min(8).build();

    return _passwordError;
  }

  @computed
  bool get isFormValid =>
      _email != null &&
      _password != null &&
      _emailError == null &&
      _passwordError == null;

  @action
  void _clean() {
    _email = null;
    _password = null;
  }

  Future<void> auth() async {
    _setIsLoading(true);

    try {
      final input = InputAuthentication(email: _email!, secret: _password!);

      final account = await _authentication.auth(input);

      if (account.getPhoto() != " ") {
        account.setPhoto(
            AppConfig.getInstance().apiStorageUrl + account.getPhoto());
      }
      await _saveCurrentAccount.save(account);

      _clean();
      if (_callback != null) {
        _callback!.call();
      } else {
        _goToHome();
      }
    } catch (error) {
      print(error);
      ShowSnackBar.error(error.toString()).show();
      _setIsLoading(false);
    }
  }

  void _goToHome() => Modular.to.navigate('/home/');

  void goToSignUp() => Modular.to.pushNamed('signup');

  void goToForget() => Modular.to.pushNamed('forgot');
}
