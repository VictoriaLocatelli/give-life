import 'package:meta/meta.dart';

abstract class RecoverAccount {
  Future<String?> recover(InputRecoverAccount input);
}

@immutable
class InputRecoverAccount {
  const InputRecoverAccount(this.email);

  final String email;
}
