import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';
import 'package:brasil_fields/brasil_fields.dart' as brasil_fields;

class Phone1InputWidget extends StatelessWidget {
  const Phone1InputWidget();

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();

    return Observer(builder: (_) {
      return CustomTextFormField(
        label: 'WhatsApp/Celular',
        initialValue: controller.phone1,
        onChanged: controller.phone1Validator,
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(
          errorText: controller.phone1Error,
        ),
        inputFormatters: [
          FilteringTextInputFormatter.digitsOnly,
          brasil_fields.TelefoneInputFormatter(),
        ],
      );
    });
  }
}
