import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/direct_selling/factories/make_load_lots.dart';
import 'package:give_life/modules/direct_selling/usescases/remote_load_lot.dart';
import 'package:give_life/shared/data/factories/api_url_factory.dart';
import 'package:give_life/shared/data/http/http_client.dart';

LoadLots makeRemoteLoadLots() => RemoteLoadLot(
      url: makeApiUrl('/lots'),
      httpClient: Modular.get<HttpClient>(),
    );
