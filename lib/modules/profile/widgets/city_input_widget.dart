import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/shared/componets/widgets/fields/dropdown_dialog_search_select_field.dart';
import 'package:give_life/shared/core/entity/city_entity.dart';

class CityInputWidget extends StatelessWidget {
  const CityInputWidget();

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();

    return DropdownDialogSearchSelectField<CityEntity>(
      label: 'Cidade',
      validator: controller.cityValidator,
      items: controller.getCities(),
      selectedItem: controller.city,
    );
  }
}
