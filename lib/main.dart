import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/shared/app_config.dart';
import 'package:give_life/shared/app_module.dart';
import 'package:give_life/shared/app_widget.dart';
import 'package:give_life/shared/core/env.dart';

void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    AppConfig.getInstance(config: Environment.dev);
    return ModularApp(module: AppModule(), child: const AppWidget());
  }
}
