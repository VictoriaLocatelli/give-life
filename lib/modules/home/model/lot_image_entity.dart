class LotImageEntity {
  int? id;
  int? lotid;
  String? filename;
  int? type;
  DateTime? createdat;
  DateTime? updatedat;
  String? fileurl;

  LotImageEntity(
      {this.id,
      this.lotid,
      this.filename,
      this.type,
      this.createdat,
      this.updatedat,
      this.fileurl});

  LotImageEntity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    lotid = json['lot_id'];
    filename = json['filename'];
    type = json['type'];
    createdat = json['created_at'] != null
        ? DateTime.parse(json['created_at'])
        : json['created_at'];
    updatedat = json['updated_at'] != null
        ? DateTime.parse(json['updated_at'])
        : json['updated_at'];
    fileurl = json['file_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['lot_id'] = lotid;
    data['filename'] = filename;
    data['type'] = type;
    data['created_at'] = createdat;
    data['updated_at'] = updatedat;
    data['file_url'] = fileurl;
    return data;
  }
}
