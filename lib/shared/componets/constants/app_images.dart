const _imgsRoot = 'assets/img';
const _svgsRoot = 'assets/svg';
const _iconsRoot = 'assets/img/icons';

enum AppImages {
  logoSvg('$_svgsRoot/logo-svg.svg'),
  calendarSvg('$_svgsRoot/calendar.svg'),
  calendarEndSvg('$_svgsRoot/calendar-end.svg'),
  warning('$_svgsRoot/warnig_square.svg'),
  validate('$_svgsRoot/validate.svg'),
  validation('$_svgsRoot/validation.svg'),
  next('$_svgsRoot/nextpointer.svg'),
  logo('$_imgsRoot/logo.png'),
  live('$_imgsRoot/live.png'),
  arrow('$_imgsRoot/arrow.png'),
  hammer('$_imgsRoot/hammer.png'),
  encerrado('$_imgsRoot/encerrado-removebg-preview.png'),
  logoIcon('$_imgsRoot/logo_icon.png'),
  noProfilePicture('$_imgsRoot/no_profile_picture.png'),
  recPng('$_imgsRoot/Rectangle277.png'),
  support("$_imgsRoot/support.png"),
  splash('$_imgsRoot/splash-background.jpg'),
  edit('$_imgsRoot/edit.png'),
  leave('$_imgsRoot/leave.png'),
  profile('$_imgsRoot/profile.png'),
  quotation('$_imgsRoot/cot.png'),
  home('$_imgsRoot/home.png'),
  barra('$_imgsRoot/barra.png'),
  localization('$_imgsRoot/localization.png'),
  calendar('$_imgsRoot/calendar.png'),
  whatsappLogo('$_imgsRoot/whatsapp_logo.png'),
  calendarStart('$_imgsRoot/calendarStart.png'),
  imageVideo('$_imgsRoot/image-video.png'),
  paly('$_imgsRoot/play.png'),
  info('$_imgsRoot/info.png'),
  catalog('$_imgsRoot/catalog.png'),
  regulamention('$_imgsRoot/regulamention.png'),
  phone('$_imgsRoot/phone.png'),
  royalSymbol('$_imgsRoot/royalSymbol.png'),

  // Icons
  whatsAppIcon('$_iconsRoot/whatsapp_icon.png'),

  //SVG
  recSvg('$_svgsRoot/Rectangle 278.svg');

  const AppImages(this.path);

  final String path;
}
