import 'package:give_life/modules/profile/entity/client_profile_entity.dart';
import 'package:give_life/modules/profile/contracts/load_client_profile.dart';
import 'package:mobx/mobx.dart';

part 'initial_client_profile.g.dart';

class InitialClientProfile = InitialClientProfileBase
    with _$InitialClientProfile;

abstract class InitialClientProfileBase with Store {
  InitialClientProfileBase({required this.loadClient});

  final LoadClientProfile loadClient;

  @observable
  bool _isLoading = false;

  @computed
  bool get isLoading => _isLoading;

  set isLoading(bool value) => _isLoading = value;

  ClientProfileEntity? account;

  Future<void> loadData() async {
    isLoading = true;
    try {
      account = await loadClient.load();

      isLoading = false;
    } catch (e) {
      isLoading = false;
      print(e);
    }
  }
}
