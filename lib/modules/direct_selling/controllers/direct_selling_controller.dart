import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/direct_selling/factories/make_load_lots.dart';
import 'package:give_life/modules/direct_selling/models/lot_entity.dart';

import 'package:mobx/mobx.dart';

part 'direct_selling_controller.g.dart';

class DirectSaleController = DirectSaleControllerBase
    with _$DirectSaleController;

abstract class DirectSaleControllerBase with Store {
  DirectSaleControllerBase({
    required LoadLots loadLots,
  }) : _loadLots = loadLots;

  final LoadLots _loadLots;
  List<LotEntity>? lotAll = [];
  List<LotEntity>? lotAllEnable = [];
  List<LotEntity>? lotAllOthers = [];

  String email = '';
  String phone = '';

  @observable
  bool _isLoading = false;

  @computed
  bool get isLoading => _isLoading;

  set isLoading(bool value) => _isLoading = value;

  Future<void> loadData() async {
    try {
      isLoading = true;
      lotAll = (await _loadLots.loadAll())
          ?.where((LotEntity lot) => lot.tradingFloor!.modality == 'V')
          .toList();
      if (lotAll != null) {
        for (int i = 0; i < lotAll!.length; i++) {
          if (lotAll![i].status == 2) {
            lotAllEnable!.add(lotAll![i]);
          } else {
            lotAllOthers!.add(lotAll![i]);
          }
        }
      }
      isLoading = false;
    } catch (e) {
      print(e);
      isLoading = false;
    }
  }

  toDatails(int id) {
    Modular.to.pushNamed('/direct/details/$id');
  }

  String phoneFormat(String value) {
    final pattern = RegExp(r'(\d{2})(\d{5})(\d+)');

    return value.replaceAllMapped(
        pattern, (Match m) => '(${m[1]}) ${m[2]}-${m[3]}');
  }
}
