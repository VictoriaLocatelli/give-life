import 'package:give_life/modules/profile/models/account_profile_model.dart';
import 'package:give_life/modules/profile/models/document_model.dart';
import 'package:give_life/modules/profile/models/property_model.dart';
import 'package:give_life/modules/profile/models/purchase_model.dart';
import 'package:give_life/modules/profile/models/reference_model.dart';
import 'package:give_life/modules/profile/models/responsible_model.dart';

class ProfileModel {
  String? email;
  String? phone;
  int? personid;
  String? name;
  String? nickname;
  String? nif;
  String? zipcode;
  String? address;
  String? number;
  String? district;
  int? cityid;
  String? birthday;
  PropertyModel? property;
  AccountModel? account;
  ResponsibleModel? responsible;
  List<ReferenceModel?>? references;
  List<PurchaseModel?>? purchases;
  List<int?>? interests;
  List<DocumentModel?>? documents;

  ProfileModel(
      {this.email,
      this.phone,
      this.personid,
      this.name,
      this.nickname,
      this.nif,
      this.zipcode,
      this.address,
      this.number,
      this.district,
      this.cityid,
      this.birthday,
      this.property,
      this.account,
      this.responsible,
      this.references,
      this.purchases,
      this.interests,
      this.documents});

  ProfileModel.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    phone = json['phone'];
    personid = json['person_id'];
    name = json['name'];
    nickname = json['nickname'];
    nif = json['nif'];
    zipcode = json['zip_code'];
    address = json['address'];
    number = json['number'];
    district = json['district'];
    cityid = json['city_id'];
    birthday = json['birthday'];
    property = json['property'] != null
        ? PropertyModel?.fromJson(json['property'])
        : null;
    account = json['account'] != null
        ? AccountModel?.fromJson(json['account'])
        : null;
    responsible = json['responsible'] != null
        ? ResponsibleModel?.fromJson(json['responsible'])
        : null;
    if (json['references'] != null) {
      references = <ReferenceModel>[];
      json['references'].forEach((v) {
        references!.add(ReferenceModel.fromJson(v));
      });
    }
    if (json['purchases'] != null) {
      purchases = <PurchaseModel>[];
      json['purchases'].forEach((v) {
        purchases!.add(PurchaseModel.fromJson(v));
      });
    }
    if (json['interests'] != null) {
      interests = [];
      json['interests'].forEach((v) {
        interests!.add(v.toInt());
      });
    }
    if (json['documents'] != null) {
      documents = <DocumentModel>[];
      json['documents'].forEach((v) {
        documents!.add(DocumentModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['email'] = email;
    data['phone'] = phone;
    data['person_id'] = personid;
    data['name'] = name;
    data['nickname'] = nickname;
    data['nif'] = nif;
    data['zip_code'] = zipcode;
    data['address'] = address;
    data['number'] = number;
    data['district'] = district;
    data['city_id'] = cityid;
    data['birthday'] = birthday;
    data['property'] = property!.toJson();
    data['account'] = account!.toJson();
    data['responsible'] = responsible!.toJson();
    data['references'] = references != null
        ? references!.map((v) => v?.toJson()).toList()
        : null;
    data['purchases'] =
        purchases != null ? purchases!.map((v) => v?.toJson()).toList() : null;
    data['interests'] = interests != null ? interests : null;
    data['documents'] =
        documents != null ? documents!.map((v) => v?.toJson()).toList() : null;
    return data;
  }
}
