class ReferenceModel {
    String? name;
    String? telephoneone;
    String? telephonetwo;

    ReferenceModel({this.name, this.telephoneone, this.telephonetwo}); 

    ReferenceModel.fromJson(Map<String, dynamic> json) {
        name = json['name'];
        telephoneone = json['telephone_one'];
        telephonetwo = json['telephone_two'];
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = Map<String, dynamic>();
        data['name'] = name;
        data['telephone_one'] = telephoneone;
        data['telephone_two'] = telephonetwo;
        return data;
    }
}