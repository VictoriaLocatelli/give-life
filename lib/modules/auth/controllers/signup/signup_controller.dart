import 'package:give_life/modules/auth/models/signup/add_account.dart';
import 'package:give_life/shared/componets/utils/show_snack_bar.dart';
import 'package:give_life/shared/data/validation/validation_builder.dart';
import 'package:give_life/shared/componets/extensions/strings.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'signup_controller.g.dart';

/// Se [callback] for diferente de null será executado após a criação bem-sucedido da conta.
class SignUpController = _SignUpControllerBase with _$SignUpController;

abstract class _SignUpControllerBase with Store {
  _SignUpControllerBase({
    required AddAccount addAccount,
    void Function()? callback,
  })  : _addAccount = addAccount,
        _callback = callback;

  final AddAccount _addAccount;
  final void Function()? _callback;

  @observable
  String? _name;
  @observable
  String? _phone;
  @observable
  String? _email;
  @observable
  String? _password;
  @observable
  String? _passwordConfirmation;

  @observable
  String? _nameError;
  @observable
  String? _phoneError;
  @observable
  String? _emailError;
  @observable
  String? _passwordError;
  @observable
  String? _passwordConfirmationError;

  @observable
  bool _isLoading = false;

  @observable
  bool? checkbox = false;
  @action
  setCheckbox(bool? value) => checkbox = value;
  @observable
  bool _obscurePassword = true;
  @observable
  bool _obscurePasswordConfirmation = true;

  String? get name => _name;
  String? get phone => _phone;
  String? get email => _email;
  String? get password => _password;
  String? get passwordConfirmation => _passwordConfirmation;

  String? get nameError => _nameError;
  String? get phoneError => _phoneError;
  String? get emailError => _emailError;
  String? get passwordError => _passwordError;
  String? get passwordConfirmationError => _passwordConfirmationError;

  bool get isLoading => _isLoading;
  bool get obscurePassword => _obscurePassword;
  bool get obscurePasswordConfirmation => _obscurePasswordConfirmation;

  @action
  void _toggleIsLoading() => _isLoading = !_isLoading;
  @action
  void toggleObscurePassword() => _obscurePassword = !_obscurePassword;
  @action
  void toggleObscurePasswordConfirmation() =>
      _obscurePasswordConfirmation = !_obscurePasswordConfirmation;

  @action
  String? validateName(String? value) {
    _name = value;
    _nameError = ValidationBuilder(value).required().min(3).build();

    return _nameError;
  }

  @action
  String? validateEmail(String? value) {
    _email = value;
    _emailError = ValidationBuilder(value).required().email().build();

    return _emailError;
  }

  @action
  String? validatePhone(String? value) {
    value = value?.digitsOnly();
    _phone = value;
    _phoneError = ValidationBuilder(value).required().min(10).max(11).build();

    return _phoneError;
  }

  @action
  String? validatePassword(String? value) {
    _password = value;
    _passwordError = ValidationBuilder(value).required().min(8).max(30).build();

    return _passwordError;
  }

  @action
  String? validatePasswordConfirmation(String? value) {
    _passwordConfirmation = value;
    _passwordConfirmationError = ValidationBuilder(value)
        .required()
        .min(8)
        .max(30)
        .compare(_password)
        .build();

    return _passwordConfirmationError;
  }

  void _clean() {
    _name = null;
    _phone = null;
    _email = null;
    _password = null;
    _passwordConfirmation = null;
    _nameError = null;
    _phoneError = null;
    _emailError = null;
    _passwordError = null;
    _passwordConfirmationError = null;
  }

  @computed
  bool get isFormValid =>
      _name != null &&
      _phone != null &&
      _email != null &&
      _password != null &&
      _passwordConfirmation != null &&
      _nameError == null &&
      _phoneError == null &&
      _emailError == null &&
      _passwordError == null &&
      _passwordConfirmationError == null &&
      checkbox == true;

  Future<void> signup() async {
    _toggleIsLoading();

    try {
      final input = InputAddAccount(
        email: email!,
        name: name!,
        password: password!,
        phone: phone!,
      );

      await _addAccount.add(input);
      _clean();
      ShowSnackBar.success('Conta criada com sucesso').show();

      if (_callback != null) {
        _callback!.call();
      } else {
        goToLoginPage();
      }
    } catch (error) {
      ShowSnackBar.error(error.toString()).show();
    }

    _toggleIsLoading();
  }

  void goToLoginPage() => Modular.to.pushReplacementNamed('/auth/');
}
