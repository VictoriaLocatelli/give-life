import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/auth/router/auth_module.dart';
import 'package:give_life/modules/direct_selling/router/direct_selling_module.dart';
import 'package:give_life/modules/help/router/help_module.dart';
import 'package:give_life/modules/profile/router/profile_module.dart';
import 'package:give_life/modules/trading_floor/router/trading_floor_module.dart';
import 'package:give_life/shared/data/cache/Local_storage_adapter.dart';
import 'package:give_life/shared/data/contracts/cache/cache_storage.dart';
import 'package:give_life/shared/data/factories/load_cities_factory.dart';
import 'package:give_life/shared/data/factories/http_client_factory.dart';
import 'package:give_life/shared/data/usecases/local/local_delete_current_account.dart';
import 'package:give_life/shared/data/usecases/local/local_load_current_account.dart';
import 'package:give_life/shared/data/usecases/local/local_save_current_account.dart';
import 'package:give_life/shared/guards/auth_guard.dart';
import 'package:localstorage/localstorage.dart';

import '../modules/home/router/home_router.dart';

class AppModule extends Module {
  @override
  List<Bind> get binds => [
        Bind.singleton((_) => makeHttpAdapter()),
        Bind.singleton((i) => makeRemoteLoadCities()),
        Bind.singleton((i) => LocalStorageAdapter(LocalStorage('gralhaazul'))),
        Bind.lazySingleton((i) => LocalLoadCurrentAccount(
            secureStorage: Modular.get<CacheStorage>())),
        Bind.lazySingleton((i) => LocalSaveCurrentAccount(
            secureStorage: Modular.get<CacheStorage>())),
        Bind.lazySingleton((i) => LocalRemoveCurrentAccount(
            cacheStorage: Modular.get<CacheStorage>())),
      ];

  @override
  List<Module> get imports => [
        // UsecasesModule(),
        // ServicesModule(),
      ];

  @override
  List<ModularRoute> get routes => [
        ModuleRoute('/auth', module: AuthModule()),
        ModuleRoute('/home', guards: [AuthGuard()], module: HomeRouter()),
        ModuleRoute('/profile', module: ProfileModule()),
        ModuleRoute('/help', module: HelpModule()),
        ModuleRoute('/trading', module: TradingFLoor()),
        ModuleRoute('/direct', module: DirectSale()), //entity
      ];
}
