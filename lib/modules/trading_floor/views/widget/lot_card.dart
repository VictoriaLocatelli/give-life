import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:give_life/modules/trading_floor/models/lot_entity.dart';
import 'package:give_life/modules/trading_floor/models/lot_images_entity.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:give_life/shared/componets/theme/theme.dart';
import 'package:intl/intl.dart';

String getFormatMoney(dynamic value) =>
    NumberFormat.currency(symbol: 'R\$', locale: 'pt_BR').format(value);

String getFormatDate(String value) =>
    DateFormat('dd/MM/yyyy').format(DateTime.parse(value));

String getFormatHour(String value) =>
    DateFormat('HH:mm').format(DateTime.parse('2000-01-01 ' + value));

class LotCard extends StatelessWidget {
  LotCard({super.key, required this.lot, this.action});
  final LotEntity lot;
  Function(int id)? action;

  @override
  Widget build(BuildContext context) {
    ImageProvider<Object> img = lot.images?[0].fileUrl != null
        ? NetworkImage(lot.images![0].fileUrl!)
        : AssetImage(AppImages.noProfilePicture.path) as ImageProvider<Object>;

    if (lot.status == 2) {
      return GestureDetector(
        child: Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                flex: 4,
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(fit: BoxFit.contain, image: img)),
                ),
              ),
              Expanded(
                flex: 4,
                child: Container(
                  margin: const EdgeInsets.all(0),
                  padding: const EdgeInsets.all(0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      AutoSizeText(
                        '${lot.name}',
                        maxLines: 2,
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                          color: AppColors.black,
                          fontWeight: FontWeight.bold,
                        ),
                        maxFontSize: 16,
                        minFontSize: 10,
                      ),
                      lot.description != null
                          ? AutoSizeText(
                              '${lot.description}',
                              maxLines: 1,
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                color: AppColors.black,
                              ),
                              maxFontSize: 12,
                              minFontSize: 10,
                            )
                          : SizedBox(
                              height: 2,
                            ),
                      SizedBox(
                        height: 2,
                      ),
                      Text(
                        lot.lastShot?.value != null
                            ? 'Lance atual: '
                                '${getFormatMoney(lot.lastShot!.value)}'
                            : 'Lance atual: '
                                '${getFormatMoney(lot.initValue)}',
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                            color: AppColors.black,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Text(
                        '${getFormatDate(lot.dateInit!)} as ${getFormatHour(lot.hourInit!)}',
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                            color: AppColors.black, fontSize: 10),
                      ),
                      Text(
                        '${getFormatDate(lot.dateEnd!)} as ${getFormatHour(lot.hourEnd!)}',
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                            color: AppColors.black, fontSize: 10),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      width: 200,
                      child: ElevatedButton(
                          onPressed: () => action!(lot.id!),
                          child: Text("VER MAIS")),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        onTap: () => action!(lot.id!),
      );
    } else {
      return GestureDetector(
        child: Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                  flex: 4,
                  child: Stack(children: [
                    Container(
                      decoration: BoxDecoration(
                          image:
                              DecorationImage(fit: BoxFit.contain, image: img)),
                    ),
                    Container(
                        decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                              'assets/img/encerrado-removebg-preview.png'),
                          fit: BoxFit.fill),
                    ))
                  ])),
              Expanded(
                flex: 5,
                child: Container(
                  margin: const EdgeInsets.all(0),
                  padding: const EdgeInsets.all(0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 35,
                        child: Container(
                          margin: const EdgeInsets.all(3),
                          padding: const EdgeInsets.all(3),
                          // decoration: BoxDecoration(
                          //   borderRadius: BorderRadius.all(
                          //     Radius.circular(5.0),
                          //   ),
                          //   boxShadow: [
                          //     BoxShadow(
                          //       color: AppColors.black,
                          //       offset: Offset(0.0, 2.0),
                          //       blurRadius: 7.0,
                          //     ),
                          //   ],
                          //   color: AppColors.secondaryWhite,
                          // ),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              lot.status == 4
                                  ? Text(
                                      'Vendido',
                                      textAlign: TextAlign.left,
                                      style: const TextStyle(
                                        color: AppColors.red,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  : Text(
                                      'Encerrado',
                                      textAlign: TextAlign.left,
                                      style: const TextStyle(
                                        color: AppColors.red,
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                            ],
                          ),
                        ),
                      ),
                      AutoSizeText(
                        '${lot.name}',
                        maxLines: 2,
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                          color: AppColors.black,
                          fontWeight: FontWeight.bold,
                        ),
                        maxFontSize: 16,
                        minFontSize: 10,
                      ),
                      lot.description != null
                          ? AutoSizeText(
                              '${lot.description}',
                              maxLines: 2,
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                color: AppColors.black,
                              ),
                              maxFontSize: 12,
                              minFontSize: 10,
                            )
                          : SizedBox(
                              height: 2,
                            ),
                      Text(
                        lot.lastShot?.value != null
                            ? '${getFormatMoney(lot.lastShot!.value)}'
                            : '${getFormatMoney(lot.initValue)}',
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                            color: AppColors.black, fontSize: 10),
                      ),
                      Text(
                        '${getFormatDate(lot.dateInit!)} as ${getFormatHour(lot.hourInit!)}',
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                            color: AppColors.black, fontSize: 10),
                      ),
                      Text(
                        '${getFormatDate(lot.dateEnd!)} as ${getFormatHour(lot.hourEnd!)}',
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                            color: AppColors.black, fontSize: 10),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      width: 200,
                      child: ElevatedButton(
                          onPressed: () => action!(lot.id!),
                          child: Text("VER MAIS")),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        onTap: () => action!(lot.id!),
      );
    }
  }
}
