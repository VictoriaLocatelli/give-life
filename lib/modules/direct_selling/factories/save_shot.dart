import 'package:give_life/modules/direct_selling/models/input_save_shot.dart';

abstract class SaveShot {
  Future<dynamic> save(InputSaveShot data);
}
