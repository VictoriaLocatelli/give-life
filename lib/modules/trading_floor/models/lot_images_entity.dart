class LotImagesEntity {
  int? id;
  int? lotId;
  String? filename;
  int? type;
  String? createdAt;
  String? updatedAt;
  String? fileUrl;
  String? urlVideo;

  LotImagesEntity(
      {this.id,
      this.lotId,
      this.filename,
      this.type,
      this.createdAt,
      this.updatedAt,
      this.fileUrl,
      this.urlVideo});

  factory LotImagesEntity.fromDomain(Map<String, dynamic> json) {
    json;
    return LotImagesEntity(
        id: json['id'],
        lotId: json['lot_id'],
        filename: json['filename'],
        type: json['type'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        fileUrl: json['file_url']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['lot_id'] = lotId;
    data['filename'] = filename;
    data['type'] = type;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['file_url'] = fileUrl;
    return data;
  }

  /*
id": 124,
          "lot_id": 139,
          "filename": "lots/V2UrCw6Yy6AFiUHlIrZ2gZfXC0knxqgu2wtziBd2.jpg",
          "type": 1,
          "created_at": "2022-12-01T21:11:50.000000Z",
          "updated_at": "2022-12-01T21:11:50.000000Z",
          "file_url":
              "https://bis365.com.br/bid365/storage/lots/V2UrCw6Yy6AFiUHlIrZ2gZfXC0knxqgu2wtziBd2.jpg"
  */

}
