import 'package:auto_size_text/auto_size_text.dart';
import 'package:custom_radio_grouped_button/custom_radio_grouped_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:give_life/modules/direct_selling/controllers/details/details_controller.dart';
import 'package:give_life/modules/direct_selling/views/widget/carrousselwidget/carroussel_widget.dart';
import 'package:give_life/modules/direct_selling/views/widget/last_bids_trading_flor_widget.dart';
import 'package:give_life/modules/direct_selling/views/widget/youtube/youtube_players_widget.dart';
import 'package:give_life/shared/componets/constants/spacing.dart';
import 'package:give_life/shared/componets/theme/theme.dart';
import 'package:give_life/shared/componets/utils/launch_link.dart';
import 'package:give_life/shared/componets/utils/scaffold_messenger.dart';
import 'package:give_life/shared/componets/widgets/bottom_widget.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../../../../shared/componets/constants/app_images.dart';

TextStyle styleInfoLot = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);

class DetailsTradingView extends StatelessWidget {
  const DetailsTradingView({
    super.key,
    required this.id,
    required this.controller,
  });
  final int id;
  final DetailsController controller;

  @override
  Widget build(BuildContext context) {
    controller.loadData(id);
    controller.pageContext = context;
    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;
    final textButtonStyle = TextButton.styleFrom(
      backgroundColor: Colors.transparent,
      alignment: Alignment.centerLeft,
    );
    //lib do youtube não aceita  url null
    if (controller.lot?.video != null) {
      return YoutubePlayerBuilder(
        player: YoutubePlayer(
          controller: YoutubePlayerController(
            initialVideoId:
                YoutubePlayer.convertUrlToId(controller.lot!.video!)!,
            flags: const YoutubePlayerFlags(
              autoPlay: false,
              mute: false,
            ),
          ),
          showVideoProgressIndicator: true,
          onReady: () => debugPrint('Redy'),
          bottomActions: [
            CurrentPosition(),
            ProgressBar(
              isExpanded: true,
              colors: const ProgressBarColors(
                  playedColor: AppColors.primary,
                  handleColor: AppColors.secondary),
            ),
            FullScreenButton(),
            const PlaybackSpeedButton(),
          ],
        ),
        builder: (context, player) => Scaffold(
          appBar: AppBar(
            title: const Text("Pregão"),
          ),
          body: Observer(builder: (context) {
            if (controller.isLoading) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            ImageProvider<Object> img =
                controller.lot?.images?[0].fileUrl != null
                    ? NetworkImage(controller.lot!.images![0].fileUrl!)
                    : AssetImage(AppImages.noProfilePicture.path)
                        as ImageProvider<Object>;
            return LayoutBuilder(builder: (context, c) {
              return Container(
                width: c.maxWidth,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                          child: CarrousselWidget(
                        contents: controller.lot!.images!,
                      )
                          //  controller.lot!.video != null
                          //     ? YoutubePlayerWidget(
                          //         url: controller.lot!.video!,
                          //       )
                          // : Container(),
                          ),
                      Container(
                        width: c.maxWidth,
                        height: 120,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Expanded(
                                flex: 5,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        const SizedBox(height: Spacing.xxSmall),
                                        Text(
                                          '${controller.lot!.name}',
                                          textAlign: TextAlign.left,
                                          style: const TextStyle(
                                            color: AppColors.black,
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      //controller.lot!.shots != null
                                      /* '${controller.getFormatMoney(controller.lot!.lastShot?.value)} '
                                    , */
                                      // :
                                      '${controller.getFormatMoney(controller.lot!.initValue)} '
                                      '${controller.lot!.condition}',
                                      textAlign: TextAlign.left,
                                      style: const TextStyle(
                                        color: AppColors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 7,
                                    ),
                                    Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        const SizedBox(height: Spacing.xxSmall),
                                        Image.asset(
                                            AppImages.calendarStart.path,
                                            width: 25,
                                            color: AppColors.black),
                                        Text(
                                          '${controller.getFormatDate(controller.lot!.dateInit)} as ${controller.getFormatHour(controller.lot!.hourInit)}',
                                          textAlign: TextAlign.left,
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        const SizedBox(height: Spacing.xxSmall),
                                        Image.asset(AppImages.calendar.path,
                                            width: 25, color: AppColors.black),
                                        Text(
                                          '${controller.getFormatDate(controller.lot!.dateEnd!)} as ${controller.getFormatHour(controller.lot!.hourEnd!)}',
                                          textAlign: TextAlign.left,
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                // ),
                                // Expanded(
                                //   flex: 4,
                                //   child: Padding(
                                //       padding: const EdgeInsets.all(20),
                                //       child: Column(
                                //         mainAxisAlignment: MainAxisAlignment.center,
                                //         crossAxisAlignment: CrossAxisAlignment.center,
                                //         mainAxisSize: MainAxisSize.min,
                                //         children: [
                                //           ElevatedButton(
                                //               onPressed: () {
                                //                 controller.toGallery(id);
                                //                 print(id);
                                //               },
                                //               child: Text("Galeria"))
                                //         ],
                                //       )),
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        width: c.maxWidth,
                        height: 200,
                        child: Container(
                          margin: const EdgeInsets.all(3),
                          padding: const EdgeInsets.all(3),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                                  padding: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                    color: AppColors.secondaryWhite,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(5.0),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color.fromRGBO(0, 0, 0, .05),
                                        offset: Offset(0.0, 4.0),
                                        blurRadius: 30.0,
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Align(
                                        alignment: Alignment.center,
                                        child: controller.lot!.status != 2
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                    left: 5.0, bottom: 5),
                                                child: Text(
                                                  'Lote Encerrado/ Vendido',
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: AppColors.red),
                                                ),
                                              )
                                            : Padding(
                                                padding: EdgeInsets.only(
                                                    left: 5.0, bottom: 5),
                                                child: Text(
                                                  'Faça seu lance agora mesmo!',
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color:
                                                          AppColors.secondary),
                                                ),
                                              ),
                                      ),
                                      IgnorePointer(
                                        ignoring: controller.lot!.status == 4 ||
                                                controller.lot!.status == 5 ||
                                                controller.lot!.status == 6
                                            ? true
                                            : false,
                                        child: CustomRadioButton(
                                          elevation: 0,
                                          height: 50,
                                          width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  2 -
                                              40,
                                          wrapAlignment:
                                              WrapAlignment.spaceBetween,
                                          enableButtonWrap: true,
                                          unSelectedColor: controller
                                                          .lot!.status ==
                                                      4 ||
                                                  controller.lot!.status == 5 ||
                                                  controller.lot!.status == 6
                                              ? Colors.grey
                                              : Theme.of(context).canvasColor,
                                          buttonLables: [
                                            if (controller.lot!.incValueOne !=
                                                0)
                                              '+${controller.getFormatMoney(controller.lot?.incValueOne)}',
                                            if (controller.lot!.incValueTwo !=
                                                0)
                                              '+${controller.getFormatMoney(controller.lot?.incValueTwo)}',
                                            if (controller.lot!.incValueThree !=
                                                0)
                                              '+${controller.getFormatMoney(controller.lot?.incValueThree)}',
                                            if (controller.lot!.incValueFour !=
                                                0)
                                              '+${controller.getFormatMoney(controller.lot?.incValueFour)}',
                                          ],
                                          buttonValues: <double>[
                                            if (controller.lot!.incValueOne !=
                                                0)
                                              controller.lot!.incValueOne!,
                                            if (controller.lot!.incValueTwo !=
                                                0)
                                              controller.lot!.incValueTwo!,
                                            if (controller.lot!.incValueThree !=
                                                0)
                                              controller.lot!.incValueThree!,
                                            if (controller.lot!.incValueFour !=
                                                0)
                                              controller.lot!.incValueFour!
                                          ],
                                          buttonTextStyle: ButtonTextStyle(
                                            selectedColor: AppColors.background,
                                            unSelectedColor: Colors.black,
                                            textStyle: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18,
                                            ),
                                          ),
                                          radioButtonValue: (value) =>
                                              controller.setBid(value),
                                          enableShape: true,
                                          customShape: RoundedRectangleBorder(
                                            side: BorderSide(),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5.0)),
                                          ),
                                          selectedColor: AppColors.green,
                                        ),
                                      )
                                    ],
                                  )),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        child: Column(
                          children: [
                            //const SizedBox(height: Spacing.small),
                            Observer(builder: (context) {
                              return Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: Spacing.large,
                                    vertical: Spacing.xSmall),
                                child: controller.lot!.status == 2
                                    ? OutlinedButton(
                                        style: OutlinedButton.styleFrom(
                                            backgroundColor: controller.isButton
                                                ? AppColors.secondary
                                                : AppColors.gray),
                                        onPressed: controller.isButton
                                            ? () => controller
                                                .toShot(controller.lot!.id!)
                                            : null,
                                        child: Text('CONFIRMAR LANCE',
                                            style: const TextStyle(
                                              color: AppColors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                            )))
                                    : Container(),
                              );
                            }),
                          ],
                        ),
                      ),
                      Container(
                          child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: Spacing.large,
                            vertical: Spacing.xSmall),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              const SizedBox(height: Spacing.small),
                              OutlinedButton(
                                  style: OutlinedButton.styleFrom(
                                      backgroundColor: AppColors.secondary),
                                  onPressed: () =>
                                      controller.lastShots(controller.lot!.id!),
                                  child: Text('ÚLTIMOS LANCES',
                                      style: const TextStyle(
                                        color: AppColors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      ))),
                            ]),
                      )),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          border: Border.all(
                            width: 1,
                            color: Color(0xffD9D9D9),
                            style: BorderStyle.solid,
                          ),
                          color: AppColors.secondaryWhite,
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            const SizedBox(height: Spacing.xxSmall),
                            Container(
                              margin: const EdgeInsets.all(3),
                              padding: const EdgeInsets.all(3),
                              child: Row(
                                children: [
                                  Text('Descrição:',
                                      textAlign: TextAlign.left,
                                      style: styleInfoLot),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.all(3),
                              padding: const EdgeInsets.all(3),
                              child: Row(
                                children: [
                                  Text(
                                    controller.lot!.description != null
                                        ? '${controller.lot!.description}'
                                        : " ",
                                    textAlign: TextAlign.left,
                                    style: const TextStyle(
                                      color: AppColors.black,
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          border: Border.all(
                            width: 1,
                            color: Color(0xffD9D9D9),
                            style: BorderStyle.solid,
                          ),
                          color: AppColors.secondaryWhite,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text('Quantidade:', style: styleInfoLot),
                                Padding(
                                  padding: EdgeInsets.only(top: 5, right: 3),
                                  child: Text(
                                    controller.lot!.quantity.toString(),
                                  ),
                                ),
                                Text(
                                  'Raça:',
                                  style: styleInfoLot,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5, right: 3),
                                  child: Text(
                                    "${controller.lot!.breed ?? ''}",
                                  ),
                                ),
                                Text(
                                  'Grau de sangue:',
                                  style: styleInfoLot,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5, right: 3),
                                  child: Text(
                                    "${controller.lot!.bloodGrade ?? ''}",
                                  ),
                                ),
                                Text(
                                  'Pelagem:',
                                  style: styleInfoLot,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5, right: 3),
                                  child: Text(
                                    "${controller.lot!.coat ?? ''}",
                                  ),
                                ),
                              ],
                            ),
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Text(
                                    'Idade:',
                                    style: styleInfoLot,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 5, right: 3),
                                    child: Text(
                                      "${controller.lot!.age ?? ''}",
                                    ),
                                  ),
                                  Text(
                                    'Categoria:',
                                    style: styleInfoLot,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 5, right: 3),
                                    child: Text(
                                      "${controller.lot!.category ?? ''}",
                                    ),
                                  ),
                                  Text(
                                    'CE(cm):',
                                    style: styleInfoLot,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 5, right: 3),
                                    child: Text(
                                      "${controller.lot!.ce ?? ''}",
                                    ),
                                  ),
                                  SizedBox(
                                    height: 50,
                                  )
                                ]),
                            Column(
                              /* crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceAround, */
                              children: [
                                Text(
                                  'Sexo:',
                                  style: styleInfoLot,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5, right: 3),
                                  child: Text(
                                    controller.lot!.gender == 'M'
                                        ? 'Macho'
                                        : controller.lot!.gender == 'F'
                                            ? 'Fêmea'
                                            : controller.lot!.gender == 'E'
                                                ? 'Embrião'
                                                : '',
                                  ),
                                ),
                                Text(
                                  'Peso médio:',
                                  style: styleInfoLot,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5, right: 3),
                                  child: Text(
                                    "${controller.lot!.averageWeight ?? ''}",
                                  ),
                                ),
                                Text(
                                  'TAT/RP:',
                                  style: styleInfoLot,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5, right: 3),
                                  child: Text(
                                    "${controller.lot!.tatRp ?? ''}",
                                  ),
                                ),
                                SizedBox(
                                  height: 50,
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          border: Border.all(
                            width: 1,
                            color: Color(0xffD9D9D9),
                            style: BorderStyle.solid,
                          ),
                          color: AppColors.secondaryWhite,
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            const SizedBox(height: Spacing.xxSmall),
                            Container(
                              margin: const EdgeInsets.all(3),
                              padding: const EdgeInsets.all(3),
                              child: Row(
                                children: [
                                  Text('Vendedor:',
                                      textAlign: TextAlign.left,
                                      style: styleInfoLot),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.all(3),
                              padding: const EdgeInsets.all(3),
                              child: Row(
                                children: [
                                  Text(
                                    controller.lot!.location != null
                                        ? '${controller.lot!.salesman}'
                                        : " ",
                                    textAlign: TextAlign.left,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          border: Border.all(
                            width: 1,
                            color: Color(0xffD9D9D9),
                            style: BorderStyle.solid,
                          ),
                          color: AppColors.secondaryWhite,
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            const SizedBox(height: Spacing.xxSmall),
                            Container(
                              margin: const EdgeInsets.all(3),
                              padding: const EdgeInsets.all(3),
                              child: Row(
                                children: [
                                  Text('Localização:',
                                      textAlign: TextAlign.left,
                                      style: styleInfoLot),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.all(3),
                              padding: const EdgeInsets.all(3),
                              child: Row(
                                children: [
                                  Text(
                                    controller.lot!.location != null
                                        ? '${controller.lot!.location}'
                                        : " ",
                                    textAlign: TextAlign.left,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          border: Border.all(
                            width: 1,
                            color: Color(0xffD9D9D9),
                            style: BorderStyle.solid,
                          ),
                          color: AppColors.secondaryWhite,
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            const SizedBox(height: Spacing.xxSmall),
                            Container(
                              margin: const EdgeInsets.all(3),
                              padding: const EdgeInsets.all(3),
                              child: Text('Condições de pagamento:',
                                  textAlign: TextAlign.left,
                                  style: styleInfoLot),
                            ),
                            Container(
                              margin: const EdgeInsets.all(3),
                              padding: const EdgeInsets.all(3),
                              child: Text(
                                controller.lot!.paymentForm != null
                                    ? '${controller.lot!.paymentForm}/${controller.lot!.paymentForm} '
                                    : " ",
                                textAlign: TextAlign.left,
                              ),
                            )
                          ],
                        ),
                      ),
                      if (controller.lot!.observation != null)
                        Container(
                          margin: const EdgeInsets.all(3),
                          padding: const EdgeInsets.all(3),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(5.0),
                            ),
                            border: Border.all(
                              width: 1,
                              color: Color(0xffD9D9D9),
                              style: BorderStyle.solid,
                            ),
                            color: AppColors.secondaryWhite,
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              const SizedBox(height: Spacing.xxSmall),
                              Container(
                                margin: const EdgeInsets.all(3),
                                padding: const EdgeInsets.all(3),
                                child: Row(
                                  children: [
                                    Text('Observação:',
                                        textAlign: TextAlign.left,
                                        style: styleInfoLot),
                                  ],
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.all(3),
                                padding: const EdgeInsets.all(3),
                                child: Row(
                                  children: [
                                    Text(
                                      controller.lot!.observation != null
                                          ? '${controller.lot!.observation}'
                                          : " ",
                                      textAlign: TextAlign.left,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                    ],
                  ),
                ),
              );
            });
            //
          }),
          bottomNavigationBar: const BottomWidget(selected: 1),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Pregão"),
        ),
        body: Observer(builder: (context) {
          if (controller.isLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          ImageProvider<Object> img = controller.lot?.images?[0].fileUrl != null
              ? NetworkImage(controller.lot!.images![0].fileUrl!)
              : AssetImage(AppImages.noProfilePicture.path)
                  as ImageProvider<Object>;
          return LayoutBuilder(builder: (context, c) {
            return Container(
              width: c.maxWidth,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                        child: CarrousselWidget(
                      contents: controller.lot!.images!,
                    )
                        //  controller.lot!.video != null
                        //     ? YoutubePlayerWidget(
                        //         url: controller.lot!.video!,
                        //       )
                        // : Container(),
                        ),
                    Container(
                      width: c.maxWidth,
                      height: 120,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                              flex: 5,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      const SizedBox(height: Spacing.xxSmall),
                                      Text(
                                        '${controller.lot!.name}',
                                        textAlign: TextAlign.left,
                                        style: const TextStyle(
                                          color: AppColors.black,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    //controller.lot!.shots != null
                                    /* '${controller.getFormatMoney(controller.lot!.lastShot?.value)} '
                                    , */
                                    // :
                                    '${controller.getFormatMoney(controller.lot!.initValue)} '
                                    '${controller.lot!.condition}',
                                    textAlign: TextAlign.left,
                                    style: const TextStyle(
                                      color: AppColors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 7,
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      const SizedBox(height: Spacing.xxSmall),
                                      Image.asset(AppImages.calendarStart.path,
                                          width: 25, color: AppColors.black),
                                      Text(
                                        '${controller.getFormatDate(controller.lot!.dateInit)} as ${controller.getFormatHour(controller.lot!.hourInit)}',
                                        textAlign: TextAlign.left,
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      const SizedBox(height: Spacing.xxSmall),
                                      Image.asset(AppImages.calendar.path,
                                          width: 25, color: AppColors.black),
                                      Text(
                                        '${controller.getFormatDate(controller.lot!.dateEnd!)} as ${controller.getFormatHour(controller.lot!.hourEnd!)}',
                                        textAlign: TextAlign.left,
                                      ),
                                    ],
                                  )
                                ],
                              ),
                              // ),
                              // Expanded(
                              //   flex: 4,
                              //   child: Padding(
                              //       padding: const EdgeInsets.all(20),
                              //       child: Column(
                              //         mainAxisAlignment: MainAxisAlignment.center,
                              //         crossAxisAlignment: CrossAxisAlignment.center,
                              //         mainAxisSize: MainAxisSize.min,
                              //         children: [
                              //           ElevatedButton(
                              //               onPressed: () {
                              //                 controller.toGallery(id);
                              //                 print(id);
                              //               },
                              //               child: Text("Galeria"))
                              //         ],
                              //       )),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      width: c.maxWidth,
                      height: 200,
                      child: Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                                margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                                padding: EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                  color: AppColors.secondaryWhite,
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(5.0),
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, .05),
                                      offset: Offset(0.0, 4.0),
                                      blurRadius: 30.0,
                                    ),
                                  ],
                                ),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Align(
                                      alignment: Alignment.center,
                                      child: controller.lot!.status != 2
                                          ? Padding(
                                              padding: EdgeInsets.only(
                                                  left: 5.0, bottom: 5),
                                              child: Text(
                                                'Lote Encerrado/ Vendido',
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.bold,
                                                    color: AppColors.red),
                                              ),
                                            )
                                          : Padding(
                                              padding: EdgeInsets.only(
                                                  left: 5.0, bottom: 5),
                                              child: Text(
                                                'Faça seu lance agora mesmo!',
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.bold,
                                                    color: AppColors.secondary),
                                              ),
                                            ),
                                    ),
                                    IgnorePointer(
                                      ignoring: controller.lot!.status == 4 ||
                                              controller.lot!.status == 5 ||
                                              controller.lot!.status == 6
                                          ? true
                                          : false,
                                      child: CustomRadioButton(
                                        elevation: 0,
                                        height: 50,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                    2 -
                                                40,
                                        wrapAlignment:
                                            WrapAlignment.spaceBetween,
                                        enableButtonWrap: true,
                                        unSelectedColor:
                                            controller.lot!.status == 4 ||
                                                    controller.lot!.status ==
                                                        5 ||
                                                    controller.lot!.status == 6
                                                ? Colors.grey
                                                : Theme.of(context).canvasColor,
                                        buttonLables: [
                                          if (controller.lot!.incValueOne != 0)
                                            '+${controller.getFormatMoney(controller.lot?.incValueOne)}',
                                          if (controller.lot!.incValueTwo != 0)
                                            '+${controller.getFormatMoney(controller.lot?.incValueTwo)}',
                                          if (controller.lot!.incValueThree !=
                                              0)
                                            '+${controller.getFormatMoney(controller.lot?.incValueThree)}',
                                          if (controller.lot!.incValueFour != 0)
                                            '+${controller.getFormatMoney(controller.lot?.incValueFour)}',
                                        ],
                                        buttonValues: <double>[
                                          if (controller.lot!.incValueOne != 0)
                                            controller.lot!.incValueOne!,
                                          if (controller.lot!.incValueTwo != 0)
                                            controller.lot!.incValueTwo!,
                                          if (controller.lot!.incValueThree !=
                                              0)
                                            controller.lot!.incValueThree!,
                                          if (controller.lot!.incValueFour != 0)
                                            controller.lot!.incValueFour!
                                        ],
                                        buttonTextStyle: ButtonTextStyle(
                                          selectedColor: AppColors.background,
                                          unSelectedColor: Colors.black,
                                          textStyle: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18,
                                          ),
                                        ),
                                        radioButtonValue: (value) =>
                                            controller.setBid(value),
                                        enableShape: true,
                                        customShape: RoundedRectangleBorder(
                                          side: BorderSide(),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5.0)),
                                        ),
                                        selectedColor: AppColors.green,
                                      ),
                                    )
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          //const SizedBox(height: Spacing.small),
                          Observer(builder: (context) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: Spacing.large,
                                  vertical: Spacing.xSmall),
                              child: controller.lot!.status == 2
                                  ? OutlinedButton(
                                      style: OutlinedButton.styleFrom(
                                          backgroundColor: controller.isButton
                                              ? AppColors.secondary
                                              : AppColors.gray),
                                      onPressed: controller.isButton
                                          ? () => controller
                                              .toShot(controller.lot!.id!)
                                          : null,
                                      child: Text('CONFIRMAR LANCE',
                                          style: const TextStyle(
                                            color: AppColors.white,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                          )))
                                  : Container(),
                            );
                          }),
                        ],
                      ),
                    ),
                    Container(
                        child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: Spacing.large, vertical: Spacing.xSmall),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            const SizedBox(height: Spacing.small),
                            OutlinedButton(
                                style: OutlinedButton.styleFrom(
                                    backgroundColor: AppColors.secondary),
                                onPressed: () =>
                                    controller.lastShots(controller.lot!.id!),
                                child: Text('ÚLTIMOS LANCES',
                                    style: const TextStyle(
                                      color: AppColors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ))),
                          ]),
                    )),
                    Container(
                      margin: const EdgeInsets.all(3),
                      padding: const EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5.0),
                        ),
                        border: Border.all(
                          width: 1,
                          color: Color(0xffD9D9D9),
                          style: BorderStyle.solid,
                        ),
                        color: AppColors.secondaryWhite,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const SizedBox(height: Spacing.xxSmall),
                          Container(
                            margin: const EdgeInsets.all(3),
                            padding: const EdgeInsets.all(3),
                            child: Row(
                              children: [
                                Text('Descrição:',
                                    textAlign: TextAlign.left,
                                    style: styleInfoLot),
                              ],
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(3),
                            padding: const EdgeInsets.all(3),
                            child: Row(
                              children: [
                                Text(
                                  controller.lot!.description != null
                                      ? '${controller.lot!.description}'
                                      : " ",
                                  textAlign: TextAlign.left,
                                  style: const TextStyle(
                                    color: AppColors.black,
                                    fontSize: 14,
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(3),
                      padding: const EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5.0),
                        ),
                        border: Border.all(
                          width: 1,
                          color: Color(0xffD9D9D9),
                          style: BorderStyle.solid,
                        ),
                        color: AppColors.secondaryWhite,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text('Quantidade:', style: styleInfoLot),
                              Padding(
                                padding: EdgeInsets.only(top: 5, right: 3),
                                child: Text(
                                  controller.lot!.quantity.toString(),
                                ),
                              ),
                              Text(
                                'Raça:',
                                style: styleInfoLot,
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 5, right: 3),
                                child: Text(
                                  "${controller.lot!.breed ?? ''}",
                                ),
                              ),
                              Text(
                                'Grau de sangue:',
                                style: styleInfoLot,
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 5, right: 3),
                                child: Text(
                                  "${controller.lot!.bloodGrade ?? ''}",
                                ),
                              ),
                              Text(
                                'Pelagem:',
                                style: styleInfoLot,
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 5, right: 3),
                                child: Text(
                                  "${controller.lot!.coat ?? ''}",
                                ),
                              ),
                            ],
                          ),
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Text(
                                  'Idade:',
                                  style: styleInfoLot,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5, right: 3),
                                  child: Text(
                                    "${controller.lot!.age ?? ''}",
                                  ),
                                ),
                                Text(
                                  'Categoria:',
                                  style: styleInfoLot,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5, right: 3),
                                  child: Text(
                                    "${controller.lot!.category ?? ''}",
                                  ),
                                ),
                                Text(
                                  'CE(cm):',
                                  style: styleInfoLot,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5, right: 3),
                                  child: Text(
                                    "${controller.lot!.ce ?? ''}",
                                  ),
                                ),
                                SizedBox(
                                  height: 50,
                                )
                              ]),
                          Column(
                            /* crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceAround, */
                            children: [
                              Text(
                                'Sexo:',
                                style: styleInfoLot,
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 5, right: 3),
                                child: Text(
                                  controller.lot!.gender == 'M'
                                      ? 'Macho'
                                      : controller.lot!.gender == 'F'
                                          ? 'Fêmea'
                                          : controller.lot!.gender == 'E'
                                              ? 'Embrião'
                                              : '',
                                ),
                              ),
                              Text(
                                'Peso médio:',
                                style: styleInfoLot,
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 5, right: 3),
                                child: Text(
                                  "${controller.lot!.averageWeight ?? ''}",
                                ),
                              ),
                              Text(
                                'TAT/RP:',
                                style: styleInfoLot,
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 5, right: 3),
                                child: Text(
                                  "${controller.lot!.tatRp ?? ''}",
                                ),
                              ),
                              SizedBox(
                                height: 50,
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(3),
                      padding: const EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5.0),
                        ),
                        border: Border.all(
                          width: 1,
                          color: Color(0xffD9D9D9),
                          style: BorderStyle.solid,
                        ),
                        color: AppColors.secondaryWhite,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const SizedBox(height: Spacing.xxSmall),
                          Container(
                            margin: const EdgeInsets.all(3),
                            padding: const EdgeInsets.all(3),
                            child: Row(
                              children: [
                                Text('Vendedor:',
                                    textAlign: TextAlign.left,
                                    style: styleInfoLot),
                              ],
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(3),
                            padding: const EdgeInsets.all(3),
                            child: Row(
                              children: [
                                Text(
                                  controller.lot!.location != null
                                      ? '${controller.lot!.salesman}'
                                      : " ",
                                  textAlign: TextAlign.left,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(3),
                      padding: const EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5.0),
                        ),
                        border: Border.all(
                          width: 1,
                          color: Color(0xffD9D9D9),
                          style: BorderStyle.solid,
                        ),
                        color: AppColors.secondaryWhite,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const SizedBox(height: Spacing.xxSmall),
                          Container(
                            margin: const EdgeInsets.all(3),
                            padding: const EdgeInsets.all(3),
                            child: Row(
                              children: [
                                Text('Localização:',
                                    textAlign: TextAlign.left,
                                    style: styleInfoLot),
                              ],
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(3),
                            padding: const EdgeInsets.all(3),
                            child: Row(
                              children: [
                                Text(
                                  controller.lot!.location != null
                                      ? '${controller.lot!.location}'
                                      : " ",
                                  textAlign: TextAlign.left,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(3),
                      padding: const EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5.0),
                        ),
                        border: Border.all(
                          width: 1,
                          color: Color(0xffD9D9D9),
                          style: BorderStyle.solid,
                        ),
                        color: AppColors.secondaryWhite,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const SizedBox(height: Spacing.xxSmall),
                          Container(
                            margin: const EdgeInsets.all(3),
                            padding: const EdgeInsets.all(3),
                            child: Text('Condições de pagamento:',
                                textAlign: TextAlign.left, style: styleInfoLot),
                          ),
                          Container(
                            margin: const EdgeInsets.all(3),
                            padding: const EdgeInsets.all(3),
                            child: Text(
                              controller.lot!.paymentForm != null
                                  ? '${controller.lot!.paymentForm}/${controller.lot!.paymentForm} '
                                  : " ",
                              textAlign: TextAlign.left,
                            ),
                          )
                        ],
                      ),
                    ),
                    if (controller.lot!.observation != null)
                      Container(
                        margin: const EdgeInsets.all(3),
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          border: Border.all(
                            width: 1,
                            color: Color(0xffD9D9D9),
                            style: BorderStyle.solid,
                          ),
                          color: AppColors.secondaryWhite,
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            const SizedBox(height: Spacing.xxSmall),
                            Container(
                              margin: const EdgeInsets.all(3),
                              padding: const EdgeInsets.all(3),
                              child: Row(
                                children: [
                                  Text('Observação:',
                                      textAlign: TextAlign.left,
                                      style: styleInfoLot),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.all(3),
                              padding: const EdgeInsets.all(3),
                              child: Row(
                                children: [
                                  Text(
                                    controller.lot!.observation != null
                                        ? '${controller.lot!.observation}'
                                        : " ",
                                    textAlign: TextAlign.left,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                  ],
                ),
              ),
            );
          });
          //
        }),
        bottomNavigationBar: const BottomWidget(selected: 1),
      );
    }
  }
}
