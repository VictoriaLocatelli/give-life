import 'package:flutter/material.dart';
import 'package:give_life/shared/componets/theme/theme.dart';

class ButtonWidget extends StatelessWidget {
  final Function onTap;
  final Color textColor;
  final String label;
  final Widget? icon;
  final double height;
  final Color buttonColor;
  final List<BoxShadow> boxShadow;

  const ButtonWidget({
    Key? key,
    required this.onTap,
    required this.label,
    this.icon,
    this.buttonColor = AppColors.secondary,
    this.boxShadow = const [
      BoxShadow(
        color: AppColors.gray,
        offset: Offset(0.0, 4.0),
        blurRadius: 30.0,
      ),
    ],
    this.height = 50,
    this.textColor = AppColors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: height,
      decoration: BoxDecoration(
        boxShadow: boxShadow,
      ),
      child: TextButton(
        onPressed: onTap(),
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(buttonColor)),
        child: icon != null
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    label,
                    style: TextStyle(
                      color: textColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 15,
                    ),
                  ),
                ],
              )
            : Text(
                label,
                style: TextStyle(
                  color: textColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
      ),
    );
  }
}
