import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/initial_client_profile.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:give_life/shared/componets/constants/spacing.dart';
import 'package:give_life/shared/componets/widgets/app_version_with_logo.dart';
import 'package:give_life/shared/componets/widgets/bottom_widget.dart';
import 'package:give_life/shared/componets/widgets/navigation_drawer.dart';
import 'package:give_life/shared/data/contracts/cache/cache_storage.dart';
import 'package:give_life/shared/data/contracts/local/remove_client_current_account.dart';

class ClientProfileView extends StatefulWidget {
  const ClientProfileView({super.key, required this.controller});
  final InitialClientProfile controller;

  @override
  State<ClientProfileView> createState() => _ClientProfileViewState();
}

class _ClientProfileViewState extends State<ClientProfileView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.controller.loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (context) {
      if (widget.controller.isLoading)
        return const Center(
          child: CircularProgressIndicator(),
        );
      return Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(180),
          child: AppBar(
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(150),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 50),
                    child: LayoutBuilder(builder: (context, c) {
                      return Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Container(
                            margin: EdgeInsets.all(0),
                            width: c.maxWidth * .35,
                            child: Container(
                              margin: EdgeInsets.all(0),
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: widget.controller.account?.photo !=
                                          null
                                      ? NetworkImage(
                                              widget.controller.account!.photo!)
                                          as ImageProvider<Object>
                                      : AssetImage(
                                              AppImages.noProfilePicture.path)
                                          as ImageProvider<Object>,
                                ), //Imagem a ser trocada
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(8.0),
                            margin: EdgeInsets.all(0),
                            width: c.maxWidth * .65,
                            child: AutoSizeText(
                              "${widget.controller.account?.name}",
                              textAlign: TextAlign.left,
                              style:
                                  TextStyle(fontSize: 22, color: Colors.white),
                              maxLines: 3,
                            ),
                          )
                        ],
                      );
                    }),
                  ),
                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(50)),
                    ),
                  ),
                  Divider(
                    height: 1,
                  )
                ],
              ),
            ),
          ),
        ),
        drawer: const NavigationDrawer(),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              flex: 1,
              child: GestureDetector(
                onTap: () => Modular.to.navigate('/profile/edit'),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Container(
                        width: 30,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.contain,
                                image: AssetImage(AppImages.edit.path))),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Editar cadastro",
                      style: TextStyle(fontSize: 20),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Expanded(
              flex: 1,
              child: GestureDetector(
                onTap: () => _showLogoutModalBottomSheet(context),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Container(
                        width: 30,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.contain,
                                image: AssetImage(AppImages.leave.path))),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Sair",
                      style: TextStyle(fontSize: 20),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 7,
              child: Container(
                alignment: Alignment.bottomCenter,
                child: AppVersionWithLogo(
                  color: Colors.black,
                  textColor: Colors.black,
                ),
              ),
            )
          ],
        ),
        bottomNavigationBar: BottomWidget(
          selected: 0,
        ),
      );
    });
  }
}

Future<void> _showLogoutModalBottomSheet(BuildContext context) {
  return showModalBottomSheet(
    context: context,
    builder: (context) => Container(
      height: 250,
      padding: const EdgeInsets.all(Spacing.medium),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Você tem certeza que deseja sair do aplicativo?',
            style: Theme.of(context).textTheme.headline6,
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: Spacing.xLarge),
          ElevatedButton(
            child: const Text('Sair'),
            onPressed: () async {
              await Modular.get<CacheStorage>().delete('account');
              await Modular.get<RemoveClientCurrentAccount>().remove();
              await Modular.get<CacheStorage>().clean();

              Modular.to.navigate('/auth/');
            },
          ),
          const SizedBox(height: Spacing.medium),
          OutlinedButton(
            child: const Text('Ficar'),
            onPressed: () => Navigator.pop(context),
          ),
        ],
      ),
    ),
  );
}
