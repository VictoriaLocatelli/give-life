import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/help/models/load_email.dart';
import 'package:give_life/modules/help/usescases/remote_load_email.dart';
import 'package:give_life/shared/data/factories/api_url_factory.dart';
import 'package:give_life/shared/data/http/http_client.dart';

LoadEmail makeRemoteLoadEmail() => RemoteLoadEmail(
      url: [
        makeApiUrl('/parameters/EMAIL'),
        makeApiUrl('/parameters/CONTATO'),
        makeApiUrl('/settings')
      ],
      httpClient: Modular.get<HttpClient>(),
    );
