import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:give_life/modules/home/controller/home_controller.dart';
import 'package:give_life/modules/home/view/widgets/online_auctions_widget.dart';
import 'package:give_life/modules/home/view/widgets/schedule_auctions_widget.dart';
import 'package:give_life/shared/componets/widgets/bottom_widget.dart';
import 'package:give_life/shared/componets/widgets/navigation_drawer.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key, required this.controller}) : super(key: key);
  final HomeController controller;

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.controller.loadData();
  }

  TextStyle styleColumnTwo =
      TextStyle(fontSize: 20, fontWeight: FontWeight.bold);
  @override
  Widget build(BuildContext context) {
    return Observer(builder: (context) {
      if (widget.controller.isLoading)
        return Center(child: CircularProgressIndicator());
      return Scaffold(
        appBar: AppBar(
          title: Text("Agenda de Leilões"),
          centerTitle: true,
        ),
        drawer: const NavigationDrawer(),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                //Live Auctions
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      if (widget.controller.auctionsOnline != null) ...[
                        Row(
                          children: [
                            ...widget.controller.auctionsOnline!
                                .map((e) => OnlineAuctionsWidget(
                                      auction: e,
                                    ))
                                .toList()
                          ],
                        )
                      ] else ...[
                        Container(
                            child: const Padding(
                                padding:
                                    EdgeInsets.only(top: 10.0, bottom: 6.0),
                                child: Text(
                                  'Ainda sem leilões na agenda, mas fique de olho!',
                                  style: const TextStyle(
                                      color: Colors.black,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ))),
                      ]
                    ],
                  ),
                ),
                //Schedule Auctions
                SizedBox(
                  height: 20,
                ),
                Text("Próximos Leilões", style: styleColumnTwo),
                SizedBox(
                  height: 20,
                ),

                if (widget.controller.auction != null) ...[
                  ...widget.controller.auction!
                      .map((scheduleAuction) => ScheduleAuctionsWidget(
                            scheduleAuction: scheduleAuction,
                            action: widget.controller.toDetail,
                          ))
                      .toList()
                ] else ...[
                  Container(
                      child: const Padding(
                          padding: EdgeInsets.only(top: 10.0, bottom: 6.0),
                          child: Text(
                            'Ainda sem leilões na agenda, mas fique de olho!',
                            style: const TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ))),
                ]
              ],
            ),
          ),
        ),
        bottomNavigationBar: BottomWidget(
          selected: 2,
        ),
      );
    });
  }
}
