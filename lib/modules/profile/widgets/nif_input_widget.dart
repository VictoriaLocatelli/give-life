import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:brasil_fields/brasil_fields.dart' as brasil_fields;
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';

class NifInputWidget extends StatelessWidget {
  const NifInputWidget();

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();

    return Observer(builder: (_) {
      return CustomTextFormField(
        label: 'CPF',
        initialValue: controller.nif,
        onChanged: controller.nifValidator,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          errorText: controller.nifError,
        ),
        inputFormatters: [
          FilteringTextInputFormatter.digitsOnly,
          brasil_fields.CpfOuCnpjFormatter(),
        ],
      );
    });
  }
}
