import 'package:flutter/material.dart';
import 'package:image_fade/image_fade.dart';

class NetWorkFadeImageWidget extends StatelessWidget {
  NetWorkFadeImageWidget({super.key, required this.url, this.widget});
  final String url;
  Widget? widget;
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, c) {
      return Column(mainAxisSize: MainAxisSize.min, children: [
        Expanded(
          flex: 15,
          child: Container(
            width: c.maxWidth,
            height: c.maxWidth,
            child: ImageFade(
              // whenever the image changes, it will be loaded, and then faded in:
              image: NetworkImage(url),

              // slow fade for newly loaded images:
              duration: const Duration(milliseconds: 900),

              // if the image is loaded synchronously (ex. from memory), fade in faster:
              syncDuration: const Duration(milliseconds: 150),

              // supports most properties of Image:
              alignment: Alignment.center,
              fit: BoxFit.cover,

              // shown behind everything:
              placeholder: Container(
                color: const Color(0xFFCFCDCA),
                alignment: Alignment.center,
                child:
                    const Icon(Icons.photo, color: Colors.white30, size: 128.0),
              ),

              // shows progress while loading an image:
              loadingBuilder: (context, progress, chunkEvent) =>
                  Center(child: CircularProgressIndicator(value: progress)),

              // displayed when an error occurs:
              errorBuilder: (context, error) => Container(
                color: const Color(0xFF6F6D6A),
                alignment: Alignment.center,
                child: const Icon(Icons.warning,
                    color: Colors.black26, size: 128.0),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(
              child: Container(
            width: c.maxWidth,
            height: 50,
            child: widget,
            color: Theme.of(context).primaryColor,
          )),
        )
      ]);
    });
  }
}
