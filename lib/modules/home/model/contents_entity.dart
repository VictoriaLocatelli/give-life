class ContentsEntity {
  int? id;
  int? auctionid;
  int? type;
  String? filename;
  String? title;
  String? description;
  String? url;
  String? createdat;
  String? updatedat;

  ContentsEntity(
      {this.id,
      this.auctionid,
      this.type,
      this.filename,
      this.title,
      this.description,
      this.url,
      this.createdat,
      this.updatedat});

  ContentsEntity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    auctionid = json['auction_id'];
    type = json['type'];
    filename = json['filename'];
    title = json['title'];
    description = json['description'];
    url = json['url'];
    createdat = json['created_at'];
    updatedat = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['auction_id'] = auctionid;
    data['type'] = type;
    data['filename'] = filename;
    data['title'] = title;
    data['description'] = description;
    data['url'] = url;
    data['created_at'] = createdat;
    data['updated_at'] = updatedat;
    return data;
  }
}
