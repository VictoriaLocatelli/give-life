import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:give_life/modules/direct_selling/views/widget/table_row_widget.dart';

import 'package:give_life/modules/home/view/widgets/list_lot_widget.dart';
import 'package:give_life/modules/trading_floor/models/input_save_shot.dart';
import 'package:give_life/modules/trading_floor/models/lot_entity.dart';
/* import 'package:give_life/modules/trading_floor/models/input_save_shot.dart';
import 'package:give_life/modules/trading_floor/models/lot_entity.dart';
import 'package:give_life/modules/trading_floor/views/widget/table_row_widget.dart'; */
import 'package:give_life/shared/componets/constants/constants.dart';
import 'package:give_life/shared/componets/theme/theme.dart';
import 'package:intl/intl.dart';

String getFormatMoney(dynamic value) =>
    NumberFormat.currency(symbol: 'R\$', locale: 'pt_BR').format(value);

String getFormatDate(String value) =>
    DateFormat('dd/MM/yyyy').format(DateTime.parse(value));

String getFormatHour(String value) =>
    DateFormat('HH:mm').format(DateTime.parse('2000-01-01 ' + value));

Future<void> alertModal(BuildContext context, String message) => showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        icon: SvgPicture.asset(AppImages.warning.path),
        title: Text(
          'Atenção!',
          style: TextStyle(
            color: AppColors.black,
            fontSize: 28,
            fontWeight: FontWeight.bold,
          ),
        ),
        content: Column(mainAxisSize: MainAxisSize.min, children: [
          Text('$message'),
          SizedBox(
            height: 20,
          ),
          ElevatedButton(
              onPressed: () => Navigator.pop(context, true), child: Text("OK"))
        ]),
      );
    });

Future<void> listLots({
  required BuildContext context,
  required String message,
  required List lots,
}) =>
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              title: Text(
                'Lotes do leilão',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: AppColors.black,
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                ),
              ),
              content: CustomScrollView(shrinkWrap: true, slivers: [
                SliverGrid(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 8.0,
                        crossAxisSpacing: 8.0,
                        mainAxisExtent: 50),
                    delegate: SliverChildListDelegate([
                      ...lots
                          .map((lot) => ListLotWidget(
                                lotName: lot.name,
                                lotId: lot.id,
                              ))
                          .toList()
                    ])),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: ElevatedButton(
                        onPressed: () => Navigator.of(context).pop(),
                        child: Text("Sair")),
                  ),
                )
              ]));
        });
Future<void> NotLot({
  required BuildContext context,
  required String message,
  required List lots,
}) =>
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
              icon: SvgPicture.asset(AppImages.warning.path),
              title: Text(
                'Atenção',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: AppColors.black,
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                ),
              ),
              content: Column(mainAxisSize: MainAxisSize.min, children: [
                Text('$message'),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () => Navigator.pop(context, true),
                    child: Text("OK"))
              ]));
        });

Future<void> confirmModal(
        {required BuildContext context,
        required double bid,
        required LotEntity lot,
        required Function(InputSaveShot data) action}) =>
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            icon: SvgPicture.asset(
              AppImages.warning.path,
              color: AppColors.secondary,
            ),
            title: Text(
              'Confirmação!',
              style: TextStyle(
                color: AppColors.black,
                fontSize: 28,
                fontWeight: FontWeight.bold,
              ),
            ),
            content: Column(mainAxisSize: MainAxisSize.min, children: [
              SizedBox(
                height: 10,
              ),
              Container(
                child: Container(
                  margin: const EdgeInsets.all(3),
                  padding: const EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(5.0),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: AppColors.black,
                        offset: Offset(0.0, 2.0),
                        blurRadius: 7.0,
                      ),
                    ],
                    color: AppColors.secondaryWhite,
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      const SizedBox(height: Spacing.xxSmall),
                      Text(
                        '${getFormatMoney(bid)}',
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                          color: AppColors.black,
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(
                    flex: 50,
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: Spacing.xxSmall,
                      ),
                      child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  AppColors.secondaryWhite),
                              foregroundColor:
                                  MaterialStateProperty.all(AppColors.primary),
                              side: MaterialStateProperty.all(const BorderSide(
                                  color: AppColors.primary, width: 2))),
                          onPressed: () => Navigator.pop(context, true),
                          child: Text("Cancelar")),
                    ),
                  ),
                  Expanded(
                    flex: 50,
                    child: Container(
                      child: ElevatedButton(
                          onPressed: () => action(InputSaveShot(
                              comesFrom: (lot.shots!.firstWhere(
                                  (shot) => lot.id == shot.lotId)).comesFrom,
                              lotId: lot.id!,
                              value: bid)),
                          child: Text("Confirmar")),
                    ),
                  ),
                ],
              ),
            ]),
          );
        });

Future<void> showModal(
        {required BuildContext context, required Function() action}) =>
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            icon: SvgPicture.asset(
              width: 100,
              height: 100,
              color: AppColors.green,
              AppImages.validation.path,
            ),
            title: Text(
              'Parabéns!',
              style: TextStyle(
                color: AppColors.black,
                fontSize: 28,
                fontWeight: FontWeight.bold,
              ),
            ),
            content: Column(mainAxisSize: MainAxisSize.min, children: [
              Text(
                'Seu lance foi realizado com sucesso!',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: AppColors.black,
                  fontSize: 20,
                ),
              ),
              ElevatedButton(
                  onPressed: () {
                    action();
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                  child: Text("OK"))
            ]),
          );
        });

Future<void> lastBidsModal(
        {required BuildContext context,
        required LotEntity lot,
        required Function() action}) =>
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(
              'Parabéns!',
              style: TextStyle(
                color: AppColors.black,
                fontSize: 28,
                fontWeight: FontWeight.bold,
              ),
            ),
            content: Column(mainAxisSize: MainAxisSize.min, children: [
              Container(
                width: double.infinity,
                margin: EdgeInsets.fromLTRB(15, 0, 15, 10),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, .05),
                      offset: Offset(0.0, 4.0),
                      blurRadius: 30.0,
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          'Lances',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 1,
                          color: Color(0xffD9D9D9),
                          style: BorderStyle.solid,
                        ),
                      ),
                      child: Table(
                        columnWidths: {
                          0: FlexColumnWidth(2),
                          1: FlexColumnWidth(6),
                          2: FlexColumnWidth(6),
                        },
                        border: TableBorder(
                          verticalInside: BorderSide(
                            color: Color(0xffD9D9D9),
                            style: BorderStyle.solid,
                            width: 1.0,
                          ),
                        ),
                        children: [
                          TableRow(
                            children:
                                'Nome-Valor-Data/Hora'.split('-').map((name) {
                              return Container(
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  color: AppColors.secondary,
                                ),
                                child: Text(
                                  name,
                                  style: TextStyle(
                                      fontSize: 15.0, color: Colors.white),
                                ),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 3.0, vertical: 10),
                              );
                            }).toList(),
                          ),
                          if (lot.shots != null)
                            for (int i = 0; i < lot.shots!.length; i++)
                              tableRow(
                                  '${lot.shots![i].client.name[0]}***|${getFormatMoney(lot.shots![i].value)}|${getFormatDate(lot.shots![i].date)} - ${getFormatHour(lot.shots![i].time)}'),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("OK"))
            ]),
          );
        });
