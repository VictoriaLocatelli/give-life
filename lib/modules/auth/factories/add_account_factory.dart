import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/auth/models/signup/add_account.dart';
import 'package:give_life/modules/auth/usecases/remote_add_account.dart';
import 'package:give_life/shared/data/factories/api_url_factory.dart';
import 'package:give_life/shared/data/http/http_client.dart';

AddAccount makeRemoteAddAccount() => RemoteAddAccount(
      httpClient: Modular.get<HttpClient>(),
      url: makeApiUrl('/auth/users'),
    );
