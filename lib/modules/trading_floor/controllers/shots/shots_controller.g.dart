// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shots_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ShotController on ShotControllerBase, Store {
  Computed<bool>? _$isLoadingComputed;

  @override
  bool get isLoading =>
      (_$isLoadingComputed ??= Computed<bool>(() => super.isLoading,
              name: 'ShotControllerBase.isLoading'))
          .value;

  late final _$_isLoadingAtom =
      Atom(name: 'ShotControllerBase._isLoading', context: context);

  @override
  bool get _isLoading {
    _$_isLoadingAtom.reportRead();
    return super._isLoading;
  }

  @override
  set _isLoading(bool value) {
    _$_isLoadingAtom.reportWrite(value, super._isLoading, () {
      super._isLoading = value;
    });
  }

  late final _$bidAtom = Atom(name: 'ShotControllerBase.bid', context: context);

  @override
  double? get bid {
    _$bidAtom.reportRead();
    return super.bid;
  }

  @override
  set bid(double? value) {
    _$bidAtom.reportWrite(value, super.bid, () {
      super.bid = value;
    });
  }

  late final _$isButtonAtom =
      Atom(name: 'ShotControllerBase.isButton', context: context);

  @override
  bool get isButton {
    _$isButtonAtom.reportRead();
    return super.isButton;
  }

  @override
  set isButton(bool value) {
    _$isButtonAtom.reportWrite(value, super.isButton, () {
      super.isButton = value;
    });
  }

  late final _$ShotControllerBaseActionController =
      ActionController(name: 'ShotControllerBase', context: context);

  @override
  void setBid(double value) {
    final _$actionInfo = _$ShotControllerBaseActionController.startAction(
        name: 'ShotControllerBase.setBid');
    try {
      return super.setBid(value);
    } finally {
      _$ShotControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setButton(bool value) {
    final _$actionInfo = _$ShotControllerBaseActionController.startAction(
        name: 'ShotControllerBase.setButton');
    try {
      return super.setButton(value);
    } finally {
      _$ShotControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
bid: ${bid},
isButton: ${isButton},
isLoading: ${isLoading}
    ''';
  }
}
