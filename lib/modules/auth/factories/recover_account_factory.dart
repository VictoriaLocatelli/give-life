import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/auth/models/forgot/recover_account.dart';
import 'package:give_life/modules/auth/usecases/remote_recover_account.dart';
import 'package:give_life/shared/data/factories/api_url_factory.dart';
import 'package:give_life/shared/data/http/http_client.dart';

RecoverAccount makeRemoteRecover() => RemoteRecoverAccount(
      httpClient: Modular.get<HttpClient>(),
      url: makeApiUrl('/auth/password/email'),
    );
