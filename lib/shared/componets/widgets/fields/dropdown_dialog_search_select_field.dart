import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:give_life/modules/profile/entity/bank_entity.dart';
import 'package:give_life/modules/profile/models/dropdown_type_model.dart';

import 'package:give_life/shared/componets/constants/constants.dart';

class DropdownDialogSearchSelectField<T> extends StatelessWidget {
  const DropdownDialogSearchSelectField({
    super.key,
    this.label,
    this.isRequired = true,
    this.validator,
    this.selectedItem,
    this.items = const [],
  });

  final String? label;
  final bool isRequired;
  final T? selectedItem;
  final String? Function(T?)? validator;
  final List<T> items;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final colorScheme = Theme.of(context).colorScheme;

    return Padding(
      padding: const EdgeInsets.only(bottom: Spacing.small),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (label != null)
            Text.rich(TextSpan(
              text: label,
              style: textTheme.caption,
              children: [
                if (isRequired)
                  TextSpan(
                    text: ' *',
                    style:
                        textTheme.caption?.copyWith(color: colorScheme.error),
                  ),
              ],
            )),
          const SizedBox(height: Spacing.xxSmall),
          DropdownSearch<T>(
            autoValidateMode: AutovalidateMode.onUserInteraction,
            validator: validator,
            selectedItem: selectedItem,
            itemAsString: (T u) {
              if (u is BankEntity) {
                return u.name as String;
              } else if (u is DropdownTypeModel) {
                return u.name as String;
              } else {
                return u.toString();
              }
            },
            items: items,
            popupProps: PopupProps.dialog(
              showSearchBox: true,
              emptyBuilder: (_, searchValue) {
                return Center(
                  child: Text(
                    'Nenhum resultado para "$searchValue"',
                    textAlign: TextAlign.center,
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
