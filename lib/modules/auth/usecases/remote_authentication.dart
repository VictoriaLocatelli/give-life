import 'package:give_life/modules/auth/authentication.dart';
import 'package:give_life/shared/core/entity/client_account_entitie.dart';
import 'package:give_life/shared/core/errors/domain_error.dart';
import 'package:give_life/shared/data/http/http_client.dart';
import 'package:give_life/shared/data/http/http_error.dart';
import 'package:give_life/shared/data/serializers/remotes/remote_client_account_serializer.dart';
import 'package:meta/meta.dart';

class RemoteAuthentication implements Authentication {
  const RemoteAuthentication({
    required this.httpClient,
    required this.url,
  });

  final String url;
  final HttpClient httpClient;

  @override
  Future<ClientAccountEntity> auth(InputAuthentication params) async {
    final body = InputRemoteAuthentication.fromDomain(params).toMap();

    try {
      final response =
          await httpClient.request(url: url, method: 'post', body: body);

      final data = response as Map<String, dynamic>;

      return RemoteClientAccountSerializer.fromMap(data['data']).toEntity();
    } on HttpError catch (error) {
      switch (error) {
        case HttpError.unauthorized:
          throw ("E-mail ou Senha inválidos");
        case HttpError.unprocessableEntity:
          throw DomainError.invalidCredentials;
        default:
          throw DomainError.unexpected;
      }
    }
  }
}

@immutable
class InputRemoteAuthentication {
  const InputRemoteAuthentication(
      {required this.email, required this.password});

  final String email;
  final String password;

  factory InputRemoteAuthentication.fromDomain(InputAuthentication params) =>
      InputRemoteAuthentication(
        email: params.email,
        password: params.secret,
      );

  Map<String, dynamic> toMap() => {
        'username': email,
        'password': password,
      };
}
