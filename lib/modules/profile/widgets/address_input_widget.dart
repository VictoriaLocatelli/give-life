import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/profile/controllers/client_profile_controller.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';

class AddressInputWidget extends StatelessWidget {
  const AddressInputWidget();

  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ClientProfileController>();

    return Observer(builder: (_) {
      return CustomTextFormField(
        label: 'Endereço',
        initialValue: controller.address != null ? controller.address : '.',
        onChanged: controller.addressValidator,
        keyboardType: TextInputType.streetAddress,
        decoration: InputDecoration(
          errorText: controller.addressError,
        ),
      );
    });
  }
}
