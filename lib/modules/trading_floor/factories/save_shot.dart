import 'package:give_life/modules/trading_floor/models/input_save_shot.dart';

abstract class SaveShot {
  Future<dynamic> save(InputSaveShot data);
}
