import 'package:flutter/material.dart';
import 'package:give_life/modules/home/view/widgets/youtube_players_widget.dart';
import 'package:give_life/shared/componets/theme/app_colors.dart';

class VideoPage extends StatelessWidget {
  final String url;

  VideoPage({required this.url});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(builder: (context, c) {
        return Container(
          width: c.maxWidth,
          height: c.maxHeight,
          color: AppColors.black,
          child: Center(
            child: YoutubePlayerWidget(
              url: url,
            ),
          ),
        );
      }),
    );
  }
}
