import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:give_life/modules/home/model/entity_%20auctions.dart';
import 'package:give_life/shared/componets/constants/app_images.dart';
import 'package:give_life/shared/componets/theme/app_colors.dart';
import 'package:intl/intl.dart';

class ScheduleAuctionsWidget extends StatelessWidget {
  const ScheduleAuctionsWidget(
      {super.key, required this.scheduleAuction, required this.action});
  final AuctionsEntity scheduleAuction;
  final Function(int id) action;

  @override
  Widget build(BuildContext context) {
    var format = DateFormat.MMM('pt_BR');

    String month = format.format(scheduleAuction.date);

    int day = scheduleAuction.date.day;

    List<String> timel = scheduleAuction.time.split(":");

    String time = timel[0] + ':' + timel[1] + 'h';

    TextStyle styleColumnOne =
        TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
    TextStyle styleColumnTwo = const TextStyle(
      fontWeight: FontWeight.bold,
    );

    var address = scheduleAuction.address;

    var city = scheduleAuction.city;

    var name = scheduleAuction.name;
    return Padding(
        padding: EdgeInsets.all(2),
        child: GestureDetector(
          onTap: () => action(scheduleAuction.id),
          child: Card(
            child: Row(
              children: [
                Expanded(
                    flex: 3,
                    child: Container(
                      alignment: Alignment.center,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Transform.translate(
                            offset: const Offset(10, 2),
                            child: Container(
                              alignment: Alignment.center,
                              child: Text(
                                '$day',
                                style: styleColumnOne,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          Transform.translate(
                              offset: const Offset(10, -15),
                              child: Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    '$month',
                                    style: styleColumnOne,
                                    textAlign: TextAlign.right,
                                  ))),
                          Transform.translate(
                              offset: const Offset(10, -12),
                              child: Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    '$time',
                                    textAlign: TextAlign.right,
                                  ))),
                          SizedBox(height: 30),
                        ],
                      ),
                    )),
                Expanded(
                    flex: 10,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        AutoSizeText(
                          '$name',
                          style: styleColumnTwo,
                          textAlign: TextAlign.center,
                          maxLines: 2,
                          minFontSize: 16,
                          maxFontSize: 40,
                        ),
                        Container(
                            width: 240,
                            alignment: Alignment.center,
                            child: AutoSizeText(
                              '$address',
                              maxLines: 2,
                              minFontSize: 6,
                              textAlign: TextAlign.center,
                            )),
                        Text('$city'),
                        SizedBox(height: 7),
                        Container(
                          width: 160,
                          height: 35,
                          child: ElevatedButton(
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    AppColors.grayButton),
                              ),
                              onPressed: () => action(scheduleAuction.id),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text('VER MAIS   '),
                                  Image.asset(
                                    AppImages.arrow.path,
                                    width: 13,
                                  ),
                                ],
                              )),
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ));
  }
}
