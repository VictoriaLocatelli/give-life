import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/shared/data/contracts/remotes/load_cities.dart';
import 'package:give_life/shared/data/factories/api_url_factory.dart';
import 'package:give_life/shared/data/http/http.dart';
import 'package:give_life/shared/data/usecases/remotes/remote_load_cities.dart';

LoadCities makeRemoteLoadCities() => RemoteLoadCities(
      url: makeApiUrl('/cities'),
      httpClient: Modular.get<HttpClient>(),
    );
