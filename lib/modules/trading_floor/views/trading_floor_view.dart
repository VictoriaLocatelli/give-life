import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:give_life/modules/trading_floor/controllers/trading_floor_controller.dart';
import 'package:give_life/modules/trading_floor/views/widget/lot_card.dart';
import 'package:give_life/shared/componets/theme/theme.dart';
import 'package:give_life/shared/componets/widgets/bottom_widget.dart';
import 'package:give_life/shared/componets/widgets/navigation_drawer.dart';

TextStyle styleColumnTwo = TextStyle(fontSize: 22, fontWeight: FontWeight.bold);

class TradingFloorView extends StatelessWidget {
  const TradingFloorView({super.key, required this.controller});
  final TradingFloorController controller;

  @override
  Widget build(BuildContext context) {
    controller.loadData();
    return Scaffold(
      appBar: AppBar(title: const Text("Pregão")),
      drawer: const NavigationDrawer(),
      body: Observer(builder: (context) {
        if (controller.isLoading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Container(
            child: CustomScrollView(
              slivers: <Widget>[
                SliverToBoxAdapter(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 5, vertical: 8),
                    child: Text('Em andamento',
                        textAlign: TextAlign.left, style: styleColumnTwo),
                  ),
                ),
                SliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing: 8.0,
                      crossAxisSpacing: 8.0,
                      mainAxisExtent: 300),
                  delegate: SliverChildListDelegate(controller
                          .lotAllEnable.isNotEmpty
                      ? controller.lotAllEnable
                          .map((e) => LotCard(
                                lot: e,
                                action: controller.toDatails,
                              ))
                          .toList()
                      : [
                          SizedBox(
                              width: double.infinity,
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    CircleAvatar(
                                      radius: 50.5,
                                      backgroundColor: Colors.black,
                                      child: CircleAvatar(
                                        radius: 48,
                                        backgroundColor: AppColors.background,
                                        child: SvgPicture.asset(
                                          'assets/svg/hammer.svg',
                                          color: Colors.black,
                                          width: 60,
                                        ),
                                      ),
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      child: Text(
                                        'Não há lote disponivel no momento',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ]))
                        ]),
                ),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 5, vertical: 8),
                    child: Text(
                      'Encerrados/ Vendidos',
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                        color: AppColors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 8,
                    crossAxisSpacing: 8,
                    mainAxisExtent: 300,
                  ),
                  delegate: SliverChildListDelegate(controller
                          .lotAllOthers!.isNotEmpty
                      ? controller.lotAllOthers!
                          .map((e) => LotCard(
                                lot: e,
                                action: controller.toDatails,
                              ))
                          .toList()
                      : [
                          SizedBox(
                              width: double.infinity,
                              child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    CircleAvatar(
                                      radius: 50.5,
                                      backgroundColor: Colors.black,
                                      child: CircleAvatar(
                                        radius: 48,
                                        backgroundColor: AppColors.background,
                                        child: SvgPicture.asset(
                                          'assets/svg/hammer.svg',
                                          color: Colors.black,
                                          width: 60,
                                        ),
                                      ),
                                    ),
                                    const Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      child: Text(
                                        'Não há lote disponivel no momento',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ]))
                        ]),
                ),
              ],
            ),
          ),
        );
      }),
      bottomNavigationBar: const BottomWidget(selected: 3),
    );
  }
}
