import 'package:give_life/shared/core/entity/city_entity.dart';

abstract class LoadCities {
  Future<List<CityEntity>> load();
}
