import 'package:flutter/cupertino.dart';
import 'package:give_life/modules/home/model/lot_entity.dart';
import 'package:give_life/shared/componets/theme/theme.dart';

class DetailsList extends StatelessWidget {
  const DetailsList({super.key, required this.lot});

  final LotEntity lot;

  @override
  Widget build(BuildContext context) {
    TextStyle styleInfoLot =
        TextStyle(fontSize: 15, fontWeight: FontWeight.bold);

    return Container(
      margin: const EdgeInsets.all(3),
      padding: const EdgeInsets.all(3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(5.0),
        ),
        border: Border.all(
          width: 1,
          color: Color(0xffD9D9D9),
          style: BorderStyle.solid,
        ),
        color: AppColors.secondaryWhite,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text('Quantidade:', style: styleInfoLot),
              Padding(
                padding: EdgeInsets.only(top: 5, right: 3),
                child: Text(
                  lot.quantity.toString(),
                ),
              ),
              Text(
                'Raça:',
                style: styleInfoLot,
              ),
              Padding(
                padding: EdgeInsets.only(top: 5, right: 3),
                child: Text(
                  "${lot.breed ?? ''}",
                ),
              ),
              Text(
                'Grau de sangue:',
                style: styleInfoLot,
              ),
              Padding(
                padding: EdgeInsets.only(top: 5, right: 3),
                child: Text(
                  "${lot.bloodgrade ?? ''}",
                ),
              ),
              Text(
                'Pelagem:',
                style: styleInfoLot,
              ),
              Padding(
                padding: EdgeInsets.only(top: 5, right: 3),
                child: Text(
                  "${lot.coat ?? ''}",
                ),
              ),
            ],
          ),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text(
                  'Idade:',
                  style: styleInfoLot,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5, right: 3),
                  child: Text(
                    "${lot.age ?? ''}",
                  ),
                ),
                Text(
                  'Categoria:',
                  style: styleInfoLot,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5, right: 3),
                  child: Text(
                    "${lot.category ?? ''}",
                  ),
                ),
                Text(
                  'CE(cm):',
                  style: styleInfoLot,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5, right: 3),
                  child: Text(
                    "${lot.ce ?? ''}",
                  ),
                ),
                SizedBox(
                  height: 50,
                )
              ]),
          Column(
            children: [
              Text(
                'Sexo:',
                style: styleInfoLot,
              ),
              Padding(
                padding: EdgeInsets.only(top: 5, right: 3),
                child: Text(
                  lot.gender == 'M'
                      ? 'Macho'
                      : lot.gender == 'F'
                          ? 'Fêmea'
                          : lot.gender == 'E'
                              ? 'Embrião'
                              : '',
                ),
              ),
              Text(
                'Peso médio:',
                style: styleInfoLot,
              ),
              Padding(
                padding: EdgeInsets.only(top: 5, right: 3),
                child: Text(
                  "${lot.averageweight ?? ''}",
                ),
              ),
              Text(
                'TAT/RP:',
                style: styleInfoLot,
              ),
              Padding(
                padding: EdgeInsets.only(top: 5, right: 3),
                child: Text(
                  "${lot.tatrp ?? ''}",
                ),
              ),
              SizedBox(
                height: 50,
              )
            ],
          ),
        ],
      ),
    );
  }
}
