import 'package:flutter_modular/flutter_modular.dart';
import 'package:give_life/modules/auth/authentication.dart';
import 'package:give_life/modules/auth/controllers/forgot/forgot_controller.dart';
import 'package:give_life/modules/auth/controllers/login/login_controller.dart';
import 'package:give_life/modules/auth/controllers/signup/signup_controller.dart';
import 'package:give_life/modules/auth/factories/recover_account_code_factory.dart';
import 'package:give_life/modules/auth/factories/recover_account_factory.dart';
import 'package:give_life/modules/auth/factories/recover_account_reset_factory.dart';
import 'package:give_life/modules/auth/models/signup/add_account.dart';
import 'package:give_life/modules/auth/factories/add_account_factory.dart';
import 'package:give_life/modules/auth/factories/authentication_factory.dart';
import 'package:give_life/modules/auth/views/forgot/forgot_code_view.dart';
import 'package:give_life/modules/auth/views/forgot/forgot_reset_view.dart';
import 'package:give_life/modules/auth/views/forgot/forgot_view.dart';
import 'package:give_life/modules/auth/views/login/login_view.dart';
import 'package:give_life/modules/auth/views/signup/signup_view.dart';
import 'package:give_life/shared/data/contracts/local/save_client_current_account.dart';

class AuthModule extends Module {
  @override
  List<Module> get imports => const [];

  @override
  List<Bind> get binds => [
        Bind.singleton((i) => makeRemoteAuthentication()),
        Bind.singleton((i) => makeRemoteRecover()),
        Bind.singleton((i) => makeRemoteAddAccount()),
        Bind.singleton((i) => makeRemoteRecoverCode()),
        Bind.singleton((i) => makeRemoteRecoverReset()),
        Bind.singleton((i) => LoginController(
              authentication: i(),
              saveCurrentAccount: i(),
            )),
        Bind.singleton((i) => SignUpController(addAccount: i())),
        Bind.singleton((i) => ForgotController(recoverAccount: i()))
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute('/',
            child: (_, __) =>
                LoginView(controller: Modular.get<LoginController>())),
        ChildRoute('/signup',
            child: (_, __) => SignUpPage(
                  controller: Modular.get<SignUpController>(),
                )),
        ChildRoute('/forgot',
            child: (_, __) => ForgotPage(
                  controller: Modular.get<ForgotController>(),
                )),
        ChildRoute('/forgotCode', child: (_, __) => const ForgotCodeView()),
        ChildRoute('/forgotReset',
            child: (_, __) => ForgotPasswordView(
                  controller: Modular.get<ForgotController>(),
                ))
      ];
}
