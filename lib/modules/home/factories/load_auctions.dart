import 'package:give_life/modules/home/model/entity_%20auctions.dart';
import 'package:give_life/modules/home/model/entity_%20detail_auctions.dart';

abstract class LoadAuctions {
  Future<List<AuctionsEntity>?> load();
  Future<DetailsAuctionsEntity?> details(int id);
}
