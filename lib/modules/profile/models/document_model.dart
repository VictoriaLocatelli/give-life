class DocumentModel {
    int? type;
    String? annex;

    DocumentModel({this.type, this.annex}); 

    DocumentModel.fromJson(Map<String, dynamic> json) {
        type = json['type'];
        annex = json['annex'];
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = {};
        data['type'] = type;
        data['annex'] = annex;
        return data;
    }
}