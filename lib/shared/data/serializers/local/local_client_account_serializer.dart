import 'dart:developer' as developer;
import 'package:give_life/shared/core/entity/client_account_entitie.dart';
import 'package:give_life/shared/data/http/http_error.dart';

import '../map_ext.dart';

class LocalClientAccountKeys {
  static const String id = 'id';
  static const String name = 'name';
  static const String photo = 'icon';
  static const String email = 'email';
  static const String token = 'token';
  static const String personId = 'person_id';
}

class LocalClientAccountSerializer {
  const LocalClientAccountSerializer(
      {required this.name,
      required this.email,
      required this.token,
      this.photo,
      this.personId});
  final String name;
  final String email;
  final String token;
  final String? photo;
  final int? personId;

  factory LocalClientAccountSerializer.fromMap(Map<String, dynamic> map) {
    map;
    final name =
        map.getValueByKeyList(LocalClientAccountKeys.name.split('.')) as String;
    final email = map.getValueByKeyList(LocalClientAccountKeys.email.split('.'))
        as String;
    final photo =
        map.getValueByKeyListPhoto(LocalClientAccountKeys.photo.split('.'))
            as String?;
    final token = map.getValueByKeyList(LocalClientAccountKeys.token.split('.'))
        as String;
    final personId = map
        .getValueByKeyList(LocalClientAccountKeys.personId.split('.')) as int?;
    personId;
    try {
      return LocalClientAccountSerializer(
        name: name,
        photo: photo,
        email: email,
        token: token,
        personId: personId,
      );
    } on TypeError catch (error) {
      developer.log('[LocalClientAccountSerializer] $error');
      throw HttpError.invalidData;
    }
  }

  ClientAccountEntity toEntity() => ClientAccountEntity(
        name: name,
        email: email,
        photo: photo,
        token: token,
        personId: personId,
      );
}
