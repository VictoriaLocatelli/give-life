import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:give_life/modules/direct_selling/models/input_save_shot.dart';
import 'package:give_life/modules/direct_selling/models/lot_entity.dart';
import 'package:give_life/modules/direct_selling/views/widget/table_row_widget.dart';
import 'package:give_life/shared/componets/constants/constants.dart';
import 'package:give_life/shared/componets/theme/theme.dart';
import 'package:intl/intl.dart';

import '../../../../shared/componets/constants/app_images.dart';

String getFormatMoney(dynamic value) =>
    NumberFormat.currency(symbol: 'R\$', locale: 'pt_BR').format(value);

String getFormatDate(String value) =>
    DateFormat('dd/MM/yyyy').format(DateTime.parse(value));

String getFormatHour(String value) =>
    DateFormat('HH:mm').format(DateTime.parse('2000-01-01 ' + value));

Future<void> alertModal(BuildContext context, String message) => showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        icon: SvgPicture.asset(AppImages.warning.path),
        title: Text(
          'Atenção!',
          style: TextStyle(
            color: AppColors.black,
            fontSize: 28,
            fontWeight: FontWeight.bold,
          ),
        ),
        content: Column(children: [
          Text('$message'),
          ElevatedButton(
              onPressed: () => Navigator.pop(context, true), child: Text("OK"))
        ]),
      );
    });

Future<void> confirmModal(
        {required BuildContext context,
        required double bid,
        required LotEntity lot,
        required Function(InputSaveShot data) action}) =>
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            icon: SvgPicture.asset(
              AppImages.warning.path,
              color: AppColors.secondary,
            ),
            title: Text(
              'Confirmação!',
              style: TextStyle(
                color: AppColors.black,
                fontSize: 28,
                fontWeight: FontWeight.bold,
              ),
            ),
            content: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Ao confirmar o seu lance será:',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    child: Container(
                      margin: const EdgeInsets.all(3),
                      padding: const EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5.0),
                        ),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const SizedBox(height: Spacing.xxSmall),
                          Text(
                            '${getFormatMoney(bid)}',
                            textAlign: TextAlign.left,
                            style: const TextStyle(
                              color: AppColors.black,
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Expanded(
                        flex: 60,
                        child: Container(
                          child: ElevatedButton(
                              onPressed: () => action(InputSaveShot(
                                  comesFrom: (lot.shots!.firstWhere(
                                          (shot) => lot.id == shot.lotId))
                                      .comesFrom,
                                  lotId: lot.id!,
                                  value: bid)),
                              child: Text("Confirmar")),
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Expanded(
                        flex: 60,
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                            horizontal: Spacing.xxSmall,
                          ),
                          child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                      AppColors.secondaryWhite),
                                  foregroundColor: MaterialStateProperty.all(
                                      AppColors.primary),
                                  side: MaterialStateProperty.all(
                                      const BorderSide(
                                          color: AppColors.secondary,
                                          width: 2))),
                              onPressed: () => Navigator.pop(context, true),
                              child: Text("Cancelar")),
                        ),
                      ),
                    ],
                  ),
                ]),
          );
        });

Future<void> showModal(
        {required BuildContext context, required Function() action}) =>
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            icon: SvgPicture.asset(
              width: 100,
              height: 100,
              color: AppColors.green,
              AppImages.validation.path,
            ),
            title: Text(
              'Parabéns!',
              style: TextStyle(
                color: AppColors.black,
                fontSize: 28,
                fontWeight: FontWeight.bold,
              ),
            ),
            content: Column(mainAxisSize: MainAxisSize.min, children: [
              Text(
                'Seu lance foi realizado com sucesso!',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: AppColors.black,
                  fontSize: 20,
                ),
              ),
              ElevatedButton(
                  onPressed: () {
                    action();
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                  child: Text("OK"))
            ]),
          );
        });

Future<void> lastBidsModal(
        {required BuildContext context,
        required LotEntity lot,
        required Function() action}) =>
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return Center(
            child: Column(mainAxisSize: MainAxisSize.min, children: [
              Container(
                width: double.infinity,
                margin: EdgeInsets.fromLTRB(0, 0, 0, 7),
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0),
                      offset: Offset(0.0, 4),
                      blurRadius: 10.0,
                    ),
                  ],
                ),
                child: LayoutBuilder(builder: (context, c) {
                  return Container(
                    width: c.maxWidth * .90,
                    height: c.maxWidth,
                    color: Colors.white,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          "Últimos lances",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: Colors.black,
                              style: BorderStyle.solid,
                            ),
                          ),
                          child: Table(
                            columnWidths: {
                              0: FlexColumnWidth(6),
                              1: FlexColumnWidth(15),
                              2: FlexColumnWidth(20),
                            },
                            border: TableBorder(
                              verticalInside: BorderSide(
                                color: Colors.white,
                                style: BorderStyle.solid,
                                width: 1.0,
                              ),
                            ),
                            children: [
                              TableRow(
                                children: 'Nome-Valor-Data/Hora'
                                    .split('-')
                                    .map((name) {
                                  return Container(
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: AppColors.secondary,
                                    ),
                                    child: Text(
                                      name,
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.white),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 5.0, vertical: 10),
                                  );
                                }).toList(),
                              ),
                              if (lot.shots != null)
                                for (int i = 0; i < lot.shots!.length; i++)
                                  tableRow(
                                      '${lot.shots![i].client.name[0]}***|${getFormatMoney(lot.shots![i].value)}|${getFormatDate(lot.shots![i].date)} - ${getFormatHour(lot.shots![i].time)}'),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                }),
              ),
              Transform.translate(
                offset: Offset(0, -70),
                child: Container(
                  width: 200,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text("OK"),
                  ),
                ),
              ),
            ]),
          );
        });
