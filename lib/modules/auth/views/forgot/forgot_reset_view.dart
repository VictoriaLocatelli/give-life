import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';

import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:give_life/modules/auth/controllers/forgot/forgot_controller.dart';
import 'package:give_life/shared/componets/constants/spacing.dart';
import 'package:give_life/shared/componets/widgets/fields/text_form_field.dart';

class ForgotPasswordView extends StatelessWidget {
  const ForgotPasswordView({Key? key, required this.controller})
      : super(key: key);
  final ForgotController controller;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return GestureDetector(
      onTap: () => FocusScope.of(context).focusedChild?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Recuperar senha'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(0),
          child: Container(
            height: double.infinity,
            width: double.infinity,
            padding: const EdgeInsets.symmetric(
              vertical: Spacing.large,
              horizontal: Spacing.medium,
            ),
            child: ListView(
              shrinkWrap: true,
              children: [
                const SizedBox(height: Spacing.medium),
                Center(
                  child: Container(
                    width: 900,
                    height: 900,
                    child: _PasswordInput(),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _SendButton extends StatefulWidget {
  const _SendButton({Key? key}) : super(key: key);

  @override
  State<_SendButton> createState() => _SendButtonState();
}

class _SendButtonState extends State<_SendButton> {
  @override
  Widget build(BuildContext context) {
    final controller = Modular.get<ForgotController>();
    String value = '0';
    setState(() {
      value;
    });
    return Observer(builder: (_) {
      return ElevatedButton(
        onPressed: value.length >= 6 ? controller.checkCode : null,
        child: controller.isLoading
            ? const CircularProgressIndicator()
            : const Text('Enviar'),
      );
    });
  }
}

class _PasswordInput extends StatefulWidget {
  _PasswordInput({Key? key}) : super(key: key);

  @override
  State<_PasswordInput> createState() => _PasswordInputState();
}

class _PasswordInputState extends State<_PasswordInput> {
  int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * 60 * 5;
  String value = '0';
  String? passwordErro = null;
  bool toggleObscurePassword = true;
  bool toggleObscurePasswordTwo = true;
  @override
  Widget build(BuildContext context) {
    void onEnd() {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        duration: Duration(seconds: 20),
        action: SnackBarAction(
            textColor: Colors.white,
            label: "Voltar",
            onPressed: () {
              Modular.to.navigate('forgot');
            }),
        content: Text(
          "Seu tempo expirou! Solicite um novo código \n",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Color.fromARGB(213, 255, 38, 74),
      ));
      Future.delayed(Duration(seconds: 20), () {
        Modular.to.navigate('forgot');
      });
    }

    final controllerTime =
        CountdownTimerController(endTime: endTime, onEnd: onEnd);
    final controller = Modular.get<ForgotController>();
    String? codeErro = null;

    setState(() {
      value;
    });
    return Observer(builder: (_) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(
            flex: 1,
            child: CustomTextFormField(
              label: 'Nova senha',
              onChanged: (text) {
                controller.password = text;
                setState(() {
                  value = text;
                });
              },
              keyboardType: TextInputType.text,
              obscureText: toggleObscurePassword,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.grey.shade200,
                prefixIcon: const Icon(Icons.lock_outline),
                errorText: codeErro,
                suffixIcon: IconButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onPressed: () {
                    setState(() {
                      toggleObscurePassword = false;
                    });
                  },
                  icon: Icon(toggleObscurePassword
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined),
                ),
              ),
            ),
          ),
          Flexible(
            flex: 1,
            child: CustomTextFormField(
              label: 'Confirmar senha',
              onChanged: (text) {
                controller.passwordConf = text;
                if (controller.passwordConf != controller.password) {
                  passwordErro = "Os campos não conferem. Tente outra vez";
                } else {
                  passwordErro = null;
                }
                setState(() {
                  passwordErro;
                  value = text;
                });
              },
              keyboardType: TextInputType.text,
              obscureText: toggleObscurePasswordTwo,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.grey.shade200,
                prefixIcon: const Icon(Icons.lock_outline),
                errorText: passwordErro,
                suffixIcon: IconButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onPressed: () {
                    setState(() {
                      toggleObscurePasswordTwo = false;
                    });
                  },
                  icon: Icon(toggleObscurePasswordTwo
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined),
                ),
              ),
            ),
          ),
          Flexible(
            flex: 3,
            child: Transform.translate(
              offset: Offset(0, -15),
              child: CountdownTimer(
                controller: controllerTime,
                onEnd: onEnd,
                endTime: endTime,
                endWidget: Container(),
              ),
            ),
          ),
          Flexible(
              flex: 3,
              child: ElevatedButton(
                onPressed: value.length >= 8 &&
                        controller.passwordConf == controller.password
                    ? controller.reset
                    : null,
                child: controller.isLoading
                    ? const CircularProgressIndicator()
                    : const Text('Verificar código'),
              )),
        ],
      );
    });
  }
}
