import 'dart:convert';

import 'package:give_life/shared/core/entity/client_account_entitie.dart';
import 'package:give_life/shared/data/contracts/cache/cache_storage.dart';
import 'package:give_life/shared/data/contracts/local/load_client_current_account.dart';
import 'package:give_life/shared/data/serializers/local/local_client_account_serializer.dart';

class LocalLoadCurrentAccount implements LoadClientCurrentAccount {
  const LocalLoadCurrentAccount({required this.secureStorage});

  final CacheStorage secureStorage;

  @override
  Future<ClientAccountEntity?> load() async {
    try {
      final rawAccount = await secureStorage.fetch('account') as String?;

      if (rawAccount == null) return null;

      return LocalClientAccountSerializer.fromMap(jsonDecode(rawAccount))
          .toEntity();
    } catch (error) {
      throw error;
    }
  }
}
